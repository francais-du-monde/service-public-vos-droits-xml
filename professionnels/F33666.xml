<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F33666" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Convention collective : les obligations de l'employeur</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Ressources humaines</dc:subject>
<dc:description>Une convention collective détermine les règles applicables en droit du travail (contrat de travail, hygiène, congés, salaires, classification, licenciement...) dont relève une entreprise, en fonction de son secteur d'activité. Conclue par les organisations syndicales représentatives des salariés et les organisations d'employeurs, elle fixe les obligations de l'employeur, qui peuvent être différentes du droit établi par le code du travail.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-05-12</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F33666</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177925&amp;cidTexte=LEGITEXT000006072050, http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006189531&amp;cidTexte=LEGITEXT000006072050, http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006901814&amp;cidTexte=LEGITEXT000006072050, http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000018535747&amp;cidTexte=LEGITEXT000006072050, http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000018535599&amp;cidTexte=LEGITEXT000006072050</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N17310</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Professionnels</Audience>
<Canal>www.service-public.fr</Canal>
<Cible>Tout professionnel</Cible>
<FilDAriane>
<Niveau ID="Professionnels">Accueil professionnels</Niveau>
<Niveau ID="N24267">Ressources humaines</Niveau>
<Niveau ID="N17310">Droits syndicaux</Niveau>
<Niveau ID="F33666" type="Fiche d'information">Convention collective : les obligations de l'employeur</Niveau>
</FilDAriane>
<Theme ID="N24267">
<Titre>Ressources humaines</Titre>
</Theme>
<SousThemePere ID="N364">Action sociale</SousThemePere><DossierPere ID="N17310">
<Titre>Droits syndicaux</Titre>
<Fiche ID="F23513">Élections professionnelles</Fiche>
<Fiche ID="F102">Délégué syndical</Fiche>
<Fiche ID="F33666">Convention collective : les obligations de l'employeur</Fiche>
<Fiche ID="F117">Droit de grève</Fiche>
<Fiche ID="F32193">Base de données économiques et sociales (BDES)</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Une convention collective détermine les règles applicables en droit du travail (contrat de travail, hygiène, congés, salaires, classification, licenciement...) dont relève une entreprise, en fonction de son secteur d'activité. Conclue par les organisations syndicales représentatives des salariés et les organisations  d'employeurs, elle fixe les obligations de l'employeur, qui peuvent être différentes du droit établi par le code du travail. </Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Champ d'application</Paragraphe>
</Titre><Paragraphe>Les signataires de la convention collective fixent son champ d'application :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe> au niveau géographique : national, régional ou départemental, </Paragraphe>
</Item>
<Item>
<Paragraphe>et au niveau professionnel : interprofessionnel, branche, entreprise.</Paragraphe>
</Item>
</Liste>
<Paragraphe>La plupart des conventions collectives sont nationales (CCN). Mais, dans certains secteurs, elles sont complétées par des conventions et accords régionaux ou départementaux.</Paragraphe>
<Paragraphe>Quand une entreprise entre dans le champ d'application d'une convention, elle est tenue d'appliquer le texte :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>obligatoirement si la convention a été étendue par arrêté du ministère chargé du travail, publié au<Expression> Journal officiel</Expression>,</Paragraphe>
</Item>
<Item>
<Paragraphe>seulement si elle adhère à l'organisation patronale signataire, en l'absence d'arrêté d'extension national.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Seuls les conventions ou accords collectifs conclus au niveau de l'entreprise sont d'application automatique.</Paragraphe>
<Paragraphe>La convention collective détermine la durée de sa validité :   généralement à durée indéterminée, mais elle peut aussi être conclue pour une durée déterminée. </Paragraphe>
<Paragraphe>Si elle est à  durée déterminée, elle ne peut pas dépasser 5 ans. Toutefois, à défaut de stipulation contraire, une convention à durée déterminée qui arrive à expiration, sans être  renouvelée, continue à produire effet.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Contenu</Paragraphe>
</Titre><Paragraphe>La convention collective traite : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>des conditions d'emploi, de formation professionnelle et de travail des salariés,</Paragraphe>
</Item>
<Item>
<Paragraphe> et de leurs garanties sociales.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Elle comporte généralement un texte de base, souvent complété par des avenants, des accords et des annexes. </Paragraphe>
<Paragraphe>La convention collective  adapte les dispositions du code du travail aux situations particulières du secteur d'activité concerné et peut contenir des dispositions :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>que le code du travail ne prévoit pas : prime de fin d'année, congés payés supplémentaires en fonction de l'ancienneté dans l'entreprise...</Paragraphe>
</Item>
<Item>
<Paragraphe> ou qui sont plus favorables pour le salarié : durée du travail hebdomadaire inférieure à la durée légale de 35 heures, calcul de l'indemnité de licenciement plus favorable que celui de l'indemnité légale...</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Quelle convention collective appliquer</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Selon l'activité principale exercée</Paragraphe>
</Titre><Paragraphe>La convention collective applicable dans une entreprise est déterminée par le <LienInterne LienPublication="F33050" type="Fiche Question-réponse" audience="Professionnels">code APE</LienInterne> (ou code Naf),  attribué par l'<LienInterne LienPublication="R12417" type="Acronyme">Insee</LienInterne>  lors de la création de l'entreprise, en fonction de l'activité principale (APE) exercée.</Paragraphe>
<Paragraphe>Il est possible d'effectuer une <LienInterne LienPublication="R31516" type="Téléservice" audience="Professionnels">recherche dans la nomenclature d'activités française</LienInterne> (NAF) pour déterminer son code APE.</Paragraphe>
<Paragraphe>Le code APE n'est pas un identifiant de convention collective. Une convention collective a pour identifiant un nom, un numéro de brochure et un numéro IDCC (identifiant des conventions collectives).</Paragraphe>
<Paragraphe>La convention collective comporte la mention de son champ d'application (interprofessionnel, branche ou entreprise). Les activités couvertes par la convention y sont généralement désignées par les codes APE correspondants : il est donc nécessaire de les comparer avec celui attribué à l'entreprise pour savoir si elle doit  appliquer la convention.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>le code APE n'est qu'un élément indicatif, sans valeur juridique ; c'est l'activité effective et principale exercée par l'entreprise qui demeure le véritable critère d'application de la convention collective.</Paragraphe>
</ASavoir>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>En cas d'activités multiples</Paragraphe>
</Titre><Paragraphe>Si l'entreprise exerce plusieurs activités, elle doit déterminer son activité principale :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>en cas d'activités industrielles multiples, l'activité principale correspond à celle qui occupe le plus de salariés,</Paragraphe>
</Item>
<Item>
<Paragraphe>en cas d'activités commerciales ou de prestations de services multiples, l'activité principale correspond à celle dont le chiffre d'affaires est le plus élevé,</Paragraphe>
</Item>
<Item>
<Paragraphe>en cas d'exercice simultané d'une activité industrielle et d'une activité commerciale, l'activité est considérée comme principalement industrielle si le chiffre d'affaires d'origine industrielle est égal ou supérieur à 25 % du chiffre d'affaires total.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si l'employeur exerce des activités différentes, indépendantes les unes des autres, dans des locaux distincts, chaque activité relève d'un champ professionnel distinct, avec l'application de la convention collective correspondante à chacune de ces activités.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Information des salariés</Paragraphe>
</Titre><Paragraphe>La convention collective s'applique à tous les salariés   de l'entreprise liés par un contrat de travail (<LienInterne LienPublication="R2454" type="Sigle">CDD</LienInterne>, <LienInterne LienPublication="R24389" type="Sigle">CDI</LienInterne>, en période d'essai...).</Paragraphe>
<Paragraphe>Le salarié ne peut pas renoncer aux droits qu'il tient d'une convention collective. </Paragraphe>
<Paragraphe>L'employeur est tenu d'informer les salariés sur les dispositions conventionnelles applicables à l'entreprise au moment de l'embauche.</Paragraphe>
<Paragraphe>Le salarié doit pouvoir consulter la convention collective applicable dans l'entreprise.</Paragraphe>
<Paragraphe>Un accord prévoit les conditions permettant de consulter la convention collective applicable dans l'entreprise. À défaut d'accord, l'employeur met une version à jour de la convention sur l'intranet et tient un exemplaire de cette convention à la disposition des représentants du personnel.</Paragraphe>
<Paragraphe>Un avis doit être affiché sur le lieu de travail :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>indiquant la référence de la convention collective dont relève l'établissement et des accords applicables,</Paragraphe>
</Item>
<Item>
<Paragraphe> précisant les modalités de leur consultation sur le lieu de travail.</Paragraphe>
</Item>
</Liste>
<Attention>
<Titre>Attention</Titre><Paragraphe>l'intitulé de la convention collective applicable dans l'entreprise doit être spécifié sur le bulletin de paie remis aux salariés.</Paragraphe>
</Attention>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Changement de convention collective</Paragraphe>
</Titre><Paragraphe>Une entreprise peut  changer de convention collective de rattachement dans les cas suivants : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>changement d'activité principale,</Paragraphe>
</Item>
<Item>
<Paragraphe>erreur lors de l'immatriculation,</Paragraphe>
</Item>
<Item>
<Paragraphe>fusion entre entreprises de deux secteurs différents,</Paragraphe>
</Item>
<Item>
<Paragraphe>scission d'une entreprise,</Paragraphe>
</Item>
<Item>
<Paragraphe>cession d'une entreprise,</Paragraphe>
</Item>
<Item>
<Paragraphe>transfert du siège social de l'entreprise dans un autre département, en cas d'application d'un convention collective territoriale.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Pour obtenir ce changement, l'employeur doit :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>demander à l'Insee la <LienInterne LienPublication="R18901" type="Formulaire" audience="Professionnels">modification du code APE</LienInterne>, avec attribution d'un nouveau code,</Paragraphe>
</Item>
<Item>
<Paragraphe> notifier cette dénonciation au comité d'entreprise, aux comités d'établissement, aux délégués du personnel, aux délégués syndicaux ou aux salariés mandatés,</Paragraphe>
</Item>
<Item>
<Paragraphe>informer les salariés de la modification de la convention collective (préavis d'1 mois).</Paragraphe>
</Item>
</Liste>
<Paragraphe>Les dispositions de la convention collective remplacée continuent à produire leurs effets jusqu'à l'entrée en vigueur de la nouvelle. Les salariés ne peuvent plus se prévaloir de droits prévus par l'ancienne convention.</Paragraphe>
<Paragraphe>Si l'ancienne convention collective était d'application obligatoire et que plus aucune convention collective n'est désormais applicable, les effets de l'ancienne sont maintenus pendant 1 an. Les salariés conservent les avantages individuels acquis au titre de l'ancienne convention pendant 15 mois (12 mois + 3 mois de préavis).</Paragraphe>
<Paragraphe>Une nouvelle négociation doit s'engager dans l'entreprise concernée, à la demande d'une des parties intéressées, dans les 3 mois suivant la mise en cause : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe> soit pour l'adaptation aux dispositions conventionnelles nouvellement applicables, </Paragraphe>
</Item>
<Item>
<Paragraphe>soit pour l'élaboration de nouvelles dispositions.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si l'ancienne convention collective était appliquée volontairement, l'ancienne convention cesse de produire effet dès sa dénonciation.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R20" type="Local personnalisé sur SP">
<Titre>Unité territoriale de la Direccte</Titre>
<PivotLocal>direccte_ut</PivotLocal>
<RessourceWeb URL="https://lannuaire.service-public.fr/recherche?whoWhat=Unit%C3%A9+territoriale+de+la+Direccte&amp;where="/>
</OuSAdresser>
<OuSAdresser ID="R2246" type="Local personnalisable">
<Titre>Direction régionale de l'Insee attributaire des numéros Siren et Siret</Titre>
<PivotLocal>insee_siren_siret</PivotLocal>
<RessourceWeb URL="http://www.insee.fr/fr/service/default.asp?page=entreprises/demarches-immatriculation.htm"/>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177925&amp;cidTexte=LEGITEXT000006072050" ID="R32837">
<Titre>Code du travail : articles L2221-1 à L2222-4</Titre>
<Complement>Objet d'une convention collective</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006189531&amp;cidTexte=LEGITEXT000006072050" ID="R32847">
<Titre>Code du travail : articles L2261-2 à L2261-14</Titre>
<Complement>Pour déterminer la convention collective applicable</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006901814&amp;cidTexte=LEGITEXT000006072050" ID="R37865">
<Titre>Code du travail : article L2262-5</Titre>
<Complement>Information et communication de la convention collective</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000018535747&amp;cidTexte=LEGITEXT000006072050" ID="R31823">
<Titre>Code du travail : articles D2231-1 à R2231-9</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000018535599&amp;cidTexte=LEGITEXT000006072050" ID="R32848">
<Titre>Code du travail : articles R2262-1 à R2262-5</Titre>
<Complement>Obligations d'information de l'employeur</Complement>
</Reference>
<ServiceEnLigne ID="R31516" URL="http://www.insee.fr/fr/methodes/default.asp?page=nomenclatures/naf2008/liste_n1.htm" type="Téléservice">
<Titre>Recherche dans la nomenclature d'activités française (NAF)</Titre>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R18901" URL="http://www.insee.fr/fr/service/entreprises/sirene/pdf/formulaire-APEN-V6-recto-verso.pdf" format="application/pdf" poids="27.4 KB" type="Formulaire">
<Titre>Demande de modification du code d'activité principale (APE) d'une entreprise</Titre>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R2970" URL="http://www.legifrance.gouv.fr/initRechConvColl.do" type="Téléservice">
<Titre>Consulter ou télécharger une convention collective en version numérique</Titre>
<Source ID="R30608">Direction de l'information légale et administrative (Premier ministre)</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R15651" URL="http://www.ladocumentationfrancaise.fr/annonce/conventions-collectives.shtml?xtor=AL-666" type="Téléservice">
<Titre>Commander une convention collective en version papier</Titre>
<Source ID="R30786">La Documentation française</Source>
</ServiceEnLigne>
<Abreviation ID="R2454" type="Sigle">
<Titre>CDD</Titre>
<Texte>
<Paragraphe>Contrat à durée déterminée</Paragraphe>
</Texte>
</Abreviation>
<Abreviation ID="R24389" type="Sigle">
<Titre>CDI</Titre>
<Texte>
<Paragraphe>Contrat de travail à durée indéterminée</Paragraphe>
</Texte>
</Abreviation>
<Abreviation ID="R12417" type="Acronyme">
<Titre>Insee</Titre>
<Texte>
<Paragraphe>Institut national de la statistique et des études économiques</Paragraphe>
</Texte>
</Abreviation>
<QuestionReponse ID="F2395" audience="Professionnels">Où peut-on consulter ou se procurer une convention collective ?</QuestionReponse>
<QuestionReponse ID="F33050" audience="Professionnels">À quoi correspond le code APE ?</QuestionReponse>
<QuestionReponse ID="F23106" audience="Professionnels">Quelles sont les obligations d'affichage dans une entreprise ?</QuestionReponse>
</Publication>
