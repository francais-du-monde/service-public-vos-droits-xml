<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F31472" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Protection de la santé dans la fonction publique</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>L'administration doit protéger la santé de ses agents. Les agents publics ont droit à un suivi médical pendant et après leur carrière dans l'administration.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2015-02-20</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F31472</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000022446870&amp;cidTexte=LEGITEXT000006068842, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006063791, http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000700869, http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021467559, http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027378973, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028322873, http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030624505, http://bjfp.fonction-publique.gouv.fr/-recup-par-id-/C_20140320_N0001</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N431</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N431">Conditions de travail dans la fonction publique</Niveau>
<Niveau ID="F31472" type="Fiche d'information">Protection de la santé dans la fonction publique</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19965">Santé, sécurité et conditions de travail</SousThemePere><DossierPere ID="N431">
<Titre>Conditions de travail dans la fonction publique</Titre>
<Fiche ID="F496">Sécurité et droit de retrait</Fiche>
<Fiche ID="F31472">Protection de la santé</Fiche>
<Fiche ID="F13974">Télétravail</Fiche>
<Fiche ID="F530">Devoir de réserve, discrétion et secret professionnel</Fiche>
<Fiche ID="F32707">Obéissance hiérarchique</Fiche>
<Fiche ID="F32706">Devoir d'information des usagers</Fiche>
<Fiche ID="F32574">Protection fonctionnelle : agent public victime</Fiche>
<Fiche ID="F18848">Protection fonctionnelle : agent public poursuivi en justice</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>L'administration doit protéger la santé de ses agents. Les agents publics ont droit à un suivi médical pendant et après leur carrière dans l'administration.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Au cours de la carrière dans l'administration</Paragraphe>
</Titre><Paragraphe>Avant sa prise de fonction, un agent public (fonctionnaire et contractuel) est soumis à un examen médical pour savoir s'il est apte à exercer ce poste. Il est procédé aux vaccinations nécessaires.</Paragraphe>
<Paragraphe> Tout au long de sa carrière, un agent public bénéficie  d'examens périodiques :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>dans la fonction publique d'État (FPE), une visite médicale a lieu tous les 5 ans. Si l'agent le demande, il peut bénéficier d'un examen médical tous les ans.</Paragraphe>
</Item>
<Item>
<Paragraphe>dans la fonction publique  territoriale (FPT), une visite médicale a lieu tous les 2 ans. Dans cet intervalle, l'agent qui le demande peut bénéficier d'un examen médical supplémentaire.</Paragraphe>
</Item>
<Item>
<Paragraphe>dans la fonction publique hospitalière (FPH), la visite médicale a lieu tous les ans.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Certains  personnels bénéficient d'une surveillance médicale renforcée (par exemple, les femmes enceintes, les travailleurs handicapés, etc.).</Paragraphe>
<Paragraphe>S'il le juge nécessaire, le médecin du travail peut prescrire des examens complémentaires.</Paragraphe>
<Paragraphe>Lorsque l'état de santé d'un agent ne lui permet plus d'assurer ses fonctions, le médecin du travail propose à l'administration des aménagements de son poste ou de ses conditions de travail.</Paragraphe>
<Paragraphe>Le médecin constitue un dossier médical en santé au travail.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Après le départ de l'administration</Paragraphe>
</Titre><Paragraphe>Les agents publics peuvent être suivis lorsqu'ils quittent définitivement l'administration (retraite, passage dans le privé...).</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Dans la FPE et la FPT</Paragraphe>
</Titre><Paragraphe>Un agent public qui, dans ses fonctions, a  été exposé à l'amiante, une substance cancérogène, mutagène ou toxique, a droit à un suivi médical post-professionnel après avoir quitté définitivement la fonction publique.</Paragraphe>
<Paragraphe>Ce suivi médical est pris en charge par la dernière administration au sein de laquelle l'agent a été exposé.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Dans la FPH</Paragraphe>
</Titre><Paragraphe>Un agent  ayant été, dans ses fonctions, exposé à un agent cancérogène, mutagène ou toxique a droit, après avoir cessé ses fonctions, à un suivi médical post-professionnel :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>pour un agent recruté avant 2013, il est nécessaire de demander une attestation d'exposition pour bénéficier du suivi,</Paragraphe>
</Item>
<Item>
<Paragraphe>pour un agent recruté depuis 2013, l'attestation est délivrée au vu de la fiche de prévention des expositions.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Les honoraires et frais médicaux sont pris en charge par l'administration au sein duquel l'agent a été exposé.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000022446870&amp;cidTexte=LEGITEXT000006068842" ID="R39701">
<Titre>Loi n°84-53 du 26 janvier 1984 relative au statut de la fonction publique territoriale : article 108-4</Titre>
<Complement>Suivi post-professsionnel dans la FPT</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006063791" ID="R2734">
<Titre>Décret n°82-453 du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000700869" ID="R15894">
<Titre>Décret n°85-603 du 10 juin 1985 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la médecine professionnelle et préventive dans la fonction publique territoriale (FPT)</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021467559" ID="R1860">
<Titre>Décret n°2009-1546 du 11 décembre 2009 relatif au suivi médical post-professionnel des agents de l'État exposés à un agent cancérogène, mutagène ou toxique pour la reproduction</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027378973" ID="R32144">
<Titre>Décret n°2013-365 du 29 avril 2013 relatif au suivi médical post-professionnel des agents de la fonction publique territoriale (FPT) exposés à l'amiante</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028322873" ID="R36402">
<Titre>Décret n°2013-1151 du 12 décembre 2013 relatif au suivi médical post-professionnel</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030624505" ID="R41237">
<Titre>Décret n°2015-567 du 20 mai 2015 relatif aux modalités du suivi médical postprofessionnel des agents de l'Etat exposés à une substance cancérogène, mutagène ou toxique pour la reproduction</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://bjfp.fonction-publique.gouv.fr/-recup-par-id-/C_20140320_N0001" ID="R36781">
<Titre>Circulaire du 20 mars 2014 relative à la mise en œuvre du plan national d'action pour la prévention des risques psychosociaux</Titre>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R32247" URL="http://www.fonction-publique.gouv.fr/files/files/publications/coll_ressources_humaines/SST_livret3.pdf" audience="Particuliers">
<Titre>Santé et sécurité dans la fonction publique d'État</Titre>
<Source ID="R30602">Ministère en charge de la fonction publique</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R32248" URL="http://www.collectivites-locales.gouv.fr/sante-hygiene-securite-et-conditions-travail-dans-fonction-publique-territoriale" audience="Particuliers">
<Titre>Santé et sécurité dans la fonction publique territoriale</Titre>
<Source ID="R30602">Ministère en charge de la fonction publique</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R32249" URL="http://www.sante.gouv.fr/sante-et-securite-au-travail-dans-la-fonction-publique-hospitaliere,11777.html" audience="Particuliers">
<Titre>Santé et sécurité dans la fonction publique hospitalière</Titre>
<Source ID="R30662">Ministère en charge de la santé</Source>
</PourEnSavoirPlus>
<QuestionReponse ID="F18085" audience="Particuliers">Fonction publique : que sont le comité médical et la commission de réforme ?</QuestionReponse>
</Publication>
