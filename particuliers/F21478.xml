<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F21478" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Assurance habitation d'une colocation</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Argent</dc:subject>
<dc:description>Vous devez obligatoirement assurer un logement en colocation, au minimum contre les risques locatifs (incendie, explosion, dégâts des eaux). Vous devrez donc fournir une attestation d'assurance à votre propriétaire chaque année. Dans le cas contraire, le contrat de location pourrait être résilié.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-09-26</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F21478</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006475076&amp;cidTexte=LEGITEXT000006069108</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N44</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19803">Argent</Niveau>
<Niveau ID="N44">Assurance habitation</Niveau>
<Niveau ID="F21478" type="Fiche d'information">Assurance habitation d'une colocation</Niveau>
</FilDAriane>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
<SousThemePere ID="N20263">Assurance</SousThemePere><DossierPere ID="N44">
<Titre>Assurance habitation</Titre><SousDossier ID="N44-1">
<Titre>Vie du contrat</Titre>
<Fiche ID="F2591">Souscription d'un contrat</Fiche>
<Fiche ID="F2598">Tarifs, cotisations et durée du contrat</Fiche>
<Fiche ID="F2594">Modification du contrat</Fiche>
<Fiche ID="F19083">Résiliation du contrat</Fiche>
<Fiche ID="F3050">Recours et litiges</Fiche>
</SousDossier>
<SousDossier ID="N44-2">
<Titre>Assurance du locataire</Titre>
<Fiche ID="F1349">Assurance de base</Fiche>
<Fiche ID="F1350">Assurances complémentaires</Fiche>
<Fiche ID="F21478">Assurance et colocation</Fiche>
<Fiche ID="F10827">En cas de déménagement</Fiche>
</SousDossier>
<SousDossier ID="N44-3">
<Titre>Assurance du propriétaire</Titre>
<Fiche ID="F2023">Assurance du logement</Fiche>
<Fiche ID="F2027">Assurance et copropriété</Fiche>
<Fiche ID="F10830">En cas de vente ou d'achat du logement</Fiche>
</SousDossier>
<SousDossier ID="N44-4">
<Titre>Sinistre</Titre>
<Fiche ID="F2029">Sinistre courant</Fiche>
<Fiche ID="F2028">Cambriolage</Fiche>
<Fiche ID="F1352">Assurance et dégât des eaux</Fiche>
<Fiche ID="F21532">Incendie ou explosion</Fiche>
<Fiche ID="F3076">Catastrophes naturelles</Fiche>
<Fiche ID="F3078">Catastrophes technologiques</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Assurance du locataire</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Vous devez obligatoirement assurer un logement en colocation, au minimum  contre les risques locatifs (incendie, explosion, dégâts des eaux). Vous devrez donc fournir une attestation d'assurance à votre propriétaire chaque année. Dans le cas contraire, le contrat de location pourrait être résilié.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Qui doit assurer le logement ?</Paragraphe>
</Titre><Paragraphe>Au moins un colocataire doit être assuré pour les <Expression>risques locatifs</Expression>.</Paragraphe>
<Paragraphe>Cependant, en cas de sinistre, chaque colocataire devra participer à la réparation du préjudice en fonction de sa <LienIntra LienID="R1690" type="Définition de glossaire">quote-part</LienIntra> du loyer.</Paragraphe>
<Paragraphe>Il est important que chacun soit couvert, donc que chaque colocataire soit assuré.</Paragraphe>
<Paragraphe>Les colocataires et le propriétaire peuvent également se mettre d'accord pour que le propriétaire souscrive lui-même l'assurance. Il ajoutera 1/12<Exposant>ème</Exposant> de la cotisation annuelle au montant du loyer. </Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Contrat unique ou contrats individuels ?</Paragraphe>
</Titre><Paragraphe>Un contrat peut être souscrit pour couvrir tous les colocataires. Le nom de chacun d'entre eux sera alors indiqué dans le contrat.</Paragraphe>
<Paragraphe>Il est également possible à chacun de s'assurer individuellement. Dans ce cas, la souscription chez le même assureur peut limiter les litiges en cas de sinistre.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Quelle est l'étendue de la garantie ?</Paragraphe>
</Titre><Paragraphe>La garantie obligatoire <Expression>risques collectifs</Expression> :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>ne couvre pas les dommages causés aux voisins (par un dégât des eaux, par exemple),</Paragraphe>
</Item>
<Item>
<Paragraphe> et n'assure pas vos biens en cas de sinistre.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Pour  garantir ces risques, vous devrez souscrire une assurance  <LienInterne LienPublication="F1350" type="Fiche d'information" audience="Particuliers">multirisques habitation</LienInterne>  par exemple.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Que faire en cas de changement d'un des colocataires ?</Paragraphe>
</Titre><Paragraphe>Si un colocataire est remplacé, il faut faire un <LienIntra LienID="R10829" type="Définition de glossaire">avenant</LienIntra> au contrat de location.</Paragraphe>
<Paragraphe>De même, vous devrez faire un avenant si un seul contrat a été souscrit pour couvrir tous les colocataires.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R19751" type="Centre de contact">
<Titre>Assurance Banque Épargne Info Service</Titre>
<Complement>Pour des informations complémentaires</Complement><Texte>
						
								<Paragraphe>
			Informations sur les démarches et les relations contractuelles dans le domaine de l'assurance, de la banque et de l'épargne
		</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			<MiseEnEvidence>0 811 901 801</MiseEnEvidence>
</Paragraphe>
								<Paragraphe>
			Du lundi au vendredi de 8h à 18h.</Paragraphe>
								<Paragraphe>Numéro violet ou majoré : coût d'un appel vers un numéro fixe + service payant, depuis un téléphone fixe ou mobile</Paragraphe>
								<Paragraphe>Pour connaître le tarif, écoutez le message en début d'appel</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par courrier</Paragraphe>
</Titre>
								<Paragraphe>
			ABE Info Service
		</Paragraphe>
								<Paragraphe>
			61 rue Taitbout
		</Paragraphe>
								<Paragraphe>
			75436 Paris Cedex 09
		</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par messagerie</Paragraphe>
</Titre>
								<Paragraphe>Via le <LienExterne URL="http://www.abe-infoservice.fr/abe-info-service/nous-contacter.html">formulaire de contact</LienExterne>
</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R2699" type="Local personnalisable">
<Titre>Votre assureur</Titre>
<Complement>Pour des informations sur votre contrat d'assurance</Complement>
<PivotLocal>compagnie_assurance</PivotLocal>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006475076&amp;cidTexte=LEGITEXT000006069108" ID="R33998">
<Titre>Loi n°89-462 du 6 juillet 1989 tendant à améliorer les rapports locatifs : article 8</Titre>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R41789" URL="http://www.abe-infoservice.fr/assurance.html" audience="Particuliers">
<Titre>Informations pratiques sur l'assurance</Titre>
<Source ID="R39432">Autorité de contrôle prudentiel et de résolution (ACPR)</Source>
</PourEnSavoirPlus>
<Definition ID="R10829">
<Titre>Avenant</Titre>
<Texte>
				<Paragraphe>Document complémentaire du contrat constatant une modification, une adaptation ou un complément qui y sont apportés d'un commun accord entre les deux parties. </Paragraphe>
			</Texte>
</Definition>
<Definition ID="R1690">
<Titre>Quote-part (droit immobilier)</Titre>
<Texte>
				<Paragraphe>Fraction de la part des parties communes rattachée à un lot (appartement, local commercial, parking, cave...) selon la situation et la superficie de ce lot notamment. </Paragraphe>
			</Texte>
</Definition>
</Publication>
