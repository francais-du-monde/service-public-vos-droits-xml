<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F2007" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Bracelet électronique dans le cadre d'une assignation à résidence</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Justice</dc:subject>
<dc:description>L’assignation à résidence oblige la personne mise en cause à porter un bracelet électronique et à demeurer, à son domicile ou dans une autre résidence. Elle ne peut s’en absenter que dans les conditions et pour les motifs déterminés par le juge.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de la justice</dc:contributor>
<dc:date>modified 2014-11-19</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F2007</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000021331508&amp;idSectionTA=LEGISCTA000021331515&amp;cidTexte=LEGITEXT000006071154, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006182903&amp;cidTexte=LEGITEXT000006071154</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N263</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19807">Justice</Niveau>
<Niveau ID="N263">Procès pénal</Niveau>
<Niveau ID="F2007" type="Fiche d'information">Bracelet électronique dans le cadre d'une assignation à résidence</Niveau>
</FilDAriane>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
<SousThemePere ID="N271">Procédures judiciaires</SousThemePere><DossierPere ID="N263">
<Titre>Procès pénal</Titre><SousDossier ID="N263-1">
<Titre>Déclenchement de la procédure</Titre>
<Fiche ID="F1435">Plainte simple</Fiche>
<Fiche ID="F20798">Plainte avec constitution de partie civile</Fiche>
<Fiche ID="F1455">Citation directe</Fiche>
</SousDossier>
<SousDossier ID="N263-2">
<Titre>Enquête</Titre>
<Fiche ID="F14837">Garde à vue</Fiche>
<Fiche ID="F32326">Perquisition</Fiche>
<Fiche ID="F1456">Instruction</Fiche>
<Fiche ID="F1470">Mise en examen</Fiche>
</SousDossier>
<SousDossier ID="N263-3">
<Titre>Moyens de contrainte</Titre>
<Fiche ID="F2902">Contrôle judiciaire</Fiche>
<Fiche ID="F2007">Assignation à résidence avec surveillance électronique</Fiche>
<Fiche ID="F1042">Détention provisoire</Fiche>
</SousDossier>
<SousDossier ID="N263-4">
<Titre>Déroulement d'un procès</Titre>
<Fiche ID="F1457">Devant le tribunal de police</Fiche>
<Fiche ID="F1485">Devant le tribunal correctionnel</Fiche>
<Fiche ID="F1487">Devant la cour d'assises</Fiche>
<Fiche ID="F1489">Audition des témoins</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Moyens de contrainte</SousDossierPere>
<Introduction>
<Texte><Paragraphe>L’assignation à résidence oblige la personne mise en cause à porter un bracelet électronique et à demeurer, à son domicile ou dans une autre résidence.
		
			Elle ne peut s’en absenter que dans les conditions et pour les motifs déterminés par le juge.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Personnes concernées</Paragraphe>
</Titre><Paragraphe>L’assignation à résidence avec surveillance électronique (ARSE) concerne : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>la <LienInterne LienPublication="F1470" type="Fiche d'information" audience="Particuliers">personne mise en examen</LienInterne> dans le cadre d’une information judiciaire,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou la personne mise en cause dans le cadre d'une procédure de convocation par procès verbal ou de comparution immédiate en <LienInterne LienPublication="F32129" type="Fiche Question-réponse" audience="Particuliers">attendant son procès</LienInterne>.</Paragraphe>
</Item>
</Liste>
<Paragraphe>L’ARSE peut être ordonnée seulement lorsque toutes ces conditions sont réunies :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>les mesures de <LienInterne LienPublication="F2902" type="Fiche d'information" audience="Particuliers">contrôle judiciaire</LienInterne> sont insuffisantes au regard des nécessités de l’enquête ou comme mesure de sûreté,</Paragraphe>
</Item>
<Item>
<Paragraphe>lorsque la personne encourt une peine de prison d’au moins 2 ans. S'il s'agit d'un délit flagrant poursuivi en comparution immédiate, l'assignation peut concerner une personne encourant une peine d'au moins 6 mois.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Procédure</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Décision</Paragraphe>
</Titre><Paragraphe>L'ARSE peut être ordonnée, dans le cadre d’une information judiciaire :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>par le juge d'instruction ,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou par le juge des libertés et de la détention (JLD), à la place de la <LienInterne LienPublication="F1042" type="Fiche d'information" audience="Particuliers">détention provisoire</LienInterne>. </Paragraphe>
</Item>
</Liste>
<Paragraphe>Dans le cadre d'une enquête préliminaire, la décision est prise par le JLD.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Accord de l'intéressé</Paragraphe>
</Titre><Paragraphe>Le placement est ordonné avec l’accord de la personne intéressée, voire à sa demande (notamment pour mettre fin à une détention provisoire).</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Décision préalable</Paragraphe>
</Titre><Paragraphe>La décision du juge intervient après un débat contradictoire.</Paragraphe>
<Paragraphe>Le juge reçoit alors la personne mise en examen assistée de son avocat et recueille ses observations éventuelles.</Paragraphe>
<Paragraphe>Le juge l'informe :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>qu'il envisage de la placer sous assignation à résidence avec surveillance électronique mais que sa décision définitive n’interviendra qu’après un débat contradictoire,</Paragraphe>
</Item>
<Item>
<Paragraphe>et qu'elle a le droit de demander un délai pour préparer sa défense.</Paragraphe>
</Item>
</Liste>
<ANoter>
<Titre>À noter</Titre><Paragraphe>dans le cadre de la détention provisoire, l'ARSE peut être décidée sans débat contradictoire lorsque le juge statue sur une demande de mise en liberté.</Paragraphe>
</ANoter>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Contestation de la décision</Paragraphe>
</Titre><Paragraphe>L’ordonnance de placement sous assignation à résidence avec surveillance électronique peut être contestée devant la cour d’appel.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Formes de la surveillance électronique</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Durée</Paragraphe>
</Titre><Paragraphe>La durée initiale de placement est de 6 mois maximum. Cette durée peut être prolongée jusqu’à 2 ans maximum.</Paragraphe>
<Paragraphe>En cas de convocation par procès verbal, le JLD place la personne sous ARSE jusqu’à la date de son jugement qui ne peut pas intervenir au-delà d’un délai de 2 mois.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Surveillance électronique simple</Paragraphe>
</Titre><Paragraphe>La personne assignée à résidence doit rester en un lieu précis (son domicile par exemple) à certaines périodes (en dehors des heures de travail par exemple).</Paragraphe>
<Paragraphe> Elle porte un bracelet électronique intégrant un émetteur permettant de vérifier qu'elle se situe bien dans le lieu défini, au moment où elle doit s'y trouver. Un boîtier fixe est installé à l'intérieur du lieu en question.</Paragraphe>
<Paragraphe>Le dispositif ne permet pas de la localiser quand elle est libre de sortir.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Surveillance électronique mobile</Paragraphe>
</Titre><Paragraphe>La personne assignée à résidence porte un récepteur portable et un bracelet électronique intégrant un émetteur permettant à tout moment de la localiser. Un boîtier fixe est également installé sur le lieu d’assignation.</Paragraphe>
<Paragraphe>Comme pour le dispositif fixe, la personne doit rester en un lieu précis à certaines périodes. Mais elle reste contrôlée dans tous ses déplacements lorsqu'elle est sortie. Le bracelet permet ainsi de repérer si la personne s'approche de certains lieux interdits comme le domicile de la victime ou d'un complice.</Paragraphe>
<Paragraphe>Ce type de surveillance n’est possible que  :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>pour une infraction punie de plus de 7 ans d’ <LienIntra LienID="R18486" type="Définition de glossaire">d'emprisonnement</LienIntra> et pour laquelle le suivi socio-judiciaire est encouru,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou pour une infraction de violences volontaires ou de menaces, punies d'au moins 5 ans d'emprisonnement, commises soit contre :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>la personne avec qui le porteur du bracelet vit ou vivait en couple,</Paragraphe>
</Item>
<Item>
<Paragraphe>ses enfants ou ceux de son conjoint, concubin ou partenaire.</Paragraphe>
</Item>
</Liste>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Autres obligations</Paragraphe>
</Titre><Paragraphe>L’assignation à résidence avec surveillance électronique peut être accompagnée d’un contrôle judiciaire assorti d’obligations ou d’interdictions.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Exécution des obligations</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Contrôle</Paragraphe>
</Titre><Paragraphe>La personne surveillée est placée sous le contrôle :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>du juge d’instruction qui exerce les attributions du juge de l’application des peines,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou du procureur de la République dans les cas de saisine du tribunal sans information judiciaire.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Le contrôle à distance est assuré par des fonctionnaires de l'administration pénitentiaire.</Paragraphe>
<Paragraphe>Les services de police et de gendarmerie peuvent également constater l’absence de la personne assignée à résidence aux lieux et heures fixés. Ils en font alors rapport au juge d’instruction ou au procureur de la République.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Non-respect de l'ARSE</Paragraphe>
</Titre><Paragraphe>En cas de non respect des obligations, le juge d’instruction ou le procureur de la République peut saisir le JLD. La personne pourra être placée en détention provisoire.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Fin de l'ARSE</Paragraphe>
</Titre><Paragraphe>La personne surveillée peut demander à tout moment que soit prononcée la fin de l’assignation à résidence avec surveillance électronique (appelée <Citation>mainlevée</Citation>)</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>pendant la procédure d’instruction : devant le juge d’instruction qui décidera après avis du Procureur de la République. Le juge peut aussi y mettre fin d'office.</Paragraphe>
</Item>
<Item>
<Paragraphe>à l’issue de la procédure d’instruction en cas de renvoi : devant le tribunal correctionnel saisi,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou, en cas de convocation par procès verbal, devant le tribunal correctionnel saisi.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Le Procureur de la République peut la requérir à tout moment.</Paragraphe>
<Paragraphe>Une <LienInterne LienPublication="F13286" type="Fiche Question-réponse" audience="Particuliers">indemnisation</LienInterne> pourra être obtenue dans les mêmes conditions que celles d’une détention provisoire injustifiée.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R11624" type="Local personnalisé sur SP">
<Titre>Cour d'appel</Titre>
<Complement>Pour contester la décision</Complement>
<PivotLocal>cour_appel</PivotLocal>
<RessourceWeb URL="http://www.annuaires.justice.gouv.fr/annuaires-12162/annuaire-des-cours-dappel-21767.html"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<OuSAdresser ID="R11621" type="Local personnalisé sur SP">
<Titre>Maison de justice et du droit</Titre>
<Complement>Pour s'informer</Complement>
<PivotLocal>mjd</PivotLocal>
<RessourceWeb URL="http://www.annuaires.justice.gouv.fr/index.php?rubrique=10111"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<OuSAdresser ID="R14218" type="Local personnalisable">
<Titre>Avocat</Titre>
<Complement>Pour se faire assister</Complement>
<PivotLocal>avocat_conseil_national</PivotLocal>
<RessourceWeb URL="http://cnb.avocat.fr/Trouver-un-avocat-en-France_a341.html"/>
<Source ID="R30762">Conseil national des barreaux</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000021331508&amp;idSectionTA=LEGISCTA000021331515&amp;cidTexte=LEGITEXT000006071154" ID="R33120">
<Titre>Code de procédure pénale : articles 142-5 à 142-13</Titre>
<Complement>ARSE dans le cadre d'une information judiciaire</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006182903&amp;cidTexte=LEGITEXT000006071154" ID="R16418">
<Titre>Code de procédure pénale : articles 393 à 397-7</Titre>
<Complement>ARSE dans le cadre d'une enquête préliminaire</Complement>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R17506" URL="http://www.justice.gouv.fr/prison-et-reinsertion-10036/la-vie-hors-detention-10040/le-placement-sous-surveillance-electronique-11997.html" audience="Particuliers">
<Titre>Bracelet électronique : placement sous surveillance électronique</Titre>
<Source ID="R30663">Ministère en charge de la justice</Source>
</PourEnSavoirPlus>
<Definition ID="R18486">
<Titre>Emprisonnement</Titre>
<Texte>
				<Paragraphe>Peine de prison prononcée en cas de délit</Paragraphe>
			</Texte>
</Definition>
<InformationComplementaire ID="R18489" date="2014-09-12">
<Titre>Débat contradictoire</Titre>
<Texte>
				<Paragraphe>Chaque partie est en mesure d'exposer son point de vue et de discuter des preuves, faits, arguments (appelés <Expression>moyens</Expression>) qui sont présentés au juge.</Paragraphe>
			</Texte>
</InformationComplementaire>
</Publication>
