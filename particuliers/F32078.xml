<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F32078" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Représentant de la section syndicale</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>Chaque syndicat qui constitue une section syndicale dans l'entreprise d'au moins 50 salariés peut, s'il n'est pas représentatif, désigner un représentant. Le représentant de section agit au nom du syndicat auprès de l'employeur et assure la défense des salariés. Pour accomplir sa mission, il bénéficie d'un certain nombre de moyens. Il peut cumuler différents mandats.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-04-29</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F32078</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000019353699&amp;cidTexte=LEGITEXT000006072050</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N518</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N518">Représentation du personnel dans l'entreprise</Niveau>
<Niveau ID="F32078" type="Fiche d'information">Représentant de la section syndicale</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19966">Relations individuelles et collectives de travail</SousThemePere><DossierPere ID="N518">
<Titre>Représentation du personnel dans l'entreprise</Titre>
<Fiche ID="F102">Délégué syndical</Fiche>
<Fiche ID="F32078">Représentant de la section syndicale</Fiche>
<Fiche ID="F1972">Délégué du personnel</Fiche>
<Fiche ID="F96">Comité d'entreprise (CE)</Fiche>
<Fiche ID="F32085">Comité d'hygiène, de sécurité et des conditions de travail (CHSCT)</Fiche>
<Fiche ID="F1775">Délégation unique du personnel (DUP)</Fiche>
<Fiche ID="F33481">Instance unique</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Chaque syndicat qui constitue une section syndicale dans l'entreprise d'au moins 50 salariés peut, s'il n'est pas représentatif, désigner un représentant. Le représentant de section agit au nom du syndicat auprès de l'employeur et assure la défense des salariés.  Pour accomplir sa mission, il bénéficie d'un certain nombre de moyens. Il peut cumuler différents mandats. </Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Quel est le rôle du représentant de section syndicale ?</Paragraphe>
</Titre><Paragraphe>Le représentant de section syndicale représente son syndicat auprès de l'employeur et assure la défense des salariés.</Paragraphe>
<Paragraphe>Il peut notamment :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>formuler des propositions, des revendications ou des réclamations,</Paragraphe>
</Item>
<Item>
<Paragraphe>assister le salarié qui le souhaite lors d'un entretien préalable à une sanction disciplinaire,</Paragraphe>
</Item>
<Item>
<Paragraphe>assister les salariés auprès du <LienInterne LienPublication="F1667" type="Fiche Question-réponse" audience="Particuliers">conseil des prud'hommes</LienInterne>.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Il ne peut cependant pas  négocier d'accords collectifs avec l'employeur, contrairement au <LienInterne LienPublication="F102" type="Fiche d'information" audience="Particuliers">délégué syndical</LienInterne>.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Comment est-il désigné ?</Paragraphe>
</Titre><Paragraphe> Chaque syndicat qui constitue une section syndicale dans l'entreprise d'au moins 50 salariés peut, s'il n'est pas représentatif dans l'entreprise, désigner un représentant de section.</Paragraphe>
<Paragraphe>Chaque syndicat ne peut désigner qu'un seul représentant.</Paragraphe>
<Paragraphe>Pour être représentant de section syndicale, il faut :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>avoir 18 ans,</Paragraphe>
</Item>
<Item>
<Paragraphe>adhérer à la section syndicale, </Paragraphe>
</Item>
<Item>
<Paragraphe>travailler dans l'entreprise depuis un an minimum (ou 4 mois en cas de création d'entreprise ou d'ouverture d'établissement),</Paragraphe>
</Item>
<Item>
<Paragraphe>n'avoir fait l'objet d'aucune interdiction, déchéance ou  incapacité relative à ses droits civiques.</Paragraphe>
</Item>
</Liste>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>dans l'entreprise de moins de 50 salariés, un syndicat non représentatif peut désigner, pour la durée de son mandat, un <LienInterne LienPublication="F1972" type="Fiche d'information" audience="Particuliers">délégué du personnel</LienInterne> comme représentant de section syndicale.</Paragraphe>
</ASavoir>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Quels sont ses moyens d'action ?</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Crédit d'heures et déplacements</Paragraphe>
</Titre><Paragraphe>Le représentant de section syndicale dispose d'au moins 4 heures de délégation par mois pour se consacrer à l'exercice de sa fonction.</Paragraphe>
<Paragraphe>Durant ses heures de délégation, il peut se déplacer en dehors de l'entreprise. Il peut également circuler librement  dans l'entreprise et prendre les contacts nécessaires à l'accomplissement de sa mission, notamment auprès d'un salarié à son poste de travail (sous réserve de ne pas gêner  le travail des salariés).</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Communication</Paragraphe>
</Titre><Paragraphe>La section syndicale dispose d'un panneau d'affichage dans l'entreprise.</Paragraphe>
<Paragraphe>Elle peut organiser, en dehors du temps de travail, des réunions mensuelles pour les salariés.</Paragraphe>
<Paragraphe>Elle peut distribuer des tracts syndicaux.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Peut-il cumuler des mandats ?</Paragraphe>
</Titre><Paragraphe>La fonction de représentant d'une section syndicale est compatible avec celle de :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>délégué du personnel,</Paragraphe>
</Item>
<Item>
<Paragraphe>
<LienInterne LienPublication="F96" type="Fiche d'information" audience="Particuliers">représentant du personnel au comité d'entreprise ou d'établissement</LienInterne>, </Paragraphe>
</Item>
<Item>
<Paragraphe>représentant syndical au comité d'entreprise ou d'établissement.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Quand prend fin son mandat ?</Paragraphe>
</Titre><Paragraphe>Le mandat du représentant de section syndicale s'achève aux élections professionnelles suivantes. </Paragraphe>
<Paragraphe>Il peut aussi prendre fin notamment s'il démissionne  de son mandat ou de son emploi.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R783" type="Local personnalisable">
<Titre>Organisations syndicales</Titre>
<Complement>Pour obtenir des informations complémentaires</Complement>
<PivotLocal>syndicat</PivotLocal>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000019353699&amp;cidTexte=LEGITEXT000006072050" ID="R19548">
<Titre>Code du travail : articles L2142-1-1 à L2142-1-4</Titre>
<Complement>Représentant de la section syndicale</Complement>
</Reference>
<QuestionReponse ID="F2063" audience="Particuliers">Comment s'exerce le droit syndical dans l'entreprise ?</QuestionReponse>
<QuestionReponse ID="F1667" audience="Particuliers">Doit-on se présenter en personne devant le conseil de prud'hommes (CPH) ?</QuestionReponse>
</Publication>
