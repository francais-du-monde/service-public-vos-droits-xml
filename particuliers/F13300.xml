<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F13300" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Retraite de base des fonctionnaires : minimum garanti</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>Si le montant de votre pension de retraite de fonctionnaire est faible, celui-ci est augmenté, sous conditions, jusqu'à atteindre un montant minimum, appelé minimum garanti. Ce minimum garanti est ouvert sous conditions, et son montant varie en fonction du nombre d'années de services.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-01-19</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F13300</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006164392&amp;cidTexte=LEGITEXT000006070302, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000005753112</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N379</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N379">Retraite d'un agent de la fonction publique (titulaire et non titulaire)</Niveau>
<Niveau ID="F13300" type="Fiche d'information">Retraite de base des fonctionnaires : minimum garanti</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N20166">Retraite</SousThemePere><DossierPere ID="N379">
<Titre>Retraite d'un agent de la fonction publique (titulaire et non titulaire)</Titre><SousDossier ID="N379-1">
<Titre>Avant la retraite</Titre>
<Fiche ID="F13890">Droit à l'information du futur retraité</Fiche>
<Fiche ID="F1049">Rachat des années d'études</Fiche>
</SousDossier>
<SousDossier ID="N379-2">
<Titre>Âge de départ à la retraite</Titre>
<Fiche ID="F2786">Âge minimum de départ à la retraite du fonctionnaire</Fiche>
<Fiche ID="F594">Âge minimum de départ à la retraite du contractuel</Fiche>
<Fiche ID="F12395">Limite d'âge</Fiche>
<Fiche ID="F2114">Cessation progressive d'activité (CPA) du fonctionnaire</Fiche>
</SousDossier>
<SousDossier ID="N379-3">
<Titre>Départ à la retraite anticipé</Titre>
<Fiche ID="F13946">Carrière longue</Fiche>
<Fiche ID="F14060">Handicap</Fiche>
<Fiche ID="F20659">Fonctionnaire parent d'un enfant handicapé</Fiche>
</SousDossier>
<SousDossier ID="N379-4">
<Titre>Retraite à taux plein</Titre>
<Fiche ID="F1781">Taux plein du fonctionnaire</Fiche>
<Fiche ID="F2081">Taux plein du contractuel</Fiche>
</SousDossier>
<SousDossier ID="N379-5">
<Titre>Pension de retraite de base</Titre>
<Fiche ID="F21142">Calcul de la pension</Fiche>
<Fiche ID="F13736">Services pris en compte</Fiche>
<Fiche ID="F2095">Rétablissement au régime général</Fiche>
<Fiche ID="F16494">Majoration de la pension</Fiche>
<Fiche ID="F20349">Décote</Fiche>
<Fiche ID="F13300">Minimum garanti</Fiche>
<Fiche ID="F12402">Cumul emploi - retraite de base</Fiche>
</SousDossier>
<SousDossier ID="N379-6">
<Titre>Pension de retraite complémentaire</Titre>
<Fiche ID="F12387">Retraite complémentaire des fonctionnaires (RAFP)</Fiche>
<Fiche ID="F12390">Retraite complémentaire des agents non titulaires (Ircantec)</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Pension de retraite de base</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Si le montant de votre pension de retraite de fonctionnaire est faible, celui-ci est augmenté, sous conditions, jusqu'à atteindre un montant minimum, appelé minimum garanti. Ce minimum garanti est ouvert sous conditions, et son montant varie en fonction du nombre d'années de services.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>De quoi s'agit-il ?</Paragraphe>
</Titre><Paragraphe>Si vous percevez une pension de retraite de fonctionnaire, celle-ci ne doit pas être inférieure à un certain montant, appelé minimum garanti. Lorsque le montant de votre pension de retraite de base de fonctionnaire est inférieur au minimum garanti, votre pension est augmentée afin d'atteindre ce montant.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Conditions</Paragraphe>
</Titre><Paragraphe>Le minimum garanti s'applique à votre pension de retraite de fonctionnaire si vous vous trouvez dans l'une des situations suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>vous justifiez, lors de votre départ à la retraite, du <LienInterne LienPublication="F1781" type="Fiche d'information" audience="Particuliers">nombre de trimestres d'assurance requis</LienInterne> pour bénéficier d'une retraite à taux plein,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous avez, lors de votre départ à la retraite, atteint la <LienInterne LienPublication="F12395" type="Fiche d'information" audience="Particuliers">limite d'âge</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous avez, lors de votre départ à la retraite, atteint l'âge d'annulation de la <LienInterne LienPublication="F20349" type="Fiche d'information" audience="Particuliers">décote</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous êtes admis à la retraite pour invalidité d'origine <LienInterne LienPublication="F550" type="Fiche d'information" audience="Particuliers">professionnelle</LienInterne> ou non professionnelle,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous avez le droit de percevoir une retraite anticipée en tant que parent d'un <LienInterne LienPublication="F20659" type="Fiche d'information" audience="Particuliers">enfant invalide</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous avez le droit de bénéficier d'une <LienInterne LienPublication="F14060" type="Fiche d'information" audience="Particuliers">retraite anticipée en tant que fonctionnaire handicapé à 50%</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous avez le droit de bénéficier d'une <LienInterne LienPublication="F18217" type="Fiche Question-réponse" audience="Particuliers">retraite anticipée pour infirmité ou maladie incurable</LienInterne>.</Paragraphe>
</Item>
</Liste>
<Attention>
<Titre>Attention</Titre><Paragraphe>Si le montant total de vos pensions de retraite (de base et complémentaire, dans le public et dans le privé) dépasse <Valeur>1 135,73 €</Valeur>, le minimum garanti est réduit à hauteur du dépassement de cette somme. Cependant, cette réduction est effectuée dans la limite du montant de la pension auquel vous avez droit  sans application du minimum garanti.</Paragraphe>
</Attention>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Montant</Paragraphe>
</Titre><Paragraphe>Le montant du minimum garanti varie en fonction de votre nombre d'années de services en tant que fonctionnaire.</Paragraphe>
<BlocCas affichage="radio">
<Cas>
<Titre>
<Paragraphe>Vous avez au moins 40 ans de services </Paragraphe>
</Titre><Paragraphe>Le montant mensuel de votre pension ne peut pas être inférieur au montant du traitement indiciaire brut au 1er janvier 2004 de l'indice majoré 227 revalorisé depuis cette date dans les mêmes conditions que les pensions (soit <Valeur>1 158,06 €</Valeur>).</Paragraphe>

</Cas>
<Cas>
<Titre>
<Paragraphe>Vous avez entre 15 et 39 ans de services</Paragraphe>
</Titre><Paragraphe>Le montant mensuel du minimum garanti qui vous est applicable est déterminé de la façon suivante :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe> pour les 15 premières années de services, 57,5% du montant du traitement indiciaire brut au 1er janvier 2004 de l'indice majoré 227 revalorisé depuis cette date dans les mêmes conditions que les pensions  (soit <Valeur>665,88 €</Valeur>),</Paragraphe>
</Item>
<Item>
<Paragraphe>puis 2,5 points par année supplémentaire de services entre 15 et 30 ans,</Paragraphe>
</Item>
<Item>
<Paragraphe> et 0,5 point par année supplémentaire entre 30 et 39 ans.</Paragraphe>
</Item>
</Liste>

</Cas>
<Cas>
<Titre>
<Paragraphe>Vous avez moins de 15 ans de services</Paragraphe>
</Titre>
<BlocCas affichage="radio">
<Cas>
<Titre>
<Paragraphe>Cas général</Paragraphe>
</Titre><Paragraphe>Le montant mensuel du minimum garanti qui vous est applicable est déterminé d'après la formule suivante : (<Valeur>1 158,06 €</Valeur> x nombre d'années de services) / nombre de trimestres d'assurance requis pour bénéficier d'une retraite à taux plein.</Paragraphe>

</Cas>
<Cas>
<Titre>
<Paragraphe>Retraite pour cause d'invalidité</Paragraphe>
</Titre><Paragraphe>Le montant mensuel du minimum garanti qui vous est applicable est déterminé d'après la formule suivante : 1/15ème de <Valeur>665,88 €</Valeur> x nombre d'années de services.</Paragraphe>

</Cas>
</BlocCas>
</Cas>
</BlocCas>

</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006164392&amp;cidTexte=LEGITEXT000006070302" ID="R33014">
<Titre>Code des pensions civiles et militaires de retraite : article L17</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000005753112" ID="R13435">
<Titre>Décret n°2003-1306 du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la CNRACL</Titre>
<Complement>Articles 22 et 23</Complement>
</Reference>
<InformationComplementaire ID="R983" date="2015-10-12">
<Titre>Pourcentages et coefficients de revalorisation des pensions de retraite</Titre>
<Texte>
				<Tableau>
<Colonne largeur="21" type="normal"/>
<Colonne largeur="33" type="normal"/>
<Colonne largeur="44" type="normal"/>
<Rangée type="header">
						<Cellule>
							<Paragraphe>Date</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>Pourcentage</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>Coefficient</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> janvier 2005</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>2 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,02</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> janvier 2006</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>1,8 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,018</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> janvier 2007</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>1,8 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,018</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> janvier 2008</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>1,1 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,011</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> septembre 2008</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>0,8 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,008</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> avril 2009</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>1 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,01</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1<Exposant>er</Exposant> avril 2010</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>0,9 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,009</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe> 1<Exposant>er</Exposant> avril 2011</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>2,1 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,021</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe> 1<Exposant>er</Exposant> avril 2012</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>2,1 %</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,021</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe> 1<Exposant>er</Exposant> avril 2013</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,3 %</Valeur>
</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,013</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
<Rangée type="normal">
						<Cellule>
							<Paragraphe>1er octobre 2015</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>0,1 %</Valeur>
</Paragraphe>
						</Cellule>
						<Cellule>
							<Paragraphe>
<Valeur>1,001</Valeur>
</Paragraphe>
						</Cellule>
					</Rangée>
</Tableau>
			</Texte>
</InformationComplementaire>
</Publication>
