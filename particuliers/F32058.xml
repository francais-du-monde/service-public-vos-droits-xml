<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F32058" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Immatriculation de la copropriété</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Logement</dc:subject>
<dc:description>Afin de mieux connaître le parc des logements regroupés en copropriété, il est instauré un registre national d'immatriculation des copropriétés. Le syndic de copropriété doit procéder à cette immatriculation dans des délais variables selon la taille de la copropriété.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-08-08</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F32058</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000028779396&amp;cidTexte=LEGITEXT000006074096</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N31339</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19808">Logement</Niveau>
<Niveau ID="N31339">Documents de copropriété</Niveau>
<Niveau ID="F32058" type="Fiche d'information">Immatriculation de la copropriété</Niveau>
</FilDAriane>
<Theme ID="N19808">
<Titre>Logement</Titre>
</Theme>
<SousThemePere ID="N31337">Copropriété</SousThemePere><DossierPere ID="N31339">
<Titre>Documents de copropriété</Titre>
<Fiche ID="F2589">Règlement de copropriété</Fiche>
<Fiche ID="F2665">Carnet d'entretien de la copropriété</Fiche>
<Fiche ID="F32058">Immatriculation de la copropriété</Fiche>
<Fiche ID="F32059">Diagnostic technique de l'immeuble</Fiche>
<Fiche ID="F1615">Bilan énergétique d'un immeuble en copropriété (DPE et audit énergétique)</Fiche>
</DossierPere>
<Avertissement ID="R36462" date="2016-04-19">
<Titre>Immatriculation des copropriétés</Titre>
<Texte>
				<Paragraphe>La <LienExterne URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028772256&amp;dateTexte=&amp;categorieLien=id">loi Alur (accès au logement et urbanisme rénové)</LienExterne>
  prévoit l'immatriculation des syndicats de copropriétaires au sein d'un registre national dont les modalités de gestion seront définies par décret. </Paragraphe>
				<Paragraphe>Dans l'attente du texte d'application, les informations présentées dans cette page restent d'actualité. </Paragraphe>
			</Texte>
</Avertissement>

<Introduction>
<Texte><Paragraphe>Afin de mieux connaître le parc des logements regroupés en copropriété, il est instauré un registre national d'immatriculation des copropriétés. Le syndic de copropriété doit procéder à cette immatriculation dans des délais variables selon la taille de la copropriété. </Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>De quoi s'agit-il ?</Paragraphe>
</Titre><Paragraphe>L’enregistrement des copropriétés sur le registre national consiste à attribuer un numéro d'immatriculation à chaque copropriété. </Paragraphe>
<Paragraphe>Il permet de faciliter la connaissance de l'état des copropriétés et prévenir la survenance de dysfonctionnements.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Informations à déclarer</Paragraphe>
</Titre><Paragraphe>Le <LienInterne LienPublication="F2608" type="Fiche d'information" audience="Particuliers">syndic</LienInterne> est tenu de déclarer certaines données sur le registre national d'immatriculation des copropriétés. </Paragraphe>
<Paragraphe>C'est à lui d'accomplir les formalités nécessaires à cette déclaration et à ses modifications ultérieures éventuelles.</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Données concernant le syndicat de copropriétaires</Paragraphe>
</Titre><Paragraphe>Les informations suivantes doivent figurer au registre :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>nom, adresse et date de création du <LienInterne LienPublication="F2606" type="Fiche d'information" audience="Particuliers">syndicat de copropriétaires</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>nombre et nature des <LienIntra LienID="R38693" type="Définition de glossaire">lots de copropriété</LienIntra>,</Paragraphe>
</Item>
<Item>
<Paragraphe>nom du syndic,</Paragraphe>
</Item>
<Item>
<Paragraphe>existence d'un <LienInterne LienPublication="F20388" type="Fiche d'information" audience="Particuliers">mandataire ad hoc</LienInterne> ou d'un <LienInterne LienPublication="F2643" type="Fiche d'information" audience="Particuliers">administrateur provisoire</LienInterne> de la copropriété,</Paragraphe>
</Item>
<Item>
<Paragraphe>existence d'un arrêté ou d'une injonction administrative dans le cas d'un <LienInterne LienPublication="F16158" type="Fiche d'information" audience="Particuliers">immeuble insalubre</LienInterne>.</Paragraphe>
</Item>
</Liste>
<ANoter>
<Titre>À noter</Titre><Paragraphe>à l'issue de chaque exercice comptable, les données relatives à la gestion et aux comptes du syndicat doivent également être portées au sein du registre.</Paragraphe>
</ANoter>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Données concernant l'immeuble</Paragraphe>
</Titre><Paragraphe>Les données essentielles relatives à l'état de l'immeuble doivent par ailleurs être déclarées au sein du registre, notamment les informations issues :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>du <LienInterne LienPublication="F2665" type="Fiche d'information" audience="Particuliers">carnet d'entretien</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>du <LienInterne LienPublication="F32059" type="Fiche d'information" audience="Particuliers">diagnostic technique global</LienInterne> s'il existe.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Délais de déclaration</Paragraphe>
</Titre><Paragraphe>Les copropriétés doivent être déclarées au sein du registre national d'immatriculation dans des délais qui varient selon la taille de la copropriété.</Paragraphe>
<Tableau>
<Titre>Délais de déclaration des immeubles au registre d'immatriculation des copropriétés</Titre>
<Colonne largeur="26" type="normal"/>
<Colonne largeur="50" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Taille de la copropriété </Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Date limite d'immatriculation</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>+ de 200 lots</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>31 décembre 2016</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>+ de 50 lots et jusqu'à 200 lots</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>31 décembre 2017</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Jusqu'à 50 lots</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>31 décembre 2018</Paragraphe>
</Cellule>
</Rangée>
</Tableau>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Conséquences de l'absence d'immatriculation</Paragraphe>
</Titre><Paragraphe>En l'absence d'immatriculation de la copropriété, le syndic peut être <LienIntra LienID="R2705" type="Définition de glossaire">mis en demeure</LienIntra> de le faire par :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>n'importe quel copropriétaire,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou toute personne ayant un intérêt. </Paragraphe>
</Item>
</Liste>
<Paragraphe>Si le syndic n'immatricule pas la copropriété dans un délai d'1 mois suivant la mise en demeure, il peut se voir appliquer une astreinte de <Valeur>20 €</Valeur> par lot de copropriété et par semaine de retard.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>le montant de cette astreinte peut être facturé aux copropriétaires uniquement lorsque la copropriété est administrée par un syndic bénévole.</Paragraphe>
</ASavoir><Paragraphe>L'absence d'immatriculation ou d'actualisation des données déclarées empêche le syndicat de copropriétaires de bénéficier de certaines subventions (<LienInterne LienPublication="F1328" type="Fiche d'information" audience="Particuliers">Anah</LienInterne>, <LienInterne LienPublication="F19905" type="Fiche d'information" audience="Particuliers">éco-prêt à taux zéro</LienInterne>).</Paragraphe>

</Chapitre>
</Texte><VoirAussi important="non">
<Dossier ID="N31339" audience="Particuliers">
<Titre>Documents de copropriété</Titre>
<Theme ID="N19808">
<Titre>Logement</Titre>
</Theme>
</Dossier>
<Fiche ID="F32059" audience="Particuliers">
<Titre>Diagnostic technique de l'immeuble</Titre>
<Theme ID="N19808">
<Titre>Logement</Titre>
</Theme>
</Fiche>
</VoirAussi>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000028779396&amp;cidTexte=LEGITEXT000006074096" ID="R37175">
<Titre>Code de la construction et de l'habitation : articles L711-1 à L711-7</Titre>
<Complement>Caractéristiques de l'immatriculation des copropriétés</Complement>
</Reference>
<Definition ID="R2705">
<Titre>Mise en demeure</Titre>
<Texte>
				<Paragraphe>Acte par lequel un créancier exige du débiteur le versement d'une somme sous peine de versement de dommages et intérêts. La mise en demeure peut être notifiée par lettre recommandée avec accusé de réception ou par acte d'huissier.</Paragraphe>
			</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006436328&amp;cidTexte=LEGITEXT000006070721" ID="R41071">
<Titre>Code civil : article 1139</Titre>
</Reference>
</Definition>
<Definition ID="R38693">
<Titre>Lot de copropriété</Titre>
<Texte>
				<Paragraphe>Bien situé au sein d'un immeuble en copropriété : appartement, local commercial, cave, comble, garage...</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F32912" audience="Particuliers">Quelles informations trouve-t-on dans le bilan énergétique d'une copropriété ?</QuestionReponse>
</Publication>
