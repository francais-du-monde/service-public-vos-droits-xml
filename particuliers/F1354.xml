<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F1354" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Qu'est-ce que le prêt sur gage ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Argent</dc:subject>
<dc:description>Le prêt sur gage permet d'obtenir un prêt qui sera garanti par un objet de valeur : un bijou, un tableau... Si vous remboursez le prêt, cet objet vous sera rendu.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-04-20</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F1354</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006170620&amp;cidTexte=LEGITEXT000006072026, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020001270&amp;cidTexte=LEGITEXT000006072026, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020001241&amp;cidTexte=LEGITEXT000006072026</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N96</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19803">Argent</Niveau>
<Niveau ID="N96">Crédit à la consommation</Niveau>
<Niveau ID="F1354" type="Fiche Question-réponse">Qu'est-ce que le prêt sur gage ?</Niveau>
</FilDAriane>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
<SousThemePere ID="N20369">Crédit et surendettement</SousThemePere><DossierPere ID="N96">
<Titre>Crédit à la consommation</Titre><SousDossier ID="N96-1">
<Titre>Différents types de crédit</Titre>
<Fiche ID="F2434">Crédit affecté</Fiche>
<Fiche ID="F2435">Prêt personnel</Fiche>
<Fiche ID="F2436">Crédit renouvelable ou revolving</Fiche>
<Fiche ID="F2437">Location avec option d'achat (LOA)</Fiche>
<Fiche ID="F2438">Crédit gratuit</Fiche>
<Fiche ID="F2471">Carte privative (carte de crédit)</Fiche>
<Fiche ID="F16242">Prêt viager hypothécaire</Fiche>
<Fiche ID="F21375">Microcrédit personnel</Fiche>
<Fiche ID="F986">Prêt étudiant garanti par l'État</Fiche>
</SousDossier>
<SousDossier ID="N96-2">
<Titre>Gestion du crédit</Titre>
<Fiche ID="F2440">Information préalable de l'emprunteur</Fiche>
<Fiche ID="F2451">Contrat de crédit</Fiche>
<Fiche ID="F2457">Assurance emprunteur</Fiche>
</SousDossier>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Le prêt sur gage permet d'obtenir un prêt qui sera garanti par un objet de valeur : un bijou, un tableau...  Si vous remboursez le prêt, cet objet vous sera rendu.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>De quoi s'agit-il ?</Paragraphe>
</Titre><Paragraphe>Le prêt sur gage est destiné aux personnes ne pouvant ou ou souhaitant pas demander un crédit classique notamment pour des raisons financières. Par exemple, parce que vos revenus ne vous permettent de payer les mensualités.</Paragraphe>
<Paragraphe>Dans ce cas, vous pouvez demander un prêt sur gage. Il n'y a pas de conditions de ressources pour y accéder.</Paragraphe>
<Paragraphe>Vous  pourrez obtenir un prêt en échange du dépôt d'un objet de valeur qui servira de garantie. On parle d'objet mis en gage. Vous restez propriétaire de l'objet.</Paragraphe>
<Paragraphe>Le bien faisant l'objet d'un prêt sur gage peut être un bijou, une montre, un tableau, un instrument de musique ou tout autre objet de valeur. </Paragraphe>
<Paragraphe>Si vous ne remboursez pas votre prêt, l'objet mis en gage pourra être vendu aux enchères.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>si vous avez de faibles revenus, il est également possible d'obtenir un <LienInterne LienPublication="F21375" type="Fiche d'information" audience="Particuliers">micro-prêt</LienInterne>.</Paragraphe>
</ASavoir>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Démarches</Paragraphe>
</Titre><Paragraphe>Pour obtenir, un prêt sur gage, vous devez vous rendre dans un établissement du Crédit municipal.</Paragraphe><OuSAdresser ID="R34162" type="National">
<Titre>Caisse de Crédit Municipal de France</Titre>
<RessourceWeb URL="http://www.pretsurgage.fr/liste-des-centres/"/>
</OuSAdresser>

<Paragraphe>Seul le Crédit municipal propose ce type de prêt. Vous ne pouvez pas faire la démarche auprès de votre banque habituelle.</Paragraphe>
<Paragraphe>Il vous faudra présenter :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>une pièce d'identité,</Paragraphe>
</Item>
<Item>
<Paragraphe>un justificatif de domicile,</Paragraphe>
</Item>
<Item>
<Paragraphe>et  l'objet que vous  souhaitez apporter en garantie.</Paragraphe>
</Item>
</Liste>
<Paragraphe>La valeur de cet objet  est évalué par un commissaire-priseur.</Paragraphe>
<Paragraphe>En échange du dépôt de cet objet, vous pourrez obtenir immédiatement un prêt.</Paragraphe>
<Paragraphe>Si le prêt est accordé, vous aurez un contrat à compléter et vous recevrez la somme convenue en échange de l'objet.</Paragraphe>
<Paragraphe>Si le prêt est refusé (ou que vous refusiez le contrat), l'objet vous est restitué.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>pour certains objets, un justificatif de propriété pourra vous être nécessaire. Une facture à votre nom par exemple.</Paragraphe>
</ASavoir>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Montant du prêt</Paragraphe>
</Titre><Paragraphe>Le prêt que vous pouvez obtenir est fonction de l'objet que vous déposez. </Paragraphe>
<Paragraphe>Son montant est généralement compris entre 50 et 70 % de la valeur estimée de l'objet.</Paragraphe>
<Paragraphe>La taux d'intérêt peut varier en fonction de la somme empruntée.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Coût</Paragraphe>
</Titre><Paragraphe>Le taux d'intérêt peut varier en fonction de la somme empruntée.</Paragraphe>
<Paragraphe>Des frais de garde de l'objet peuvent également s'ajouter.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Absence de droit de rétractation</Paragraphe>
</Titre><Paragraphe>Le droit de rétractation ne s'applique pas au prêt sur gage. Une fois que vous avez signé votre contrat, vous ne pouvez plus revenir dessus.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Remboursement du prêt</Paragraphe>
</Titre><Paragraphe>Le contrat peut durer jusqu'à 2 ans. Vous pouvez cependant récupérer votre bien à tout moment, en remboursant le prêt et les intérêts.</Paragraphe>
<Paragraphe>Au terme d'une année, vous pouvez prolonger le contrat, à condition de  régler le montant des intérêts.</Paragraphe>
<Paragraphe>Si vous ne le voulez pas ou ne pouvez pas le faire, l'objet peut être vendu aux enchères.</Paragraphe>
<Paragraphe>Le produit de la vente sert à rembourser le prêt et les intérêts. Comme vous restez propriétaire de l'objet, le surplus vous est reversé si l'objet est vendu pour plus cher que la somme à rembourser. En revanche, si la vente ne couvre pas le montant du prêt et des intérêts, il ne vous sera rien demandé.</Paragraphe>

</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006170620&amp;cidTexte=LEGITEXT000006072026" ID="R19868">
<Titre>Code monétaire et financier : article L514-1</Titre>
<Complement>Mission du Crédit municipal</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020001270&amp;cidTexte=LEGITEXT000006072026" ID="R2450">
<Titre>Code monétaire et financier : articles D514-1 à D514-9</Titre>
<Complement>Principes du prêt sur gage</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020001241&amp;cidTexte=LEGITEXT000006072026" ID="R2661">
<Titre>Code monétaire et financier : articles D514-12 à D514-15</Titre>
<Complement>Conservation de l'objet mis en gage</Complement>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R43990" URL="http://www.pretsurgage.fr" audience="Particuliers">
<Titre>Site d'information sur le prêt sur gage</Titre>
<Source ID="R43991">Crédit Municipal</Source>
</PourEnSavoirPlus>
</Publication>
