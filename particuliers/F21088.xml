<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F21088" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Peut-on contester une loi au cours d'un procès ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Justice</dc:subject>
<dc:description>Le Conseil constitutionnel est alors saisi de cette question prioritaire de constitutionnalité (QPC)</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de la justice</dc:contributor>
<dc:date>modified 2014-07-24</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F21088</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006071194#LEGIARTI000019241077</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N259</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19807">Justice</Niveau>
<Niveau ID="N259">Contestation d'un jugement</Niveau>
<Niveau ID="F21088" type="Fiche Question-réponse">Peut-on contester une loi au cours d'un procès ?</Niveau>
</FilDAriane>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
<SousThemePere ID="N271">Procédures judiciaires</SousThemePere><DossierPere ID="N259">
<Titre>Contestation d'un jugement</Titre>
<Fiche ID="F1384">Faire appel d'un jugement civil ou pénal</Fiche>
<Fiche ID="F1386">Faire opposition à un jugement</Fiche>
<Fiche ID="F1381">Demander la révision d'une décision de justice</Fiche>
<Fiche ID="F1382">Saisir la Cour de cassation</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>À l’occasion d’un procès devant une juridiction administrative ou judiciaire, si vous estimez qu’une disposition législative porte atteinte aux droits et libertés garantis par la Constitution, vous pouvez la contester. Selon une procédure particulière, . Si le Conseil constitutionnel estime que la disposition contestée n’est pas conforme à la Constitution, son application sera écartée du procès.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Quelle est la procédure ?</Paragraphe>
</Titre><Paragraphe>La demande doit être présentée au juge sous forme d’un document écrit et motivé.</Paragraphe>
<Paragraphe>Elle peut être présentée à tout moment, sauf en matière criminelle où la question ne pourra être soulevée que durant l’ <LienInterne LienPublication="F1456" type="Fiche d'information" audience="Particuliers">instruction</LienInterne> ou lors de l'appel d'un arrêt de la cour d'assises et pas durant le <LienInterne LienPublication="F1487" type="Fiche d'information" audience="Particuliers">procès</LienInterne> lui-même.</Paragraphe>
<Paragraphe>La juridiction saisie doit statuer sans délai et vérifier que :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>la question présente un caractère sérieux,</Paragraphe>
</Item>
<Item>
<Paragraphe>la disposition contestée est applicable au litige ou à la procédure, ou constitue le fondement des poursuites,</Paragraphe>
</Item>
<Item>
<Paragraphe>la question n’a pas déjà été posée au Conseil constitutionnel.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si toutes ces conditions sont remplies, la juridiction saisie transmet la question de constitutionnalité, selon l'appartenance de la question à l'ordre administratif ou judiciaire, au Conseil d'État ou à la Cour de cassation.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Quel est le rôle du Conseil d'État ou de la Cour de cassation ?</Paragraphe>
</Titre><Paragraphe>Le Conseil d’État ou  la Cour de cassation doit dire si la QPC est recevable ou non. Ils ont 3 mois pour rendre leur décision.</Paragraphe>
<Paragraphe>S'ils estiment à leur tour que la question est recevable, ils la transmettent au Conseil constitutionnel.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Quel est le rôle du Conseil constitutionnel ?</Paragraphe>
</Titre><Paragraphe>Dès réception de la demande, le Conseil constitutionnel avise le Président de la République, le Premier ministre et les présidents du Sénat et de l’Assemblée nationale. Ceux-ci adresseront au Conseil constitutionnel leurs observations sur la question prioritaire de constitutionnalité qui leur est soumise.</Paragraphe>
<Paragraphe>Le Conseil constitutionnel statue dans un délai de 3 mois.</Paragraphe>
<Paragraphe>S’il juge la disposition inconstitutionnelle, celle-ci est abrogée à compter de la publication de cette décision et son application est écartée du procès en cours.</Paragraphe>
<Attention>
<Titre>Attention</Titre><Paragraphe>la juridiction saisie ne pourra pas statuer sur le litige jusqu'à la décision du Conseil d'État ou de la Cour de cassation, puis, s'il a été saisi, de celle du Conseil constitutionnel.</Paragraphe>
</Attention>
</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006071194#LEGIARTI000019241077" ID="R33115">
<Titre>Constitution du 4 octobre 1958 : article 61-1</Titre>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R12126" URL="http://www.conseil-constitutionnel.fr/conseil-constitutionnel/root/bank_mm/QPC/plaquette_qpc.pdf" audience="Particuliers" format="application/pdf" poids="402.7 KB">
<Titre>La question prioritaire de constitutionnalité (QPC)</Titre>
<Source ID="R30735">Conseil constitutionnel</Source>
</PourEnSavoirPlus>
</Publication>
