<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F2064" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Allocations chômage : comment est fixé le salaire journalier de référence ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Social - Santé</dc:subject>
<dc:description>Le salaire journalier de référence, qui sert à calculer vos allocations chômage, est déterminé en fonction d'un salaire annuel de référence.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-02-12</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F2064</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029150768</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N178</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19811">Social - Santé</Niveau>
<Niveau ID="N178">Allocation chômage d'aide au retour à l'emploi (ARE)</Niveau>
<Niveau ID="F2064" type="Fiche Question-réponse">Allocations chômage : comment est fixé le salaire journalier de référence ?</Niveau>
</FilDAriane>
<Theme ID="N19811">
<Titre>Social - Santé</Titre>
</Theme>
<SousThemePere ID="N461">Chômage</SousThemePere><DossierPere ID="N178">
<Titre>Allocation chômage d'aide au retour à l'emploi (ARE)</Titre>
<Fiche ID="F14860">Conditions d'attribution et démarche</Fiche>
<Fiche ID="F1447">Montant</Fiche>
<Fiche ID="F1776">Durée du versement</Fiche>
<Fiche ID="F1465">Cumul avec des revenus d'activité</Fiche>
</DossierPere>

<Texte><Paragraphe>Le <LienIntra LienID="R31070" type="Définition de glossaire">salaire annuel de référence</LienIntra> est calculé à partir des rémunérations brutes  des 12 mois civils précédant votre dernier jour travaillé payé.</Paragraphe>
<Paragraphe>Les jours pendant lesquels vous n'avez été sous contrat avec un employeur au cours de ces 12 mois, les jours d'absence non payés et, les jours non rémunérés ne sont pas pris en compte.</Paragraphe>
<Paragraphe>Pour chaque mois, les rémunérations prises en compte ne peuvent pas dépasser <Valeur>12 872 €</Valeur>.</Paragraphe>
<Paragraphe>Les rémunérations perçues en dehors des 12 mois civils précédant votre dernier jour travaillé payé mais se rapportant à cette période sont prises en compte.</Paragraphe>
<Paragraphe> Les indemnités de 13<Exposant>è</Exposant> mois, les primes de bilan, les gratifications perçues au cours des 12 mois civils précédant votre dernier jour travaillé payé ne sont retenues que pour la fraction relative à cette période.</Paragraphe>
<Paragraphe>En revanche, sont notamment exclues :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>les indemnités de licenciement ou de départ,</Paragraphe>
</Item>
<Item>
<Paragraphe>les indemnités de rupture conventionnelle,</Paragraphe>
</Item>
<Item>
<Paragraphe>les indemnités compensatrices de congés payés,</Paragraphe>
</Item>
<Item>
<Paragraphe>les indemnités de préavis ou de non-concurrence,</Paragraphe>
</Item>
<Item>
<Paragraphe>les indemnités journalières de la Sécurité sociale.</Paragraphe>
</Item>
<Item>
<Paragraphe>et  toutes sommes qui ne trouvent pas leur contrepartie dans l'exécution du contrat de travail.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Le salaire journalier de référence est égal au salaire annuel de référence divisé par le nombre de jours au titre desquels les rémunérations prises en compte pour son calcul  ont été perçues.</Paragraphe>
<Paragraphe>Exemple : si vous avez travaillé du 1<Exposant>er</Exposant> juillet 2014 au 28 février 2015, c'est-à-dire 243 jours, et avez perçu <Valeur>25 000 €</Valeur> de rémunération brute, votre salaire journalier de référence est : 25 000/243 = <Valeur>102,89 € €</Valeur>.</Paragraphe>

</Texte><OuSAdresser ID="R194" type="Local personnalisé sur SP">
<Titre>Pôle emploi</Titre>
<Complement>Pour plus d'information</Complement>
<PivotLocal>pole_emploi</PivotLocal>
<RessourceWeb URL="http://www.pole-emploi.fr/informations/votre-pole-emploi-@/votre_pole_emploi/" commentaireLien="Pôle emploi - réseau local"/>
<Source ID="R30668">Pôle emploi</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029150768" ID="R785">
<Titre>Arrêté du 25 juin 2014 portant agrément de la convention du 14 mai 2014 relative à l'indemnisation du chômage et les textes qui lui sont associés</Titre>
<Complement>Articles 13, 14, 43 du règlement général annexé</Complement>
</Reference>
<Definition ID="R31070">
<Titre>Salaire de référence</Titre>
<Texte>
				<Paragraphe>Salaire reconstitué à partir de rémunérations perçues pendant une période donnée. Permet par exemple le calcul du montant d'une indemnité.</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F12386" audience="Particuliers">Comment est indemnisé un agent public en cas de chômage ?</QuestionReponse>
<QuestionReponse ID="F2761" audience="Particuliers">Médiateur de Pôle emploi : comment y recourir ?</QuestionReponse>
<QuestionReponse ID="F31197" audience="Particuliers">Les allocations chômage peuvent-elles être réduites ou supprimées ?</QuestionReponse>
<QuestionReponse ID="F31592" audience="Particuliers">Comment doit-on rembourser les allocations chômage perçues à tort ?</QuestionReponse>
<QuestionReponse ID="F31225" audience="Particuliers">Licenciement pour inaptitude physique : quand débute le versement de l'ARE ?</QuestionReponse>
</Publication>
