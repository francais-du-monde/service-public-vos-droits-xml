<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F740" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Dépendance à la drogue : prise en charge des toxicomanes</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Social - Santé</dc:subject>
<dc:description>Un(e) toxicomane peut bénéficier d'une cure de désintoxication. La justice peut également prononcer une injonction thérapeutique.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-05-12</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F740</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000021940301&amp;idSectionTA=LEGISCTA000006171214&amp;cidTexte=LEGITEXT000006072665</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N437</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19811">Social - Santé</Niveau>
<Niveau ID="N437">Addictions</Niveau>
<Niveau ID="F740" type="Fiche d'information">Dépendance à la drogue : prise en charge des toxicomanes</Niveau>
</FilDAriane>
<Theme ID="N19811">
<Titre>Social - Santé</Titre>
</Theme>
<SousThemePere ID="N20180">Santé</SousThemePere><DossierPere ID="N437">
<Titre>Addictions</Titre>
<Fiche ID="F20104">Alcool</Fiche>
<Fiche ID="F740">Drogues</Fiche>
<Fiche ID="F160">Tabac</Fiche>
<Fiche ID="F15814">Jeux d'argent</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>La toxicomanie est la dépendance aux drogues : cannabis, héroïne, cocaïne... La prise en charge médicale peut se faire soit à l'initiative de la personne toxicomane elle-même, soit à la suite d'un signalement.  Un(e) toxicomane peut bénéficier d'une cure de désintoxication. La justice peut également prononcer une injonction thérapeutique.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>À la demande d'un(e) toxicomane</Paragraphe>
</Titre><Paragraphe>Une personne toxicomane est une personne dépendante aux drogues (cocaïne, cannabis, héroïne...). </Paragraphe>
<Paragraphe>Pour lutter contre sa dépendance, elle peut se rendre dans un <LienIntra LienID="R41570" type="National">service médical spécialisé</LienIntra>. Elle pourra ainsi bénéficier d'une cure de désintoxication si elle le souhaite. Dans ce cas, la personne toxicomane peut bénéficier de l'anonymat et ne sera pas poursuivie en justice pour usage de drogues.</Paragraphe><OuSAdresser ID="R41570" type="National">
<Titre>Services spécialisés d'aide aux toxicomanes</Titre>
<RessourceWeb URL="http://www.drogues-info-service.fr/Tout-savoir-sur-les-drogues/Se-faire-aider/L-aide-specialisee#.Vd8lH_ntlBc"/>
<Source ID="R30860">Mission interministérielle de lutte contre les drogues et les conduites addictives (Mildeca)</Source>
</OuSAdresser>


</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>À la demande d'un professionnel</Paragraphe>
</Titre><Paragraphe>Un médecin ou une assistante sociale peuvent signaler le cas d'une personne toxicomane à l'Agence régionale de santé - ARS (ex-direction départementale des affaires sanitaires et sociales - Ddass).</Paragraphe>
<Paragraphe>L'ARS ordonne la réalisation d'une enquête sur la vie familiale, professionnelle et sociale de la personne et demande un examen médical :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>si l'examen révèle une dépendance, la personne doit suivre une cure de désintoxication dans l'établissement de son choix. À défaut, l'ARS peut en désigner un d'office ;</Paragraphe>
</Item>
<Item>
<Paragraphe>si l'examen médical ne révèle pas de dépendance, la personne est invitée à demeurer sous surveillance médicale. Cette surveillance sera effectuée par un médecin choisi par la personne concernée pendant une durée définie.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Ordonnée par la justice (injonction thérapeutique)</Paragraphe>
</Titre><Paragraphe>La justice pénale peut demander à un(e) toxicomane de se faire soigner. Ces soins sont appelés <Expression>injonction thérapeutique</Expression> (ou <Expression>injonction de soins</Expression>). Ils peuvent comprendre une cure de désintoxication. Ils peuvent être ordonnés dans le cadre :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>d'une <LienInterne LienPublication="F2277" type="Fiche d'information" audience="Particuliers">mesure alternative</LienInterne> aux poursuites pénales,</Paragraphe>
</Item>
<Item>
<Paragraphe>d'une <LienInterne LienPublication="F1406" type="Fiche d'information" audience="Particuliers">peine complémentaire</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou d'un <LienInterne LienPublication="F1531" type="Fiche d'information" audience="Particuliers">sursis avec mise à l'épreuve</LienInterne>.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Le magistrat ou le tribunal qui a prononcé l'injonction thérapeutique adresse la personne à un médecin relais. </Paragraphe>
<Paragraphe>Ce médecin réalise un 1<Exposant>er</Exposant> examen pour déterminer la nécessité de la mesure prononcée. Si la nécessité de la mesure est confirmée, un autre médecin choisi par la personne concernée assurera la mise en œuvre des soins. Le médecin relais contrôlera le bon déroulement de la mesure et informera la justice de l'évolution de la dépendance de la personne concernée.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R20696" type="Centre de contact">
<Titre>Drogues info service</Titre>
<Complement>Pour obtenir des renseignements, notamment sur les établissements proposant un suivi</Complement>
<Source ID="R30662">Ministère en charge de la santé</Source><Texte>
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			0 800 23 13 13 (appel gratuit)
		</Paragraphe>
								<Paragraphe>
			Ouvert de 8h à 2h, 7 jours/7</Paragraphe>
								<Paragraphe>
			Service anonyme. </Paragraphe>
								<Paragraphe>Informations sur les drogues, l'alcool, les dépendances y compris la dépendance aux jeux. </Paragraphe>
								<Paragraphe>Écoute, soutien, conseils et orientations. </Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par internet</Paragraphe>
</Titre>
								<Paragraphe>
			Accès à <LienExterne URL="http://www.drogues-info-service.fr/?-Vos-questions-nos-reponses-">la rubrique Vos questions/ Nos réponses</LienExterne>
 pour poser des questions aux professionnels du service</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000021940301&amp;idSectionTA=LEGISCTA000006171214&amp;cidTexte=LEGITEXT000006072665" ID="R41571">
<Titre>Code de la santé publique : articles L3411-1 à L3425-2</Titre>
<Complement>Mesures de lutte contre la toxicomanie</Complement>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R41572" URL="http://www.drogues.gouv.fr/" audience="Particuliers">
<Titre>Site d'information sur les drogues</Titre>
<Source ID="R30860">Mission interministérielle de lutte contre les drogues et les conduites addictives (Mildeca)</Source>
</PourEnSavoirPlus>
<QuestionReponse ID="F33341" audience="Particuliers">Que risque-t-on pour usage de drogues ?</QuestionReponse>
<QuestionReponse ID="F17535" audience="Particuliers">Comment se débarrasser des seringues usagées ?</QuestionReponse>
</Publication>
