<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F13512" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Européen en France : entrée et séjour de moins de 3 mois</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Étranger - Europe</dc:subject>
<dc:description>Si vous êtes citoyen d'un pays de l'Espace économique européen (EEE) ou suisse, vous pouvez circuler et séjourner librement pendant une période de 3 mois en France. Vous pouvez être accompagné par les membres de votre famille proche. Ce droit de libre circulation et de séjour jusqu'à 3 mois vous est reconnu quel que soit le motif de votre séjour : tourisme, stage, emploi de courte durée... Il peut toutefois être limité.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2015-07-01</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F13512</dc:identifier>
<dc:source>http://circulaire.legifrance.gouv.fr/pdf/2011/04/cir_32884.pdf, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006163221&amp;cidTexte=LEGITEXT000006070158, http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006334969&amp;idSectionTA=LEGISCTA000006147746&amp;cidTexte=LEGITEXT000006070158, http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000024540182&amp;idSectionTA=LEGISCTA000006163245&amp;cidTexte=LEGITEXT000006070158</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N123</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19804">Étranger - Europe</Niveau>
<Niveau ID="N123">Européens en France</Niveau>
<Niveau ID="F13512" type="Fiche d'information">Européen en France : entrée et séjour de moins de 3 mois</Niveau>
</FilDAriane>
<Theme ID="N19804">
<Titre>Étranger - Europe</Titre>
</Theme>
<SousThemePere ID="N20306">Étrangers en France</SousThemePere><DossierPere ID="N123">
<Titre>Européens en France</Titre><SousDossier ID="N123-1">
<Titre>Résider et travailler en France</Titre>
<Fiche ID="F13512">Entrée et séjour de moins de 3 mois</Fiche>
<Fiche ID="F2651">Séjour de plus de 3 mois en tant qu'actif</Fiche>
<Fiche ID="F12017">Séjour de plus de 3 mois en tant qu'inactif</Fiche>
<Fiche ID="F21841">Séjour de plus de 3 mois d'un étudiant</Fiche>
<Fiche ID="F12136">Demandeur d'emploi</Fiche>
</SousDossier>
<SousDossier ID="N123-2">
<Titre>Vivre avec sa famille en France</Titre>
<Fiche ID="F2653">Installer son époux, ses enfants et ascendants</Fiche>
<Fiche ID="F21830">Installer d'autres membres de famille (concubin, partenaire, frère à charge...)</Fiche>
<Fiche ID="F21849">Installation de l'Européen époux de Français</Fiche>
<Fiche ID="F21858">Installation de l'Européen pacsé avec un Français</Fiche>
</SousDossier>
<SousDossier ID="N123-3">
<Titre>Obtenir une carte de séjour</Titre>
<Fiche ID="F16003">Carte "UE" les 5 premières années pour l'Européen</Fiche>
<Fiche ID="F22116">Carte "UE-séjour permanent" après 5 ans de séjour pour l'Européen</Fiche>
<Fiche ID="F19315">Carte "UE" les 5 premières années pour l'époux, les enfants majeurs, ascendants</Fiche>
<Fiche ID="F22117">Carte "UE-séjour permanent" après 5 ans pour l'époux, les enfants et ascendants</Fiche>
<Fiche ID="F847">Perte de sa carte de séjour</Fiche>
<Fiche ID="F22118">Vol de sa carte de séjour</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Résider et travailler en France</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Si vous êtes citoyen d'un pays de l'Espace économique européen (EEE) ou suisse, vous pouvez circuler et séjourner librement pendant une période de 3 mois en France. Vous pouvez être accompagné par les membres de votre famille proche. Ce droit de libre circulation et de séjour jusqu'à 3 mois vous est reconnu quel que soit le motif de votre séjour : tourisme, stage, emploi de courte durée... Il peut toutefois être limité.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Entrée et séjour de l'Européen ou du Suisse</Paragraphe>
</Titre><Paragraphe>Vous pouvez entrer et séjourner jusqu'à 3 mois en France sans formalité particulière.</Paragraphe>
<Paragraphe>Vous devez simplement vous munir d'un passeport ou d'un titre d'identité en cours de validité, en cas de contrôle d'identité sur le territoire.</Paragraphe>
<Paragraphe>La possession d'un de ces 2 documents vous permet de séjourner librement en France.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Entrée et séjour de la famille</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Famille européenne ou suisse accompagnante</Paragraphe>
</Titre><Paragraphe>Les membres européens ou suisses de votre famille   peuvent, comme vous, entrer et séjourner librement en France jusqu'à 3 mois avec un titre d'identité ou un passeport  valide.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Famille non européenne accompagnante</Paragraphe>
</Titre><Paragraphe>Vous pouvez être accompagné ou rejoint en France par :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>votre époux,</Paragraphe>
</Item>
<Item>
<Paragraphe>vos enfants de moins de 21 ans ou à charge,</Paragraphe>
</Item>
<Item>
<Paragraphe>vos <LienIntra LienID="R12668" type="Définition de glossaire">ascendants</LienIntra> directs à charge,</Paragraphe>
</Item>
<Item>
<Paragraphe>et les enfants ou ascendants directs à charge de votre époux.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si vous êtes étudiant, vous ne pouvez toutefois pas faire venir vos ascendants.</Paragraphe>
<Paragraphe>Pour entrer en France, les membres non européens de votre famille doivent détenir :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>un titre de séjour en cours de validité, délivré en tant que membre de famille d'Européen par un autre pays de l'EEE ou la Suisse,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou un passeport valide</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>revêtu d'un visa de court séjour,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou, s'ils sont <LienInterne LienPublication="F21921" type="Fiche Question-réponse" audience="Particuliers">dispensés de visa en raison de leur nationalité</LienInterne>, d'un document établissant leur lien familial.</Paragraphe>
</Item>
</Liste>
</Item>
</Liste>
<Paragraphe>À l'appui de sa <LienInterne LienPublication="R1476" type="Formulaire" audience="Particuliers">demande de visa</LienInterne>, le membre de votre famille doit prouver son lien familial avec vous.</Paragraphe>
<Paragraphe>L'ambassade ou le consulat délivre gratuitement, et dans les meilleurs délais, le visa.</Paragraphe>
<Paragraphe>Toute décision de refus de visa doit être motivée, sauf si des motifs intéressant la sûreté de l'État s'y opposent.</Paragraphe>
<Paragraphe>L'administration française doit  faciliter vos démarches ou celles de votre famille, si vous ne disposez pas de document d'identité, de voyage ou d'entrée.</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>
<LienInterne LienPublication="F21830" type="Fiche d'information" audience="Particuliers">d'autres membres de famille</LienInterne> peuvent aussi être autorisés, après examen de leur situation, à entrer et séjourner  avec vous
        en France (par exemple concubin ou partenaire).</Paragraphe>
</ANoter>
</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Exercice d'une activité professionnelle</Paragraphe>
</Titre><Paragraphe>Si vous souhaitez exercer une activité salariée ou non salariée en France, vous n'avez pas besoin de titre de séjour, ni de titre de travail.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>depuis le 1er juillet 2015, cette règle vous est aussi applicable si vous êtes  travailleur croate. </Paragraphe>
</ASavoir>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Limitations au droit de circulation et de séjour</Paragraphe>
</Titre><Paragraphe>Votre liberté de circuler et de séjourner  et de celle de votre famille en France n'est pas illimitée.</Paragraphe>
<Paragraphe>Dans des cas très exceptionnels, <LienInterne LienPublication="F32514" type="Fiche Question-réponse" audience="Particuliers">l'entrée en France peut  vous être interdite</LienInterne>. </Paragraphe>
<Paragraphe>Par ailleurs, votre séjour peut être <LienInterne LienPublication="F13517" type="Fiche Question-réponse" audience="Particuliers">remis en cause</LienInterne> :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>si vous représentez une menace particulière grave pour l'ordre public,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou si vous devenez une charge déraisonnable pour le système d'assistance sociale français, notamment l'assurance maladie et l'aide sociale. </Paragraphe>
<Paragraphe>Le recours à des mesures d'assistance sociale n'entraîne toutefois pas automatiquement une mesure d'éloignement.</Paragraphe>
</Item>
</Liste>

</Chapitre>
</Texte><OuSAdresser ID="R100" type="Centre de contact">
<Titre>Europe Direct</Titre>
<Complement>Pour toute information</Complement><Texte>
						
								<Paragraphe>
			Informations générales sur l'Europe et sur les organismes à contacter. Réponses dans toutes les langues officielles de l'Union Européenne.
		</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Liste type="puce">
									<Item>
										<Paragraphe>
			<MiseEnEvidence>00 800 67 89 10 11</MiseEnEvidence> depuis un pays de l'Union européenne</Paragraphe>
										<Paragraphe> (numéro vert, appel gratuit depuis un téléphone fixe ou mobile) </Paragraphe>
									</Item>
								</Liste>
								<Liste type="puce">
									<Item>
										<Paragraphe>
<MiseEnEvidence>+32 2 299 96 96</MiseEnEvidence> depuis un pays hors Union européenne (appel facturé au tarif international)
		</Paragraphe>
									</Item>
								</Liste>
								<Paragraphe>
			Du lundi au vendredi de 9h à 18h</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par messagerie</Paragraphe>
</Titre>
								<Paragraphe>
			
      Accès à l'<LienExterne URL="http://europa.eu/europedirect/write_to_us/index_fr.htm">adresse de messagerie</LienExterne>
 d'Europe Direct</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par dialogue en ligne</Paragraphe>
</Titre>
								<Paragraphe> Accès au
      <LienExterne URL="http://europa.eu/europedirect/web_assistance/index_fr.htm">service d'assistance
      en ligne</LienExterne>
 d'Europe Direct</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R2750" type="Local personnalisable">
<Titre>Ambassade ou consulat français à l'étranger</Titre>
<Complement>Pour s'informer et demander un visa pour le membre de famille non européen s'il est d'une nationalité soumise à visa</Complement>
<PivotLocal>ambassade_france_etranger</PivotLocal>
<RessourceWeb URL="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/"/>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://circulaire.legifrance.gouv.fr/pdf/2011/04/cir_32884.pdf" ID="R11670" format="application/pdf" poids="1.5 MB">
<Titre>Circulaire du 10 septembre 2010 sur le droit de séjour des citoyens européens et suisses ainsi que des membres de leur famille</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006163221&amp;cidTexte=LEGITEXT000006070158" ID="R33037">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles L211-2 à L211-2-2</Titre>
<Complement>Motivation des refus de visa</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006334969&amp;idSectionTA=LEGISCTA000006147746&amp;cidTexte=LEGITEXT000006070158" ID="R33036">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles L121-1 à L121-5</Titre>
<Complement>Conditions d'entrée en France et séjour jusqu'à 3 mois</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000024540182&amp;idSectionTA=LEGISCTA000006163245&amp;cidTexte=LEGITEXT000006070158" ID="R33038">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles R121-1 à R121-2-1</Titre>
<Complement>Papiers pour entrer en France</Complement>
</Reference>
<Definition ID="R12668">
<Titre>Ascendant</Titre>
<Texte>
				<Paragraphe>Parents, grands-parents et arrière-grands-parents d'une personne</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F21921" audience="Particuliers">Un étranger peut-il séjourner en Europe sans visa ?</QuestionReponse>
<QuestionReponse ID="F32514" audience="Particuliers">Un Européen peut-il être interdit d'entrée en France ?</QuestionReponse>
<QuestionReponse ID="F13517" audience="Particuliers">Un Européen peut-il être renvoyé de France ?</QuestionReponse>
<InformationComplementaire ID="R1030" date="2016-06-30">
<Titre>Pays de l'Espace économique européen (EEE)</Titre>
<Texte>
				<Paragraphe>Allemagne,
			Autriche, Belgique,
			Bulgarie,
			Chypre, Croatie, Danemark,
			Espagne,
			Estonie, Finlande, France,
			Grèce,
			Hongrie,
			Irlande, Islande,
			Italie, Lettonie, Liechtenstein,
			Lituanie,
			Luxembourg,
			Malte,
			Norvège,
			Pays-Bas,
			Pologne,
			Portugal,
			République tchèque,
			Roumanie,
			Royaume-Uni,
			Slovaquie, Slovénie,
			Suède</Paragraphe>
			</Texte>
</InformationComplementaire>
</Publication>
