<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F11007" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Garantie légale des vices cachés</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Argent</dc:subject>
<dc:description>Lorsque vous achetez un produit, le vendeur (ou le fabricant) doit vous garantir contre ses défauts cachés. La garantie s'applique à condition que le défaut soit caché, rende le produit impropre à l'usage auquel on le destine et existe à la date d'acquisition. Vous avez 2 ans pour agir et bénéficier de la garantie, sans frais.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2015-04-14</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F11007</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006165624&amp;cidTexte=LEGITEXT000006070721, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029958952</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N31164</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19803">Argent</Niveau>
<Niveau ID="N31164">Garanties</Niveau>
<Niveau ID="F11007" type="Fiche d'information">Garantie légale des vices cachés</Niveau>
</FilDAriane>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
<SousThemePere ID="N20154">Consommation</SousThemePere><DossierPere ID="N31164">
<Titre>Garanties</Titre>
<Fiche ID="F11094">Garantie légale de conformité</Fiche>
<Fiche ID="F11007">Garantie légale des vices cachés</Fiche>
<Fiche ID="F11093">Garantie commerciale</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Lorsque vous achetez un produit, le vendeur (ou le fabricant) doit vous garantir contre ses défauts cachés. La garantie s'applique à condition que le défaut soit caché, rende le produit impropre à l'usage auquel on le destine et existe à la date d'acquisition. Vous avez 2 ans pour agir et bénéficier de la garantie, sans frais.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>De quoi s'agit-il ?</Paragraphe>
</Titre><Paragraphe>La garantie des vices cachées vous permet d'être protégé contre les défauts cachés du produit que vous achetez et qui en empêchent l'usage ou l'affectent à un point tel que vous ne l'auriez pas acquise.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>depuis mars 2015, les conditions générales de vente (CGV) doivent inclure une information  sur la garantie et sa mise en œuvre.</Paragraphe>
</ASavoir>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>À quelles conditions s'applique-t-elle ?</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Ventes concernées </Paragraphe>
</Titre><Paragraphe>La garantie des vices cachés s'applique :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>Quel que soit le bien acheté (neuf ou d'occasion, en promotion, etc.), <LienIntra LienID="R1185" type="Définition de glossaire">mobilier</LienIntra> ou <LienIntra LienID="R10833" type="Définition de glossaire">immobilier</LienIntra>
</Paragraphe>
</Item>
<Item>
<Paragraphe>Quel que soit le vendeur (professionnel ou simple particulier)</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Conditions liées au défaut du bien</Paragraphe>
</Titre><Paragraphe>Pour faire  jouer la garantie des vices cachés, le défaut du bien doit remplir les 3 conditions suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>Être caché, c'est à dire non apparent lors de l'achat</Paragraphe>
</Item>
<Item>
<Paragraphe>Rendre le bien impropre à l'usage auquel on le destine ou diminuer très fortement son usage</Paragraphe>
</Item>
<Item>
<Paragraphe>Exister au moment de l'achat</Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Comment la mettre en œuvre ?</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Preuve du vice caché</Paragraphe>
</Titre><Paragraphe>C'est à vous de prouver l'existence du vice caché. À cet effet, vous pouvez produire les différentes attestations ou devis de réparation.</Paragraphe>
<Paragraphe>Vous pouvez aussi faire procéder à une expertise. Pour trouver un expert, vous pouvez demander à un tribunal proche de votre domicile la <LienIntra LienID="R37308" type="Local">liste des experts agréés auprès des tribunaux</LienIntra>.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Délai pour agir </Paragraphe>
</Titre><Paragraphe>Vous avez 2 ans à partir de la découverte du défaut caché pour mettre en œuvre la garantie et saisir la justice.</Paragraphe>
<Paragraphe>Vous pouvez rapporter le bien au vendeur (ou au fabricant) ou le prévenir par courrier, de préférence <LienInterne LienPublication="R33564" type="Modèle de document" audience="Particuliers">par lettre recommandée avec avis de réception</LienInterne>. Vous devez pouvoir présenter des justificatifs : bon de livraison, devis, ticket de caisse, etc.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Remboursement partiel ou total du prix</Paragraphe>
</Titre><Paragraphe>Vous avez le choix entre 2 solutions :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>Garder le produit et demander une réduction du prix</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>Ou rendre le produit et demander le remboursement du prix payé ainsi que des frais occasionnés par la vente</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Dommages et intérêts en cas de mauvaise foi du vendeur</Paragraphe>
</Titre>
<Paragraphe>Si le vendeur connaissait le défaut du produit que vous avez acheté,  il doit, en plus de vous rembourser le prix, vous verser des dommages et intérêts.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Que faire en cas de litige ?</Paragraphe>
</Titre><Paragraphe>Si vous rencontrez des difficultés (par exemple réparations mal effectuées, délai de réparation non respecté), vous disposez de droits et de voies de recours, <LienExterne URL="http://www.economie.gouv.fr/dgccrf/Recourir-a-la-mediation-ou-a-la-conciliation">amiables</LienExterne>

 et <LienInterne LienPublication="F2289" type="Fiche avec liens externes" audience="Particuliers">judiciaires</LienInterne>.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><VoirAussi important="non">
<Fiche ID="F24042" audience="Particuliers">
<Titre>Protection du consommateur</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
<Fiche ID="F11094" audience="Particuliers">
<Titre>Garantie légale de conformité</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
<Fiche ID="F11093" audience="Particuliers">
<Titre>Garantie commerciale</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
</VoirAussi>
<OuSAdresser ID="R20797" type="Centre de contact">
<Titre>Direction générale de la concurrence, de la consommation et de la répression des fraudes (DGCCRF)</Titre>
<Complement>Pour tout renseignement sur la garantie de conformité</Complement><Texte>
						
								<Paragraphe>
			Pour toute question de consommation, de qualité et sécurité des produits et services</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			<MiseEnEvidence>3939</MiseEnEvidence> (coût : <Valeur>0,15 €</Valeur> TTC la minute)
		</Paragraphe>
								<Paragraphe>
			Du lundi au vendredi de 8h30 à 19h</Paragraphe>
								<Paragraphe>
			Depuis l'étranger ou hors métropole : <MiseEnEvidence>+33 1 73 60 39 39</MiseEnEvidence>  uniquement depuis un poste fixe (coût d'une communication + coût de l'appel international variable selon les pays)</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par messagerie</Paragraphe>
</Titre>
								<Paragraphe>
			
      Accès au
      <LienExterne URL="http://www.economie.gouv.fr/courrier/4169">formulaire de contact</LienExterne>

</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R37308" type="Local">
<Titre>Expert judiciaire</Titre>
<Complement>Pour faire procéder à une expertise</Complement>
<RessourceWeb URL="http://www.courdecassation.fr/informations_services_6/listes_experts_judiciaires_8700.html#experts"/>
<Source ID="R30633">Cour de cassation</Source>
</OuSAdresser>
<OuSAdresser ID="R33535" type="National">
<Titre>Commission de la sécurité des consommateurs</Titre>
<Complement>Pour contacter ou alerter la CSC lorsque vous avez acheté un produit s'avérant dangereux</Complement>
<Source ID="R33534">Commission de la sécurité des consommateurs</Source><Texte>
						<Chapitre>
<Titre>
<Paragraphe>Par courrier</Paragraphe>
</Titre>
								<Paragraphe>59, Boulevard Vincent Auriol</Paragraphe>
								<Paragraphe>Télédoc 000</Paragraphe>
								<Paragraphe>75703 Paris cedex 13</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par messagerie</Paragraphe>
</Titre>
								<Liste type="puce">
									<Item>
										<Paragraphe>Accès au <LienExterne URL="http://www.securiteconso.org/formulaire/">formulaire "Alerter la Commission"</LienExterne>
 pour signaler  un problème de sécurité ou un accident</Paragraphe>
									</Item>
									<Item>
										<Paragraphe>Accès au <LienExterne URL="http://www.securiteconso.org/la-csc/nous-contacter/">formulaire "Contact"</LienExterne>
 pour tout autre demande</Paragraphe>
									</Item>
								</Liste>
							</Chapitre>
					</Texte>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006165624&amp;cidTexte=LEGITEXT000006070721" ID="R33576">
<Titre>Code civil : articles 1641 à 1649</Titre>
<Complement>Définition et fonctionnement de la garantie des défauts cachés</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029958952" ID="R39215">
<Titre>Arrêté du 18 décembre 2014 relatif au contenu des conditions générales de vente en matière de garantie légale</Titre>
</Reference>
<ServiceEnLigne ID="R33564" URL="http://www.conso.net/content/le-lave-linge-achet%C3%A9-neuf-pr%C3%A9sente-un-d%C3%A9faut-vous-demandez-l%E2%80%99application-de-la-garantie" type="Modèle de document">
<Titre>Demander l'application de la garantie légale des vices cachés pour un bien acheté neuf et défectueux</Titre>
<Source ID="R30643">Institut national de la consommation (INC)</Source>
</ServiceEnLigne>
<PourEnSavoirPlus type="Information pratique" ID="R33538" URL="http://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques/Les-Garanties" audience="Particuliers">
<Titre>Garanties légales, garantie commerciale et service après-vente</Titre>
<Source ID="R30656">Ministère en charge de l'économie</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R2944" URL="http://www.economie.gouv.fr/dgccrf/Publications/Vie-pratique/Fiches-pratiques-de-la-concurrence-et-de-la-consom" audience="Particuliers">
<Titre>Fiches pratiques de la concurrence et de la consommation</Titre>
<Source ID="R30656">Ministère en charge de l'économie</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R3132" URL="http://www.conso.net/page/bases.3_associations.1_presentation./" audience="Particuliers">
<Titre>Les associations nationales de défense des consommateurs</Titre>
<Source ID="R30643">Institut national de la consommation (INC)</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R41754" URL="http://www.conso.net" commentaireLien="Portail de la consommation" audience="Particuliers">
<Titre>Site Conso.net</Titre>
<Source ID="R30643">Institut national de la consommation (INC)</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R41649" URL="http://www.securiteconso.org/" audience="Particuliers">
<Titre>Site de la Commission de la sécurité des consommateurs</Titre>
<Source ID="R33534">Commission de la sécurité des consommateurs</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R41753" URL="http://www.clauses-abusives.fr" audience="Particuliers">
<Titre>Site de la Commission des clauses abusives</Titre>
<Source ID="R30682">Commission des clauses abusives</Source>
</PourEnSavoirPlus>
<Definition ID="R10833">
<Titre>Bien immeuble</Titre>
<Texte>
				<Paragraphe>Bien ne pouvant être déplacé (un terrain ou un appartement par exemple) ou objet en faisant partie intégrante (la clôture du terrain par exemple)</Paragraphe>
			</Texte>
</Definition>
<Definition ID="R1185">
<Titre>Bien meuble</Titre>
<Texte>
				<Paragraphe>Désigne 2 catégories de biens : corporels (objets pouvant être déplacés, par exemple un véhicule) et incorporels (par exemple droits d'auteur, parts sociales)</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F18954" audience="Particuliers">Quelles garanties s'appliquent après l'achat d'un produit ?</QuestionReponse>
</Publication>
