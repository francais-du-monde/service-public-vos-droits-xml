<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F1526" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Infraction sexuelle sur majeur : viol, agression</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Justice</dc:subject>
<dc:description>Une infraction sexuelle est une atteinte sexuelle commise sans le consentement de la victime. L'agresseur encourt une peine de prison</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de la justice</dc:contributor>
<dc:date>modified 2014-07-25</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F1526</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006165281&amp;cidTexte=LEGITEXT000006070719, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006181753&amp;cidTexte=LEGITEXT000006070719, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006181754&amp;cidTexte=LEGITEXT000006070719</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N19681</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19807">Justice</Niveau>
<Niveau ID="N19681">Violence - Atteinte à l'intégrité</Niveau>
<Niveau ID="F1526" type="Fiche d'information">Infraction sexuelle sur majeur : viol, agression</Niveau>
</FilDAriane>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
<SousThemePere ID="N20326">Infractions</SousThemePere><DossierPere ID="N19681">
<Titre>Violence - Atteinte à l'intégrité</Titre>
<Fiche ID="F1526">Viol, agression sexuelle sur majeur</Fiche>
<Fiche ID="F1524">Violences volontaires et involontaires</Fiche>
<Fiche ID="F12544">Violences conjugales</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Une agression sexuelle est une atteinte sexuelle commise par un individu sans le consentement de la personne agressée (victime). La victime a des droits et peut porter plainte contre son agresseur. Ce dernier encourt une peine d'emprisonnement ou de réclusion criminelle .</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Actes visés</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Viol et agression sexuelle</Paragraphe>
</Titre><Paragraphe>Une agression sexuelle est une atteinte sexuelle commise avec violence, contrainte, menace ou surprise. Il peut s"agir, par exemple, d’attouchements, de caresses de nature sexuelle ou de viol.</Paragraphe>
<Paragraphe>Le viol se distingue des autres agressions sexuelles en ce qu’il suppose un acte de pénétration sexuelle, de quelque nature qu'il soit, commis également avec violence, contrainte, menace ou surprise.</Paragraphe>
<Paragraphe>Tout acte de pénétration sexuelle est visé : vaginale, anale ou buccale, notamment par le sexe de l'auteur. Il peut aussi s’agir de pénétrations digitales (avec le doigt) ou de pénétration au moyen d’un objet.</Paragraphe>
<Paragraphe>La contrainte suppose l’existence de pressions physiques ou morales. Par exemple, la contrainte peut résulter de la différence d’âge existant entre l’auteur des faits et une victime mineure et de l’autorité qu’exerce celui-ci sur cette victime.</Paragraphe>
<Paragraphe>Il y a recours à la menace lorsque l'auteur annonce des représailles en cas de refus de la victime.</Paragraphe>
<Paragraphe>Il y a recours à la surprise lorsque l'auteur utilise un stratagème pour surprendre sa victime ou encore lorsque la victime était inconsciente ou en état d’alcoolémie.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Absence de consentement de la victime</Paragraphe>
</Titre><Paragraphe>Pour caractériser un viol ou une autre agression sexuelle, il faut établir que l’auteur :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>a eu également l’intention de commettre cet acte,</Paragraphe>
</Item>
<Item>
<Paragraphe>et a eu conscience d’imposer ses agissements à la victime sans son consentement.</Paragraphe>
</Item>
</Liste>
<Paragraphe>L’absence de consentement de la victime à l’acte peut être prouvée quelles que soient les relations entre l’auteur et la victime. Une agression sexuelle peut donc même être caractérisée entre époux, concubins, partenaires liés par un pacte civil de solidarité (Pacs) ou encore entre personnes appartenant à la même famille.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Dépôt de plainte</Paragraphe>
</Titre><Paragraphe>Le <LienInterne LienPublication="F1435" type="Fiche d'information" audience="Particuliers">dépôt de plainte</LienInterne> peut se faire contre une personne précise ou contre X si l'agresseur est inconnu de la victime.</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Délais de prescription</Paragraphe>
</Titre><Paragraphe>La victime dispose d'un certain délai pour porter plainte.</Paragraphe>
<Paragraphe>Si la victime est majeure, ce délai est de :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>10 ans en cas de viol,</Paragraphe>
</Item>
<Item>
<Paragraphe>3 ans en cas d'autres infractions sexuelles comme des attouchements.</Paragraphe>
</Item>
</Liste>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>ces délais sont rallongés si la victime <LienInterne LienPublication="F2274" type="Fiche d'information" audience="Particuliers">était mineure</LienInterne> au moment des faits.</Paragraphe>
</ASavoir>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Examen par un médecin</Paragraphe>
</Titre><Paragraphe>À la demande des policiers ou gendarmes, la victime sera alors examinée par un médecin si le dépôt de plainte a lieu peu de temps après l'agression.</Paragraphe>
<Paragraphe>Dans la mesure du possible, il est préférable que la victime ne procède à aucune ablution avant son examen (douche, bain).</Paragraphe>
<Paragraphe>Le médecin doit lui dispenser tous les soins nécessaires et lui délivrer un certificat médical indiquant son état.</Paragraphe>
<Paragraphe>Le médecin ou la victime transmettra ce certificat aux officiers de police ou de gendarmerie en charge de l’enquête.</Paragraphe>
<Paragraphe>Il sera alors conseillé à la victime de réaliser un test de dépistage de maladie vénérienne, du VIH et, le cas échéant, un test de grossesse.</Paragraphe>
<Paragraphe>En cas de résultat positif, la victime pourra transmettre un certificat médical constatant son état aux services de police ayant enregistré la déposition.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Constitution de partie civile</Paragraphe>
</Titre><Paragraphe>En <LienInterne LienPublication="F1454" type="Fiche Question-réponse" audience="Particuliers">se constituant partie civile,</LienInterne> la victime peut être indemnisée du préjudice subi résultant de l’agression.</Paragraphe>
<Paragraphe>Pour évaluer le préjudice, les souffrances physiques et psychiques, les frais médicaux exposés et les dégâts matériels lors de l’agression seront notamment pris en compte.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Assistance de la victime</Paragraphe>
</Titre><Paragraphe>Il est important de ne pas affronter ces épreuves seul. </Paragraphe>
<Paragraphe>Un avocat et une association spécialisée, notamment une association d'aide aux victimes, peuvent assister la victime dans ses démarches.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Procès et peines encourues</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Procès à huis clos</Paragraphe>
</Titre><Paragraphe>À la demande de la victime, la cour d'assises prononce obligatoirement le huis clos en cas de viol ou de tortures et actes de barbarie accompagnés d'agressions sexuelles.  Il peut aussi être ordonné d'office par la cour si la victime ne s'y oppose pas.</Paragraphe>
<Paragraphe>Pour les autres agressions sexuelles, le huis clos reste à l'appréciation du tribunal.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Peines encourues</Paragraphe>
</Titre><Paragraphe>Les peines encourues sont de :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>15 ans de réclusion criminelle en cas de viol,</Paragraphe>
</Item>
<Item>
<Paragraphe>5 ans d'emprisonnement et <Valeur>75 000 €</Valeur> d'amende pour les autres agressions sexuelles.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Ces peines sont augmentées notamment :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>si l’acte a été commis par un <LienIntra LienID="R12668" type="Définition de glossaire">ascendant</LienIntra> ou par une personne ayant autorité sur la victime,</Paragraphe>
</Item>
<Item>
<Paragraphe>si l’acte a été commis par le conjoint, le concubin ou le partenaire lié à la victime par un Pacs,</Paragraphe>
</Item>
<Item>
<Paragraphe>lorsque la victime a été mise en contact avec l’auteur des faits par internet,</Paragraphe>
</Item>
<Item>
<Paragraphe>si la victime était particulièrement vulnérable (personne infirme, malade, enceinte),</Paragraphe>
</Item>
<Item>
<Paragraphe>si l’acte a entraîné une mutilation ou une infirmité permanente (en cas de viol) ou une blessure ou lésion (pour les autres agressions sexuelles),</Paragraphe>
</Item>
<Item>
<Paragraphe>si l’acte a été commis à raison de l’orientation sexuelle, réelle ou supposée, de la victime,</Paragraphe>
</Item>
<Item>
<Paragraphe>si l’acte a été commis sous l'emprise de l'alcool ou de produits stupéfiants ou avec l’usage ou la menace d'une arme ou encore par plusieurs personnes (auteur ou complice).</Paragraphe>
</Item>
</Liste>
<Paragraphe>Le viol est puni :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>de 30 ans de réclusion criminelle si l'acte a entraîné la mort de la victime,</Paragraphe>
</Item>
<Item>
<Paragraphe>de la réclusion criminelle à perpétuité si l'acte a été précédé, accompagné ou suivi de tortures ou d'actes de barbarie.</Paragraphe>
</Item>
</Liste>
<ANoter>
<Titre>À noter</Titre><Paragraphe>les <LienInterne LienPublication="F2274" type="Fiche d'information" audience="Particuliers">infractions sexuelles sur mineur</LienInterne> relèvent de peines et d'une procédure particulière.</Paragraphe>
</ANoter>
</SousChapitre>
</Chapitre>
</Texte><VoirAussi important="oui">
<Fiche ID="F2274" audience="Particuliers">
<Titre>Victime mineure</Titre>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
</Fiche>
</VoirAussi>
<OuSAdresser ID="R167" type="Centre de contact">
<Titre>08 Victimes</Titre>
<Complement>Pour être écouté et aidé (pour tous les types de victimes)</Complement>
<Source ID="R30797">Institut national d'aide aux victimes et de médiation (Inavem)</Source><Texte>
						
								<Paragraphe>
			
      Écoute, informe et conseille les victimes d'infractions (agressions, vols, escroqueries, accidents de la route), ainsi que leurs proches</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			<MiseEnEvidence>08 842 846 37</MiseEnEvidence>
      depuis la France métropolitaine</Paragraphe>
								<Paragraphe>Ouvert 7 jours sur 7 de 9h à 21h</Paragraphe>
								<Paragraphe>Numéro gris ou banalisé : coût d'un appel vers un fixe et service gratuit, depuis un téléphone fixe ou mobile</Paragraphe>
								<Paragraphe>
<MiseEnEvidence>+33 (0)1 41 83 42 08</MiseEnEvidence> depuis l'outre-mer ou l'étranger</Paragraphe>
								<Paragraphe>Ouvert 7 jours sur 7 de 9h à 21h</Paragraphe>
								<Paragraphe>
			Coût d'un appel local depuis un poste fixe</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par courriel</Paragraphe>
</Titre>
								<Paragraphe>En utilisant le <LienExterne URL="http://www.inavem.org/index.php/component/chronoforms5/?chronoform=contact_victimes">formulaire de contact</LienExterne>
</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R17892" type="Centre de contact">
<Titre>Violences Femmes Info - 3919</Titre><Texte>
						
								<Paragraphe>
			Écoute, informe et oriente les femmes victimes de violences, ainsi que les témoins de violences faites à des femmes.
		</Paragraphe>
								<Paragraphe>
			Traite les violences physiques, verbales ou psychologiques, à la maison ou au travail, et de toute nature (dont les harcèlements sexuels, les coups et blessures et les viols).
		</Paragraphe>
								<Paragraphe>
			Ne traite pas les situations d'urgence (n'est pas un service de police ni de gendarmerie).
		</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			<MiseEnEvidence>39 19</MiseEnEvidence>
      (appel gratuit depuis un téléphone fixe ou mobile)
    
		</Paragraphe>
								<Paragraphe>
			Ouvert </Paragraphe>
								<Liste type="puce">
									<Item>
										<Paragraphe>de 8h à 22h du lundi au vendredi,</Paragraphe>
									</Item>
									<Item>
										<Paragraphe>et de 9h à 18h le samedi, le dimanche et les jours fériés.</Paragraphe>
									</Item>
								</Liste>
								<Paragraphe>
			Appel anonyme. </Paragraphe>
								<Paragraphe>Appel ne figurant pas sur les factures de téléphone. </Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R31431" type="Local personnalisé sur SP">
<Titre>Bureau d'aide aux victimes</Titre>
<PivotLocal>bav</PivotLocal>
<RessourceWeb URL="http://www.interieur.gouv.fr/A-votre-service/Ma-securite/Aide-aux-victimes"/>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006165281&amp;cidTexte=LEGITEXT000006070719" ID="R17325">
<Titre>Code pénal : articles 222-22 et 222-22-2</Titre>
<Complement>Définition pénale du viol et de l'agression sexuelle</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006181753&amp;cidTexte=LEGITEXT000006070719" ID="R458">
<Titre>Code pénal : articles 222-23 à 222-26</Titre>
<Complement>Peines encourues en cas de viol</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006181754&amp;cidTexte=LEGITEXT000006070719" ID="R723">
<Titre>Code pénal : articles 222-27 à 222-31</Titre>
<Complement>Peines encourues pour les autres cas d'agression sexuelle</Complement>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R35961" URL="http://www.interieur.gouv.fr/A-votre-service/Ma-securite/Aide-aux-victimes" audience="Particuliers">
<Titre>Guide des droits des victimes</Titre>
<Source ID="R30603">Ministère en charge de l'intérieur</Source>
</PourEnSavoirPlus>
<Definition ID="R12668">
<Titre>Ascendant</Titre>
<Texte>
				<Paragraphe>Parents, grands-parents et arrière-grands-parents d'une personne</Paragraphe>
			</Texte>
</Definition>
</Publication>
