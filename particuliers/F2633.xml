<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F2633" type="Fiche avec liens externes" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Impôt sur le revenu - Enfant mineur à charge</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Argent</dc:subject>
<dc:description>Vos enfants mineurs sont normalement considérés à votre charge. Des règles spécifiques s'appliquent dans certaines situations : enfant atteignant sa majorité en cours d'année, enfants dont les parents sont imposés séparément (parents séparés, divorcés ou vivant en union libre).</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-01-01</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F2633</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006179569&amp;cidTexte=LEGITEXT000006069577, http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006179577&amp;cidTexte=LEGITEXT000006069577, http://bofip.impots.gouv.fr/bofip/2177-PGP, http://bofip.impots.gouv.fr/bofip/2175-PGP, http://bofip.impots.gouv.fr/bofip/2229-PGP.html?identifiant=BOI-IR-LIQ-10-10</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N247</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19803">Argent</Niveau>
<Niveau ID="N247">Impôt sur le revenu : déclaration et revenus à déclarer</Niveau>
<Niveau ID="F2633" type="Fiche avec liens externes">Impôt sur le revenu - Enfant mineur à charge</Niveau>
</FilDAriane>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
<SousThemePere ID="N20073">Impôts, taxes et douane</SousThemePere><DossierPere ID="N247">
<Titre>Impôt sur le revenu : déclaration et revenus à déclarer</Titre><SousDossier ID="N247-1">
<Titre>Déclaration de revenus : mode d'emploi</Titre>
<Fiche ID="F369">Première déclaration</Fiche>
<Fiche ID="F358">Déclaration annuelle</Fiche>
<Fiche ID="F388">Changement de situation</Fiche>
</SousDossier>
<SousDossier ID="N247-2">
<Titre>Quotient familial selon les personnes à charge</Titre>
<Fiche ID="F2633">Enfants mineurs</Fiche>
<Fiche ID="F3085">Enfants majeurs</Fiche>
<Fiche ID="F2661">Enfants handicapés</Fiche>
<Fiche ID="F387">Personnes invalides</Fiche>
</SousDossier>
<SousDossier ID="N247-3">
<Titre>Quotient familial selon la situation personnelle</Titre>
<Fiche ID="F2705">Couple marié ou pacsé</Fiche>
<Fiche ID="F2702">Personne seule ou en union libre</Fiche>
</SousDossier>
<SousDossier ID="N247-4">
<Titre>Salaires et éléments du salaire</Titre>
<Fiche ID="F1225">Revenus d'activité salariée</Fiche>
<Fiche ID="F1226">Avantages en nature</Fiche>
<Fiche ID="F1989">Frais professionnels : déduction forfaitaire ou frais réels</Fiche>
<Fiche ID="F408">Indemnités de fin de contrat</Fiche>
<Fiche ID="F1228">Sommes perçues par les jeunes</Fiche>
</SousDossier>
<SousDossier ID="N247-5">
<Titre>Pensions, retraites et rentes imposables</Titre>
<Fiche ID="F415">Pensions de retraite</Fiche>
<Fiche ID="F3169">Pensions d'invalidité</Fiche>
<Fiche ID="F3170">Pensions alimentaires</Fiche>
</SousDossier>
<SousDossier ID="N247-6">
<Titre>Revenus fonciers et mobiliers</Titre>
<Fiche ID="F2613">Revenus mobiliers</Fiche>
<Fiche ID="F21618">Plus-values sur valeurs mobilières</Fiche>
<Fiche ID="F10864">Plus-values immobilières</Fiche>
<Fiche ID="F1991">Revenus fonciers : logements non meublés</Fiche>
<Fiche ID="F32744">Revenus locatifs : logements meublés</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Quotient familial selon les personnes à charge</SousDossierPere><Avertissement ID="R35411" date="2016-04-25">
<Titre>Impôt sur le revenu : déclaration 2016 des revenus de 2015</Titre>
<Texte>
				<Paragraphe>Les règles relatives à l'imposition sur le revenu sont susceptibles d'être modifiées (loi de finance 2017 et lois de finances rectificatives).</Paragraphe>
				<Paragraphe>Les informations contenues dans cette page sont à jour pour la déclaration 2016 des revenus de 2015.</Paragraphe>
				<Paragraphe>Cette page sera modifiée en 2017 pour la déclaration des revenus de 2016.</Paragraphe>
			</Texte>
</Avertissement>

<Introduction>
<Texte><Paragraphe>Vos enfants mineurs sont normalement considérés à votre charge.
		
			Des règles spécifiques s'appliquent dans certaines situations : enfant atteignant sa majorité en cours d'année, enfants dont les parents sont imposés séparément (parents séparés, divorcés ou vivant en union libre).</Paragraphe>
</Texte>
</Introduction>
<LienExterneCommente><LienExterne URL="http://www.impots.gouv.fr/portal/dgi/public/popup?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_1538">Votre enfant est mineur</LienExterne>

<Source ID="R30612">Ministère en charge des finances</Source>
</LienExterneCommente>
<LienExterneCommente><LienExterne URL="http://www.impots.gouv.fr/portal/dgi/public/popup?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_1539">Votre enfant devient majeur en cours d'année</LienExterne>

<Source ID="R30612">Ministère en charge des finances</Source>
</LienExterneCommente>
<LienExterneCommente><LienExterne URL="http://www.impots.gouv.fr/portal/dgi/public/popup?espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_1543">Comment compter à charge les enfants de parents imposés séparément ?</LienExterne>

<Source ID="R30612">Ministère en charge des finances</Source>
</LienExterneCommente><VoirAussi important="non">
<Fiche ID="F2705" audience="Particuliers">
<Titre>Impôt sur le revenu : quotient familial d'un couple marié ou pacsé</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
<Fiche ID="F2702" audience="Particuliers">
<Titre>Impôt sur revenu : quotient familial d'une personne seule ou en union libre</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
<Fiche ID="F358" audience="Particuliers">
<Titre>Impôt sur le revenu : déclaration annuelle</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
<Fiche ID="F388" audience="Particuliers">
<Titre>Déclaration de revenus en cas de changement de situation en cours d'année</Titre>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
</Fiche>
</VoirAussi>
<OuSAdresser ID="R20684" type="Centre de contact">
<Titre>Impôts Service</Titre>
<Complement>Pour des informations générales</Complement>
<Source ID="R30612">Ministère en charge des finances</Source><Texte>
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			<MiseEnEvidence>0 810 467 687</MiseEnEvidence> (0 810 IMPOTS)</Paragraphe>
								<Paragraphe>
			Du lundi au vendredi de 8h à 22h et le samedi de 9h à 19h, hors jours fériés.</Paragraphe>
								<Paragraphe>Numéro violet ou majoré : coût d'un appel vers un numéro fixe + service payant, depuis un téléphone fixe ou mobile</Paragraphe>
								<Paragraphe>Pour connaître le tarif, écoutez le message en début d'appel</Paragraphe>
								<Paragraphe>Hors métropole ou depuis l'étranger, composer le + 33 (0)8 10 46 76 87.</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R15019" type="Local personnalisable">
<Titre>Service en charge des impôts (trésorerie, centre des impôts fonciers...)</Titre>
<Complement>Pour joindre le service local gestionnaire de votre dossier (les coordonnées figurent sur vos avis d'imposition et déclarations de revenus)</Complement>
<PivotLocal>centre_impots</PivotLocal>
<RessourceWeb URL="http://www.impots.gouv.fr/portal/dgi/public/contacts?pageId=contacts&amp;sfid=07#services"/>
<Source ID="R30612">Ministère en charge des finances</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006179569&amp;cidTexte=LEGITEXT000006069577" ID="R32026">
<Titre>Code général des impôts : articles 4A à 8 quinquies</Titre>
<Complement>Personnes imposables</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006179577&amp;cidTexte=LEGITEXT000006069577" ID="R31871">
<Titre>Code général des impôts : articles 193 à 199</Titre>
<Complement>Enfants à charge (article 196)</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://bofip.impots.gouv.fr/bofip/2177-PGP" ID="R36577">
<Titre>Bofip-impôts n°BOI-IR-LIQ-10-10-10-10 relatif aux enfants mineurs à charge</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://bofip.impots.gouv.fr/bofip/2175-PGP" ID="R36579">
<Titre>Bofip-impôts n°BOI-IR-LIQ-10-10-10-20 relatif aux enfants majeurs à charge</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://bofip.impots.gouv.fr/bofip/2229-PGP.html?identifiant=BOI-IR-LIQ-10-10" ID="R2343">
<Titre>Bofip-impôts n°BOI-IR-LIQ-10-10 relatif à la prise en compte de la situation et des charges de famille pour l'impôt sur le revenu</Titre>
</Reference>
<ServiceEnLigne ID="R3120" URL="https://cfspart.impots.gouv.fr/LoginMDP?op=c&amp;url=aHR0cHM6Ly9jZnNwYXJ0LmltcG90cy5nb3V2LmZyL3BvcnRhbC9kZ2kvcHVibGljL3BlcnNvP3BhZ2VJZD1wbmEycGFyJnNmaWQ9MzA=" type="Téléservice">
<Titre>Impôts : accéder à votre espace Particulier</Titre>
<Source ID="R30612">Ministère en charge des finances</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R1280" URL="https://cfspart.impots.gouv.fr/LoginMDP?op=c&amp;url=aHR0cHM6Ly9jZnNwYXJ0LmltcG90cy5nb3V2LmZyL3BvcnRhbC9kZ2kvcHVibGljL3BlcnNvP3BhZ2VJZD1wbmEycGFyJnNmaWQ9MzA=" type="Téléservice">
<Titre>Déclaration 2016 en ligne des revenus</Titre>
<Source ID="R30612">Ministère en charge des finances</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R1281" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10424/fichedescriptiveformulaire_10424.pdf" numerocerfa="10330*20" autrenumero="2042" type="Formulaire">
<Titre>Déclaration 2016 des revenus</Titre>
<Source ID="R30612">Ministère en charge des finances</Source><NoticeLiee ID="R15720" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10420/fichedescriptiveformulaire_10420.pdf" numerocerfa="50796#16" format="application/pdf" poids="668.4 KB">Notice pour remplir votre déclaration de revenus</NoticeLiee>
<NoticeLiee ID="R40459" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10407/fichedescriptiveformulaire_10407.pdf" format="application/pdf" poids="153.0 KB">Déclaration des revenus 2015 - fiche facultative de calculs</NoticeLiee>
<NoticeLiee ID="R36611" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10403/fichedescriptiveformulaire_10403.pdf" numerocerfa="50988#12">Notice revenus 2015 : résidence alternée d'enfants mineurs</NoticeLiee>
<NoticeLiee ID="R36724" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10401/fichedescriptiveformulaire_10401.pdf" numerocerfa="50688#17">Notice revenus 2015 : allocations pour frais d'emploi</NoticeLiee>
<NoticeLiee ID="R36723" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10402/fichedescriptiveformulaire_10402.pdf" numerocerfa="50883#15" format="application/pdf" poids="155.6 KB">Notice revenus de 2015 : plafonnement des effets du quotient familial.</NoticeLiee>
<NoticeLiee ID="R36692" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10416/fichedescriptiveformulaire_10416.pdf" numerocerfa="50149#20">Notice revenus 2015 : Revenus exceptionnels ou différés et cas particuliers</NoticeLiee>
<NoticeLiee ID="R40371" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10399/fichedescriptiveformulaire_10399.pdf" numerocerfa="50154#20">Notice revenus 2015 : revenus des valeurs et capitaux mobiliers (RCM)</NoticeLiee>
<NoticeLiee ID="R36721" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10406/fichedescriptiveformulaire_10406.pdf" numerocerfa="50794#16">Notice revenus 2015 : Travaux dans l'habitation principale</NoticeLiee>
<NoticeLiee ID="R36677" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10400/fichedescriptiveformulaire_10400.pdf" numerocerfa="50153#20">Notice revenus 2015 : BIC non professionnels : revenus de locations meublées et autres activités BIC non professionnelles</NoticeLiee>
<NoticeLiee ID="R43226" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10398/fichedescriptiveformulaire_10398.pdf" numerocerfa="50318#19">Notice revenus 2015 : personnes fiscalement domiciliées hors de France</NoticeLiee>
</ServiceEnLigne>
<ServiceEnLigne ID="R2740" URL="http://www.impots.gouv.fr/portal/dgi/public/popup?pageId=particuliers&amp;espId=1&amp;typePage=cpr02&amp;docOid=documentstandard_705" type="Simulateur">
<Titre>Simulateur de calcul pour 2016 : impôt sur les revenus de 2015</Titre>
<Source ID="R30612">Ministère en charge des finances</Source>
</ServiceEnLigne>
<QuestionReponse ID="F476" audience="Particuliers">Quel quotient familial retenir en cas de divorce ou de séparation dans l'année ?</QuestionReponse>
</Publication>
