<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F14724" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Comment régler un litige avec un avocat ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Justice</dc:subject>
<dc:description>Le règlement d'un conflit entre un justiciable et son avocat diffère suivant l'objet du conflit (faute déontologique, civile, etc.).</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de la justice</dc:contributor>
<dc:date>modified 2014-08-08</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F14724</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006068396, http://www.legifrance.gouv.fr/affichTexte.do;?cidTexte=LEGITEXT000006078311, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000633327</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N279</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19807">Justice</Niveau>
<Niveau ID="N279">Acteurs de la justice</Niveau>
<Niveau ID="F14724" type="Fiche Question-réponse">Comment régler un litige avec un avocat ?</Niveau>
</FilDAriane>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
<SousThemePere ID="N20260">Organisation de la justice</SousThemePere><DossierPere ID="N279">
<Titre>Acteurs de la justice</Titre>
<Fiche ID="F2176">Magistrats et autres agents publics habilités</Fiche>
<Fiche ID="F2158">Huissier de justice</Fiche>
<Fiche ID="F2153">Avocat</Fiche>
<Fiche ID="F2164">Notaire</Fiche>
<Fiche ID="F1540">Juré d'assises</Fiche>
<Fiche ID="F1822">Médiateur civil</Fiche>
<Fiche ID="F1739">Médiateur pénal</Fiche>
<Fiche ID="F1736">Conciliateur de justice</Fiche>
<Fiche ID="F2161">Expert judiciaire</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Le règlement d'un conflit entre un justiciable et son avocat diffère suivant l'objet du conflit (faute déontologique, civile, etc.).</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Si le différend porte sur les frais de l'avocat</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Contestation sur les émoluments, droits et débours</Paragraphe>
</Titre><Paragraphe>S'il s'agit d'une contestation sur le <LienInterne LienPublication="F15018" type="Fiche Question-réponse" audience="Particuliers">coût de la prestation d'un avocat</LienInterne> , appelé "émoluments, droits et débours", vous pouvez saisir, après la décision de justice, le greffier en chef du tribunal (<LienInterne LienPublication="F1783" type="Fiche d'information" audience="Particuliers">TI</LienInterne> ou <LienInterne LienPublication="F20851" type="Fiche d'information" audience="Particuliers">TGI</LienInterne> pour les litiges supérieurs à <Valeur>10 000 €</Valeur>) qui a jugé le litige.</Paragraphe>
<Paragraphe>Après une éventuelle rectification du compte, le greffier en chef doit vous remettre un certificat de vérification. Celui-ci doit être notifié à l'avocat et peut faire l'objet de recours devant le <LienIntra LienID="R19324" type="Définition de glossaire">bâtonnier</LienIntra>.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Contestation sur les honoraires</Paragraphe>
</Titre><Paragraphe>Si vous contestez les honoraires de l'avocat, votre contestation doit être portée devant le <LienIntra LienID="R19324" type="Définition de glossaire">bâtonnier</LienIntra> de l'Ordre auquel est rattaché l'avocat. Elle doit obligatoirement être formulée par lettre recommandée avec accusé de réception ou remise à l'Ordre directement contre récépissé.</Paragraphe>
<ServiceEnLigne ID="R19096" URL="http://www.conso.net/content/vous-contestez-les-honoraires-de-votre-avocat-et-deposez-un-recours-devant-le-batonnier" type="Modèle de document">
<Titre>Saisir le bâtonnier pour contester les honoraires de son avocat</Titre>
<Source ID="R30643">Institut national de la consommation (INC)</Source>
<Introduction>
<Texte><Paragraphe>Permet d'exercer un recours devant le bâtonnier de l'ordre des avocats du barreau au sein duquel son avocat est inscrit pour contester ses honoraires.</Paragraphe>
</Texte>
</Introduction>
</ServiceEnLigne>

<Paragraphe>Si le bâtonnier répond à la requête dans le délai de 4 mois de sa réception, son arbitrage peut faire l'objet (en cas de désaccord) d'un appel devant le 1er président de la Cour d'appel compétente dans le délai d'1 mois.</Paragraphe>
<Paragraphe>Si le bâtonnier ne répond pas la requête , son silence ouvre au requérant le droit de saisir directement le 1er président de la Cour d'appel compétente dans le délai d'1 mois suivant l'échéance des 4 mois.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Si le différend porte sur un autre sujet</Paragraphe>
</Titre><Paragraphe>Vous pouvez rencontrer d'autres difficultés avec votre avocat : retards inexpliqués, absence de réponse à des correspondances, refus répétés de rendez-vous, absence de l'avocat à l'audience, renvois d'audience inexpliqués, absence d'explications, absence de compte-rendu, etc.</Paragraphe>
<Paragraphe>Dans ce cas, vous pouvez saisir le <LienIntra LienID="R19324" type="Définition de glossaire">bâtonnier</LienIntra> de l'Ordre auquel appartient l'avocat en cause.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre>
<Paragraphe>en cas de faute disciplinaire, le procureur général de la <LienInterne LienPublication="F2224" type="Fiche avec liens externes" audience="Particuliers">Cour d'appel</LienInterne> peut également se saisir ou être saisi du problème d'insuffisance professionnelle posé par l'attitude d'un avocat.</Paragraphe>
</ASavoir>
</Chapitre>
</Texte><OuSAdresser ID="R11621" type="Local personnalisé sur SP">
<Titre>Maison de justice et du droit</Titre>
<Complement>Pour s'informer</Complement>
<PivotLocal>mjd</PivotLocal>
<RessourceWeb URL="http://www.annuaires.justice.gouv.fr/index.php?rubrique=10111"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<OuSAdresser ID="R11624" type="Local personnalisé sur SP">
<Titre>Cour d'appel</Titre>
<Complement>Pour se plaindre</Complement>
<PivotLocal>cour_appel</PivotLocal>
<RessourceWeb URL="http://www.annuaires.justice.gouv.fr/annuaires-12162/annuaire-des-cours-dappel-21767.html"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<OuSAdresser ID="R17" type="Local personnalisable">
<Titre>Barreau des avocats</Titre>
<Complement>Pour se plaindre</Complement>
<PivotLocal>avocat_centre_national</PivotLocal>
<RessourceWeb URL="http://www.cnb.avocat.fr/index.php?id_plugin=2868&amp;path=pluginCNB/index.php&amp;action=plugin&amp;view=barreaux"/>
<Source ID="R30762">Conseil national des barreaux</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000006068396" ID="R2205">
<Titre>Loi n°71-1130 du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques</Titre>
<Complement>Article 21</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do;?cidTexte=LEGITEXT000006078311" ID="R11310">
<Titre>Décret n°91-1197 du 27 novembre 1991 organisant la profession d'avocat</Titre>
<Complement>Articles 174 à 179</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000633327" ID="R38014">
<Titre>Décret n°2005-790 du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat.</Titre>
<Complement>Articles 14 et 19</Complement>
</Reference>
<ServiceEnLigne ID="R19096" URL="http://www.conso.net/content/vous-contestez-les-honoraires-de-votre-avocat-et-deposez-un-recours-devant-le-batonnier" type="Modèle de document">
<Titre>Saisir le bâtonnier pour contester les honoraires de son avocat</Titre>
<Source ID="R30643">Institut national de la consommation (INC)</Source>
</ServiceEnLigne>
<Definition ID="R19324">
<Titre>Bâtonnier</Titre>
<Texte>
				<Paragraphe>Avocat élu pour 2 ans par ses confrères dans chaque barreau pour les représenter et  garantir la déontologie et la discipline de la profession. Il désigne les avocats commis d'office, règle les différends entre eux ou avec leurs clients.</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F15006" audience="Particuliers">Un avocat peut-il exiger de l'argent d'un bénéficiaire d'aide juridictionnelle ?</QuestionReponse>
<QuestionReponse ID="F15018" audience="Particuliers">Combien coûte un avocat ?</QuestionReponse>
<QuestionReponse ID="F932" audience="Particuliers">Un avocat peut-il prendre un pourcentage sur l'argent gagné grâce à un procès ?</QuestionReponse>
</Publication>
