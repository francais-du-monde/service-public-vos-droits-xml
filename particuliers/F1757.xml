<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F1757" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Conduire en France avec un permis d'un autre pays européen</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Transports</dc:subject>
<dc:description>Les permis de conduire délivrés par les autres pays de l'Espace économique européen (EEE) sont reconnus en France. La personne qui réside en France, titulaire d'un permis de conduire obtenu dans un autre pays européen, peut circuler avec. Elle doit toutefois respecter certaines conditions. Elle peut demander l'échange de son permis national contre un permis français. Mais ce n'est pas une obligation, sauf exception.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-09-10</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F1757</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006159563&amp;cidTexte=LEGITEXT000006074228, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000005627531</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N19126</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19812">Transports</Niveau>
<Niveau ID="N19126">Conduire en France avec un permis étranger</Niveau>
<Niveau ID="F1757" type="Fiche d'information">Conduire en France avec un permis d'un autre pays européen</Niveau>
</FilDAriane>
<Theme ID="N19812">
<Titre>Transports</Titre>
</Theme><DossierPere ID="N19126">
<Titre>Conduire en France avec un permis étranger</Titre>
<Fiche ID="F1757">Conduire en France avec un permis d'un autre pays européen</Fiche>
<Fiche ID="F1459">Conduire en France avec un permis non européen</Fiche>
<Fiche ID="F1758">Échanger un permis de conduire d'un autre pays européen</Fiche>
<Fiche ID="F1460">Échanger un permis de conduire non européen</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Les permis de conduire délivrés par les autres pays de l'Espace économique européen (EEE) sont  reconnus en France.
		La personne qui réside en France, titulaire d'un permis de conduire obtenu dans un autre pays européen, peut circuler avec. Elle doit toutefois respecter certaines conditions. Elle peut demander l'échange de son permis national contre un permis français. Mais ce n'est pas une obligation, sauf exception.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Conditions pour conduire avec un permis européen</Paragraphe>
</Titre><Paragraphe>Pour être valable en France, le permis de conduire doit remplir  les 3 conditions suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>être en cours de validité,</Paragraphe>
</Item>
<Item>
<Paragraphe>être utilisé par une personne qui a atteint l'âge minimal pour conduire le véhicule de la catégorie équivalente (au moins 18 ans pour le <LienInterne LienPublication="F2827" type="Fiche Question-réponse" audience="Particuliers">permis B)</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>être utilisé conformément aux mentions d'ordre médical (port obligatoire de lunettes par exemple) qui y sont inscrites.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Par ailleurs, le conducteur :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>ne doit pas avoir fait l'objet dans le pays de délivrance de son permis d'une mesure de suspension, de restriction ou d'annulation de son droit de conduire,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou ne doit pas avoir obtenu son permis dans un autre pays européen pendant une période d'interdiction de solliciter ou d'obtenir un permis de conduire en France (par exemple, en accompagnement d'une <LienInterne LienPublication="F21774" type="Fiche d'information" audience="Particuliers">peine d'annulation du permis)</LienInterne>.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si la personne remplit<MiseEnEvidence> toutes</MiseEnEvidence> ces conditions, elle peut conduire en France avec son permis <MiseEnEvidence>aussi longtemps qu'il reste valable</MiseEnEvidence>.</Paragraphe>
<Paragraphe>Si elle ne remplit pas toutes ces conditions, elle n'a pas le droit de conduire avec son permis en France. Elle doit : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit attendre de satisfaire toutes les conditions pour circuler avec son permis (par exemple attendre ses 18 ans pour le permis B si elle a 17 ans ou attendre la fin de la mesure de suspension de son permis si elle est frappée d'une telle mesure), </Paragraphe>
</Item>
<Item>
<Paragraphe> soit repasser son permis de conduire (en cas d'annulation de son permis). </Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Cas du permis européen obtenu en échange d'un permis hors EEE</Paragraphe>
</Titre><Paragraphe>Le permis européen obtenu en échange d'un permis délivré par un pays non européen, avec lequel la France ne pratique pas l'échange réciproque des permis, ne peut être utilisé que durant un temps limité en France. Son titulaire ne peut conduire avec que pendant un an à partir de l'acquisition de sa <LienIntra LienID="R2308" type="Définition de glossaire">résidence normale</LienIntra> sur le territoire.</Paragraphe>
<Paragraphe>Un tel permis n'est pas échangeable et son titulaire, passé le délai précité, devra passer <LienInterne LienPublication="F2828" type="Fiche d'information" audience="Particuliers">l'examen du permis de conduire français</LienInterne> s'il veut continuer à conduire en France.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Échange du permis</Paragraphe>
</Titre><Paragraphe>Le personne, qui souhaite échanger son permis de conduire contre un permis français, doit en faire la <LienInterne LienPublication="F1758" type="Fiche d'information" audience="Particuliers">demande à la préfecture ou à la sous-préfecture de son domicile</LienInterne>.</Paragraphe>
<Paragraphe>Ce n'est pas une obligation, sauf en cas d'infraction routière entraînant une mesure de restriction, de suspension, d'annulation du permis ou une perte de points.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R2" type="Local personnalisé sur SP">
<Titre>Préfecture</Titre>
<Complement>Conducteur domicilié hors Paris : pour toute information</Complement>
<PivotLocal>prefecture</PivotLocal>
<RessourceWeb URL="http://www.interieur.gouv.fr/Le-ministere/Prefectures"/>
<Source ID="R30603">Ministère en charge de l'intérieur</Source>
</OuSAdresser>
<OuSAdresser ID="R19575" type="Local personnalisé sur SP">
<Titre>Préfecture de police de Paris - Bureau des permis de conduire</Titre>
<Complement>Conducteur domicilié à Paris : pour toute information</Complement>
<PivotLocal>paris_ppp_permis_conduire</PivotLocal>
<RessourceWeb URL="http://www.prefecturedepolice.interieur.gouv.fr/Demarches/Particulier/Permis-de-conduire-et-papiers-du-vehicule/Permis-de-conduire/Contacts-et-informations-pratiques/Nous-contacter-Permis-de-conduire"/>
<Source ID="R30800">Préfecture de police de Paris</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006159563&amp;cidTexte=LEGITEXT000006074228" ID="R799">
<Titre>Code de la route : articles R222-1 à R222-8</Titre>
<Complement>Reconnaissance des permis de conduire étrangers en France</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000005627531" ID="R11477">
<Titre>Arrêté du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les États appartenant à l'UE et à l'EEE</Titre>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R10943" URL="http://www.diplomatie.gouv.fr/fr/IMG/pdf/Liste_permis_de_conduire_valables_a_l_echange_01_2014_cle8cc6c4.pdf" audience="Particuliers" format="application/pdf" poids="0">
<Titre>Pays pratiquant l'échange réciproque des permis de conduire avec la France</Titre>
<Source ID="R30604">Ministère en charge des affaires étrangères</Source>
</PourEnSavoirPlus>
<Definition ID="R2308">
<Titre>Résidence normale (permis de conduire)</Titre>
<Texte>
				<Paragraphe>Pays où une personne demeure plus de 6 mois (185 jours minimum) par an,
		du fait d'attaches personnelles ou professionnelles</Paragraphe>
			</Texte>
</Definition>

</Publication>
