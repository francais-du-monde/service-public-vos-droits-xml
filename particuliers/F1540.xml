<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F1540" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Juré d'assises</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Justice</dc:subject>
<dc:description>Les jurés sont des citoyens tirés au sort qui participent, aux côtés des magistrats professionnels, au jugement des crimes au sein de la cour d'assises. Les jurés sont des juges à part entière.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de la justice</dc:contributor>
<dc:date>modified 2014-08-28</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F1540</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006182899&amp;cidTexte=LEGITEXT000006071154, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006182900&amp;cidTexte=LEGITEXT000006071154, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177836&amp;cidTexte=LEGITEXT000006072050</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N279</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19807">Justice</Niveau>
<Niveau ID="N279">Acteurs de la justice</Niveau>
<Niveau ID="F1540" type="Fiche d'information">Juré d'assises</Niveau>
</FilDAriane>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
<SousThemePere ID="N20260">Organisation de la justice</SousThemePere><DossierPere ID="N279">
<Titre>Acteurs de la justice</Titre>
<Fiche ID="F2176">Magistrats et autres agents publics habilités</Fiche>
<Fiche ID="F2158">Huissier de justice</Fiche>
<Fiche ID="F2153">Avocat</Fiche>
<Fiche ID="F2164">Notaire</Fiche>
<Fiche ID="F1540">Juré d'assises</Fiche>
<Fiche ID="F1822">Médiateur civil</Fiche>
<Fiche ID="F1739">Médiateur pénal</Fiche>
<Fiche ID="F1736">Conciliateur de justice</Fiche>
<Fiche ID="F2161">Expert judiciaire</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Les jurés sont des citoyens tirés au sort qui participent, aux côtés des magistrats professionnels, au jugement des crimes au sein de la cour d'assises. Les jurés sont des juges à part entière.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Personnes concernées</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Conditions</Paragraphe>
</Titre><Paragraphe>Vous pouvez être juré si vous remplissez  les conditions cumulatives suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>être de nationalité française,</Paragraphe>
</Item>
<Item>
<Paragraphe>avoir au moins 23 ans,</Paragraphe>
</Item>
<Item>
<Paragraphe>savoir lire et écrire le français,</Paragraphe>
</Item>
<Item>
<Paragraphe>ne pas se trouver dans un cas d'incapacité ou d'incompatibilité avec les fonctions de jurés.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Cas d'incapacité et d'incompatibilité</Paragraphe>
</Titre><Paragraphe>Certaines catégories de personnes énumérées par la loi ne sont pas autorisées à participer au jugement des <LienInterne LienPublication="F1157" type="Fiche Question-réponse" audience="Particuliers">crimes</LienInterne>.</Paragraphe>
<Paragraphe>Il s'agit notamment :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>des personnes ayant été condamnées pour un crime ou un délit,</Paragraphe>
</Item>
<Item>
<Paragraphe>des agents publics ayant été révoqués de leurs fonctions,</Paragraphe>
</Item>
<Item>
<Paragraphe>des personnes sous tutelle ou curatelle,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>des membres du gouvernement,</Paragraphe>
</Item>
<Item>
<Paragraphe>des députés et des sénateurs,</Paragraphe>
</Item>
<Item>
<Paragraphe>des magistrats,</Paragraphe>
</Item>
<Item>
<Paragraphe>des fonctionnaires des services de police ou de gendarmerie, etc.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Sont aussi rayés des listes de jurés, les noms des personnes proches (conjoint, parents, enfants, etc.) de l'un des magistrats formant la cour d'assises ou de l'un des jurés précédemment inscrits.</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>hormis ces cas, il est difficile d'être dispensé de son rôle de juré sauf à s'exposer à une amende de <Valeur>3 750 €</Valeur>.</Paragraphe>
</ANoter>
</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Sélection</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Liste préparatoire de la liste annuelle</Paragraphe>
</Titre><Paragraphe>Le maire établit une liste préparatoire en tirant au sort publiquement un nombre de noms triple de celui prévu pour la commune, et ce à partir de la liste électorale. Si vous n'avez pas atteint 23 ans au cours de l'année civile qui suit, vous n'êtes pas retenus. </Paragraphe>
<Paragraphe>Le maire avertit par courrier les électeurs qui figurent sur cette liste préparatoire et transmet la liste au  greffe de la cour d'assises dont dépend la commune.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Liste annuelle</Paragraphe>
</Titre><Paragraphe>Une commission spéciale placée auprès de chaque cour d'assises se réunit pour :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>exclure les personnes qui ne remplissent pas les conditions pour être juré,</Paragraphe>
</Item>
<Item>
<Paragraphe>se prononcer sur les <LienInterne LienPublication="F1044" type="Fiche Question-réponse" audience="Particuliers">demandes de dispense</LienInterne> qui lui sont soumises,</Paragraphe>
</Item>
<Item>
<Paragraphe>procéder à un nouveau tirage au sort et établir la liste annuelle des jurés et la liste spéciale des jurés suppléants.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Les listes sont communiquées aux mairies.</Paragraphe>
<Paragraphe>Les maires doivent alerter la cour d'assises de toute survenance de décès, d'incapacité ou d'incompatibilité parmi les personnes retenues.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Liste de session</Paragraphe>
</Titre><Paragraphe>Pour chaque session d'assises, les présidents de tribunaux de grande instance et de la Cour d'appel, ou leurs délégués, tirent au sort publiquement, à partir de la liste annuelle :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>35 jurés pour former la liste de session,</Paragraphe>
</Item>
<Item>
<Paragraphe>10 jurés suppléants pour former la liste spéciale.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si vous êtes juré  titulaire et suppléant, le greffier de la cour d'assises vous convoquera par courrier. La convocation précise la date et l'heure d'ouverture de la session, sa durée prévisible et le lieu où elle se tiendra. Vous devez y répondre par courrier.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Constitution du jury</Paragraphe>
</Titre><Paragraphe>Pour chaque affaire, chaque juré de la liste de session est appelé en audience publique et une carte portant son nom est déposée dans une urne. Un dernier tirage au sort est effectué.</Paragraphe>
<Paragraphe>À chaque nom sortant de l'urne, l'accusé (ou son avocat), puis l'avocat général, récusent ou non le juré (c'est-à-dire le refusent ou non), sous réserve de respecter les <LienInterne LienPublication="F1487" type="Fiche d'information" audience="Particuliers">limites imposées dans le nombre de récusations possibles</LienInterne>. </Paragraphe>
<Paragraphe>Les premiers jurés non récusés (au nombre de 6 en 1ère instance et 9 en appel) forment le jury de jugement, après avoir prêté serment. </Paragraphe>
<Paragraphe>Des jurés supplémentaires sont tirés au sort, pour pouvoir remplacer les jurés subitement empêchés (raisons de santé, impératifs professionnels, etc.).</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Exercice de la fonction</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Formation</Paragraphe>
</Titre><Paragraphe>Vous regardez un <LienExterne URL="http://www.mediatheque.justice.gouv.fr/direct/46-1a63745717d74f7055ad7aeedc2dc14217715bd1-1329847232-direct">film présentant la fonction</LienExterne>

 que vous allez  assumer. La possibilité de visiter une prison est souvent proposée.</Paragraphe>
<Paragraphe>Le président de la cour d'assises vous fournit aussi des explications.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Pouvoirs</Paragraphe>
</Titre><Paragraphe>Vous siégez aux audiences et participez aux délibérations à l'issue desquelles les magistrats et vous-mêmes votez à bulletin secret sur la culpabilité de l'accusé et sur sa peine. </Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Obligations</Paragraphe>
</Titre><Liste type="puce">
<Item>
<Paragraphe>Être attentif lors des débats</Paragraphe>
</Item>
<Item>
<Paragraphe>Être impartial, c'est-à-dire indépendant, neutre et objectif</Paragraphe>
</Item>
<Item>
<Paragraphe>Ne pas communiquer avec d'autres personnes sur l'affaire</Paragraphe>
</Item>
<Item>
<Paragraphe>Respecter le secret du délibéré (y compris une fois que vous avez cessé d'être juré)</Paragraphe>
</Item>
</Liste>
<Attention>
<Titre>Attention</Titre><Paragraphe>le non respect du délibéré vous fait encourir une peine d'un an de prison et <Valeur>15 000 €</Valeur> d'amende.</Paragraphe>
</Attention>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Durée</Paragraphe>
</Titre><Paragraphe>Vous exercez cette fonction de façon continue et à temps plein durant tout le temps nécessaire à l'examen des affaires d'une même session.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Indemnisation</Paragraphe>
</Titre><Paragraphe>Vous percevez, sur votre demande expresse, des <LienInterne LienPublication="F17783" type="Fiche Question-réponse" audience="Particuliers">indemnités compensatrices</LienInterne>.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Relation avec l'employeur</Paragraphe>
</Titre><Paragraphe>Votre employeur ne vous rémunère pas pendant votre absence. Il doit vous fournir un document indiquant le montant de votre salaire ou tout document attestant une perte de revenu professionnel afin d'obtenir vos indemnités compensatrices.</Paragraphe>
<Paragraphe>Votre employeur n'a pas à vous demander à prendre des jours de congés pour siéger à la cour d'assises. Votre absence est considérée comme un congé sans solde.</Paragraphe>
<Paragraphe>Vous ne pouvez pas être sanctionné ou faire l'objet d'une mesure discriminatoire en raison de cette absence.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><OuSAdresser ID="R11621" type="Local personnalisé sur SP">
<Titre>Maison de justice et du droit</Titre>
<Complement>Pour s'informer gratuitement</Complement>
<PivotLocal>mjd</PivotLocal>
<RessourceWeb URL="http://www.annuaires.justice.gouv.fr/index.php?rubrique=10111"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<OuSAdresser ID="R30" type="Local personnalisé sur SP">
<Titre>Tribunal de grande instance (TGI)</Titre>
<Complement>Pour s'informer</Complement>
<PivotLocal>tgi</PivotLocal>
<RessourceWeb URL="http://www.annuaires.justice.gouv.fr/annuaires-12162/annuaire-des-tribunaux-de-grande-instance-21768.html"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006182899&amp;cidTexte=LEGITEXT000006071154" ID="R19820">
<Titre>Code de procédure pénale : articles 255 à 258-2</Titre>
<Complement>Conditions d'aptitude aux fonctions de juré</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006182900&amp;cidTexte=LEGITEXT000006071154" ID="R569">
<Titre>Code de procédure pénale : articles 259 à 267</Titre>
<Complement>Formation du jury</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177836&amp;cidTexte=LEGITEXT000006072050" ID="R2808">
<Titre>Code du travail : articles L1132-1 à L1132-4</Titre>
<Complement>Article L1132-3</Complement>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R19613" URL="http://www.mediatheque.justice.gouv.fr/direct/46-1a63745717d74f7055ad7aeedc2dc14217715bd1-1329847232-direct" audience="Particuliers">
<Titre>Présentation du citoyen juré</Titre>
<Source ID="R30663">Ministère en charge de la justice</Source>
</PourEnSavoirPlus>
<PourEnSavoirPlus type="Information pratique" ID="R38091" URL="http://www.justice.gouv.fr/publication/guide_jures_assises.pdf" audience="Particuliers" format="application/pdf" poids="6.2 MB">
<Titre>Guide pratique du juré d'assises</Titre>
<Source ID="R30663">Ministère en charge de la justice</Source>
</PourEnSavoirPlus>
<QuestionReponse ID="F1044" audience="Particuliers">Peut-on refuser d'être juré à une cour d'assises ?</QuestionReponse>
<QuestionReponse ID="F1157" audience="Particuliers">Quelles sont les différences entre une contravention, un délit et un crime ?</QuestionReponse>
<QuestionReponse ID="F17783" audience="Particuliers">Quelles sont les indemnités dues aux jurés d'assises ?</QuestionReponse>
</Publication>
