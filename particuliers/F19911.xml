<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F19911" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Malus et taxe CO₂ pour un véhicule polluant</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Transports</dc:subject>
<dc:description>Une écotaxe dite malus et une taxe dite taxe CO₂ sanctionnent financièrement l'acquisition ou la location (sous conditions) d'un véhicule particulier (VP) neuf ou d'occasion polluant, en fonction de la quantité de dioxyde de carbone (CO₂) émise par le véhicule. À ces taxes s'ajoute, les années suivant l'achat ou la location, le paiement d'une taxe annuelle sur la détention de VP polluants pour les véhicules dont le taux de CO₂ est le plus élevé.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-01-01</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F19911</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000018619161&amp;cidTexte=LEGITEXT000006069577, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000018029918&amp;cidTexte=LEGITEXT000006069577, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020694387&amp;cidTexte=LEGITEXT000006069574, http://bofip.impots.gouv.fr/bofip/2707-PGP.html?ftsq=malus&amp;identifiant=BOI-ENR-TIM-20-60-30-20120912, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029574487</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N18131</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19812">Transports</Niveau>
<Niveau ID="N18131">Mesures antipollution</Niveau>
<Niveau ID="F19911" type="Fiche d'information">Malus et taxe CO₂ pour un véhicule polluant</Niveau>
</FilDAriane>
<Theme ID="N19812">
<Titre>Transports</Titre>
</Theme><DossierPere ID="N18131">
<Titre>Mesures antipollution</Titre><SousDossier ID="N18131-1">
<Titre>Prime écologique (bonus)</Titre>
<Fiche ID="F18132">Véhicule particulier (VP) essence, GPL ou gaz naturel</Fiche>
<Fiche ID="F32430">Véhicule particulier (VP) électrique ou hybride</Fiche>
<Fiche ID="F18167">Camionnette (CTTE)</Fiche>
</SousDossier>
<SousDossier ID="N18131-2">
<Titre>Remplacement d'un ancien diesel (prime à la casse)</Titre>
<Fiche ID="F32487">Prime à la conversion : achat d'un véhicule neuf électrique ou hybride</Fiche>
<Fiche ID="F32767">Aide pour une personne non imposable : achat d'un véhicule moins polluant</Fiche>
</SousDossier>
<SousDossier ID="N18131-3">
<Titre>Pollution</Titre>
<Fiche ID="F19911">Malus et taxe CO₂ pour un véhicule polluant</Fiche>
<Fiche ID="F10332">Pollution atmosphérique</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Pollution</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Une écotaxe dite <Expression>malus</Expression> et une taxe dite <Expression>taxe CO₂ </Expression>sanctionnent financièrement l'acquisition ou la location (sous conditions) d'un véhicule particulier (VP) neuf ou d'occasion polluant, en fonction de la quantité de dioxyde de carbone (CO₂) émise par le véhicule.
		
			À ces taxes s'ajoute, les années suivant l'achat ou la location, le paiement d'une taxe annuelle sur la détention de VP polluants pour les véhicules dont le taux de CO₂ est le plus élevé.</Paragraphe>
</Texte>
</Introduction>
<ListeSituations affichage="onglet"><Situation>
<Titre>Véhicule jamais immatriculé en France</Titre>
<Texte><Chapitre>
<Titre>
<Paragraphe>Dans quel cas la taxe est-elle due ?</Paragraphe>
</Titre><Paragraphe>Le paiement de l'écotaxe additionnelle (malus) sur le certificat d'immatriculation (ex-carte grise) se fait lors de la 1ère immatriculation en France d'un <LienIntra LienID="R10269" type="Définition de glossaire">véhicule particulier</LienIntra> polluant :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>acheté ou loué (avec option d'achat de longue durée de 2 ans minimum) neuf en France ou à l'étranger et importé en France,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou acheté <LienIntra LienID="R14737" type="Définition de glossaire">d'occasion</LienIntra> à l'étranger et importé en France.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Ce paiement a lieu lors de l'immatriculation. C'est l'année de l'immatriculation qui compte, pas la date de commande ou d'achat.</Paragraphe>
<Paragraphe>Pour les véhicules introduits en France après avoir été immatriculés dans un autre pays, la taxe est réduite d'un dixième par année entamée depuis cette 1ère immatriculation.</Paragraphe>
<Paragraphe>Si l'immatriculation est faite par le concessionnaire, le malus est inclus dans la facture dans la catégorie des "frais d'immatriculation". Si l'acheteur fait lui-même la démarche en préfecture, le malus est réglé en même temps que le certificat d'immatriculation.</Paragraphe>
<Paragraphe>La taxe est calculée en fonction du nombre de grammes de CO₂  émis par kilomètre.</Paragraphe>
<Paragraphe>Pour connaître ce niveau d'émission, il est possible d'utiliser le téléservice<LienInterne LienPublication="R18602" type="Simulateur" audience="Particuliers"/> en indiquant, la marque, le modèle et la version de la voiture.</Paragraphe><ServiceEnLigne ID="R18602" URL="http://carlabelling.ademe.fr/recherche/index?category" type="Simulateur">
<Titre>Connaître le taux d'émission de CO2 de sa voiture</Titre>
<Source ID="R30674">Agence de l'environnement et de la maîtrise de l'énergie (Ademe)</Source>
<Introduction>
<Texte><Paragraphe>Permet de connaître le niveau d'émission de CO2 de son véhicule en indiquant la marque, le modèle et la version de la voiture</Paragraphe>
</Texte>
</Introduction>
</ServiceEnLigne>


</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Montant de la taxe</Paragraphe>
</Titre><BlocCas affichage="radio">
<Cas>
<Titre>
<Paragraphe>Véhicule ayant fait l'objet d'une réception communautaire</Paragraphe>
</Titre><Paragraphe>Pour les véhicules ayant fait l'objet d'une <LienIntra LienID="R1222" type="Définition de glossaire">réception communautaire</LienIntra>, les montants du malus sont les suivants :</Paragraphe>
<Tableau>
<Titre>Montants du malus pour les années 2015 et 2016</Titre>
<Colonne largeur="50" type="normal"/>
<Colonne largeur="28" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Taux d'émission de CO₂ par kilomètre</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Montant </Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Inférieur ou égal à 130 grammes</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>0 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 131 à 135 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>150 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 136 à 140 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>250 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 141 à 145 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>500 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 146 à 150 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>900 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 151 à 155 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>1 600 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 156 et 175 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>2 200 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 176 à 180 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>3 000 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 181 à 185 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>3 600 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 186 et 190 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>4 000 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 191 et 200 grammes inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>6 500 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Plus de 200 grammes</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>8 000 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
</Tableau>
<ANoter>
<Titre>À noter</Titre><Paragraphe>les familles nombreuses peuvent bénéficier d'une <LienInterne LienPublication="F31484" type="Fiche Question-réponse" audience="Particuliers">minoration du malus</LienInterne>.</Paragraphe>
</ANoter>
</Cas>
<Cas>
<Titre>
<Paragraphe>Véhicule n'ayant pas fait l'objet d'une réception communautaire</Paragraphe>
</Titre><Paragraphe>Pour les véhicules n'ayant pas fait l'objet d'une <LienIntra LienID="R1222" type="Définition de glossaire">réception communautaire</LienIntra>, les montants sont les suivants :</Paragraphe>
<Tableau>
<Titre>Montants du malus pour les années 2015 et 2016</Titre>
<Colonne largeur="50" type="normal"/>
<Colonne largeur="50" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Puissance fiscale</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Montant </Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Inférieure ou égale à 5 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>0 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>6 et 7 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>1 500 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>8 et 9 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>2 000 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>10 et 11 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>3 600 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>De 12 à 16 CV inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>6 000 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Plus de 16 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>8 000 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
</Tableau>

</Cas>
</BlocCas>

</Chapitre>
</Texte>
</Situation>
<Situation>
<Titre>Véhicule déjà immatriculé en France</Titre>
<Texte><Chapitre>
<Titre>
<Paragraphe>Dans quel cas la taxe est-elle due ?</Paragraphe>
</Titre><Paragraphe>Pour les véhicules mis en service depuis le 1er juin 2004, une taxe additionnelle (taxe CO₂), en cas d'achat d'un véhicule d'occasion polluant, est due lors de l'établissement du certificat d'immatriculation suite à l'achat.</Paragraphe>
<Paragraphe>Elle n'est pas due en cas</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>de délivrance d'un nouveau certificat à la suite d'un changement d'état civil, de domicile ou de dénomination sociale,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou de délivrance d'un duplicata.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Elle est calculée différemment selon que le véhicule a fait l'objet ou non d'une réception communautaire.</Paragraphe>
<Paragraphe>Pour les véhicules spécialement équipés pour fonctionner au moyen du superéthanol E85, le montant de la taxe est réduit de 50%.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Montant</Paragraphe>
</Titre><BlocCas affichage="radio">
<Cas>
<Titre>
<Paragraphe>Véhicule ayant fait l'objet d'une réception communautaire</Paragraphe>
</Titre><Paragraphe>Pour les véhicules d'occasion ayant fait l'objet d'une <LienIntra LienID="R1222" type="Définition de glossaire">réception communautaire</LienIntra>, la taxe additionnelle est calculée selon le nombre de grammes de CO₂  émis par kilomètre, selon le barème suivant :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>
<Valeur>2 €</Valeur> par gramme de dioxyde de carbone pour un taux supérieur à 200 g CO₂  / km et inférieur ou égal à 250 g CO₂  / km,</Paragraphe>
</Item>
<Item>
<Paragraphe>
<Valeur>4 €</Valeur> par gramme de dioxyde de carbone pour un taux supérieur à 250 g CO₂  / km.</Paragraphe>
</Item>
</Liste>

</Cas>
<Cas>
<Titre>
<Paragraphe>Véhicule n'ayant pas fait l'objet d'une réception communautaire</Paragraphe>
</Titre><Paragraphe>Pour les véhicules n'ayant pas fait l'objet d'une <LienIntra LienID="R1222" type="Définition de glossaire">réception communautaire</LienIntra>, la taxe est calculée en fonction de la puissance fiscale (CV) :</Paragraphe>
<Tableau>
<Titre>Montant de la taxe en 2015 et 2016</Titre>
<Colonne largeur="50" type="normal"/>
<Colonne largeur="50" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Puissance fiscale (CV) </Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Montant de la taxe </Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Puissance inférieure à 10 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>0 €</Valeur> </Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Puissance égale à 10 CV et inférieure à 15 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>100 €</Valeur> </Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Puissance égale et supérieure à 15 CV</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>300 €</Valeur> </Paragraphe>
</Cellule>
</Rangée>
</Tableau>

</Cas>
</BlocCas>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Taxe annuelle sur la détention d'un véhicule particulier polluant</Paragraphe>
</Titre><Paragraphe>Une taxe annuelle de <Valeur>160 €</Valeur> s'applique aux véhicules particuliers les plus polluants, immatriculés pour la première fois en France.</Paragraphe>
<Paragraphe>Elle est due, à partir de l'année qui suit la délivrance du certificat d'immatriculation du véhicule.</Paragraphe>
<Paragraphe>Vous êtes concerné si vous êtes  propriétaire du véhicule polluant ou si vous avez  un contrat de location avec option d'achat ou contrat pour une durée de 2 ans).</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>si vous êtes <LienInterne LienPublication="F31484" type="Fiche Question-réponse" audience="Particuliers">en situation de handicap</LienInterne>,  vous êtes exonéré du paiement de cette taxe annuelle.</Paragraphe>
</ANoter><Paragraphe>Vous êtes concerné si  le taux d'émission de dioxyde de carbone (CO₂)  du véhicule excède la limite suivante :</Paragraphe>
<Tableau>
<Titre>Taux d'émission CO₂</Titre>
<Colonne largeur="26" type="header"/>
<Colonne largeur="16" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Année de la 1<Exposant>ère</Exposant> immatriculation</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>CO₂ (en g/km)</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>2009</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>250</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>2010</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>245</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>2011</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>245</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>2012 ou après</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>190</Paragraphe>
</Cellule>
</Rangée>
</Tableau>
<Paragraphe>Un titre de perception est adressé chaque année au propriétaire ou au locataire par le centre du Trésor public de son lieu de résidence.</Paragraphe>

</Chapitre>
</Texte>
</Situation>
</ListeSituations><OuSAdresser ID="R15019" type="Local personnalisable">
<Titre>Service en charge des impôts (trésorerie, centre des impôts fonciers...)</Titre>
<Complement>Pour obtenir des informations sur le paiement</Complement>
<PivotLocal>centre_impots</PivotLocal>
<RessourceWeb URL="http://www.impots.gouv.fr/portal/dgi/public/contacts?pageId=contacts&amp;sfid=07#services"/>
<Source ID="R30612">Ministère en charge des finances</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000018619161&amp;cidTexte=LEGITEXT000006069577" ID="R19912">
<Titre>Code général des impôts : article 1010 bis</Titre>
<Complement>Taxe additionnelle à la taxe sur les certificats d'immatriculation</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000018029918&amp;cidTexte=LEGITEXT000006069577" ID="R19913">
<Titre>Code général des impôts : articles 1011 bis et 1011 ter</Titre>
<Complement>Malus applicable aux voitures particulières les plus polluantes</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000020694387&amp;cidTexte=LEGITEXT000006069574" ID="R15657">
<Titre>Code général des impôts, annexe 3 : article 313-0 BR ter</Titre>
<Complement>Malus applicable aux voitures particulières les plus polluantes</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://bofip.impots.gouv.fr/bofip/2707-PGP.html?ftsq=malus&amp;identifiant=BOI-ENR-TIM-20-60-30-20120912" ID="R13608">
<Titre>BOFIP-Impôts n° BOI-ENR-TIM-20-60-30-20120912 relatif au malus</Titre>
<Complement>Taxe additionnelle à la taxe sur les certificats d'immatriculation des véhicules : malus applicable aux voitures particulières les plus polluantes</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029574487" ID="R259">
<Titre>Décret n°2014-1171 du 13 octobre 2014 relatif au report définitif de la date limite d'émission des titres de perception de la taxe annuelle sur les véhicules les plus polluants</Titre>
</Reference>
<ServiceEnLigne ID="R13614" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_5813/fichedescriptiveformulaire_5813.pdf" type="Formulaire">
<Titre>Demande de remboursement de la taxe additionnelle à la taxe sur les certificats d'immatriculation des véhicules</Titre>
<Source ID="R30612">Ministère en charge des finances</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R18602" URL="http://carlabelling.ademe.fr/recherche/index?category" type="Simulateur">
<Titre>Connaître le taux d'émission de CO2 de sa voiture</Titre>
<Source ID="R30674">Agence de l'environnement et de la maîtrise de l'énergie (Ademe)</Source>
</ServiceEnLigne>
<Definition ID="R10269">
<Titre>Véhicule particulier (VP)</Titre>
<Texte>
				<Paragraphe>Véhicule à moteur, construit et conçu pour le transport de personnes, ayant au moins 4 roues, comportant, outre le siège du conducteur, 8 places assises au maximum et dont le poids total en charge autorisé (PTAC) est inférieur à 3,5 tonnes</Paragraphe>
			</Texte>
</Definition>
<Definition ID="R1222">
<Titre>Réception CE ou communautaire des véhicules</Titre>
<Texte>
				<Paragraphe>Acte attestant qu'un véhicule satisfait aux normes techniques exigées par l'Union européenne pour sa mise en circulation</Paragraphe>
			</Texte>
</Definition>
<Definition ID="R14737">
<Titre>Véhicule d'occasion (définition fiscale)</Titre>
<Texte>
				<Paragraphe>Véhicule âgé de plus de 6 mois à la date de livraison et ayant parcouru plus de 6 000 kilomètres</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F31484" audience="Particuliers">Quels sont les cas de minoration ou d'exonération du malus et de la taxe CO₂ ?</QuestionReponse>
<InformationComplementaire ID="R35964" date="2014-10-10">
<Titre>Certificat de conformité à un type national</Titre>
<Texte>
				<Paragraphe>Document, délivré par un constructeur, attestant que le véhicule est conforme aux normes techniques obligatoires de circulation propres à un pays.</Paragraphe>
				<Paragraphe>Ne concerne que les véhicules routiers de transport de marchandises ou de personnes et les véhicules agricoles.</Paragraphe>
				<Paragraphe>Ne concerne donc pas les véhicules particuliers, ni les 2 ou 3 roues.</Paragraphe>
			</Texte>
</InformationComplementaire>
<InformationComplementaire ID="R12142" date="2014-10-10">
<Titre>Certificat de conformité à un type CE (automobile)</Titre>
<Texte>
				<Paragraphe>Appelé aussi parfois certificat de conformité communautaire ou COC (<TermeEtranger langue="en">certificate of conformity</TermeEtranger>).</Paragraphe>
				<Paragraphe>Document, délivré par le constructeur au moment de la première vente du véhicule, attestant que le véhicule est conforme aux normes techniques européennes obligatoires pour circuler.</Paragraphe>
				<Paragraphe>Ne concerne que les véhicules particuliers, les 2 et 3 roues.</Paragraphe>
				<Paragraphe>Pour obtenir une copie de ce document, contactez le constructeur ou son représentant en France. Cette copie est généralement payante.</Paragraphe>
			</Texte>
</InformationComplementaire>

</Publication>
