<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F75" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Répartition des horaires de travail dans le secteur privé</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>Si l'entreprise alterne périodes de haute et de basse activité, elle peut prévoir un aménagement des horaires de travail du salarié.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-09-16</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F75</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000019356978&amp;cidTexte=LEGITEXT000006072050, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000019725777&amp;cidTexte=LEGITEXT000006072050</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N458</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N458">Temps de travail dans le secteur privé</Niveau>
<Niveau ID="F75" type="Fiche d'information">Répartition des horaires de travail dans le secteur privé</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19964">Temps de travail et congés</SousThemePere><DossierPere ID="N458">
<Titre>Temps de travail dans le secteur privé</Titre><SousDossier ID="N458-1">
<Titre>Durée du travail</Titre>
<Fiche ID="F1911">Durée légale du travail</Fiche>
<Fiche ID="F2216">Durée légale du travail des jeunes</Fiche>
<Fiche ID="F19261">Durée du travail du salarié : forfait en heures ou en jours</Fiche>
</SousDossier>
<SousDossier ID="N458-2">
<Titre>Travail à temps partiel</Titre>
<Fiche ID="F874">Bénéficiaires du temps partiel</Fiche>
<Fiche ID="F1915">Contrat de travail</Fiche>
<Fiche ID="F32428">Durée de travail</Fiche>
<Fiche ID="F878">Heures complémentaires</Fiche>
<Fiche ID="F876">Droits du salarié à temps partiel</Fiche>
<Fiche ID="F2243">Travail à temps partiel pour raisons familiales</Fiche>
<Fiche ID="F2247">Travail intermittent</Fiche>
</SousDossier>
<SousDossier ID="N458-3">
<Titre>Repos et jours fériés</Titre>
<Fiche ID="F990">Repos quotidien</Fiche>
<Fiche ID="F2327">Repos hebdomadaire</Fiche>
<Fiche ID="F13887">Repos dominical</Fiche>
<Fiche ID="F2405">Jours fériés et ponts</Fiche>
<Fiche ID="F1907">Compte épargne-temps (CET)</Fiche>
</SousDossier>
<SousDossier ID="N458-4">
<Titre>Aménagement du temps de travail</Titre>
<Fiche ID="F75">Répartition des horaires</Fiche>
<Fiche ID="F125">Récupération des heures perdues</Fiche>
<Fiche ID="F74">Horaires individualisés</Fiche>
</SousDossier>
<SousDossier ID="N458-5">
<Titre>Heures supplémentaires, équivalence et astreintes</Titre>
<Fiche ID="F2391">Heures supplémentaires</Fiche>
<Fiche ID="F1903">Heures d'équivalence</Fiche>
<Fiche ID="F20873">Astreintes</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Aménagement du temps de travail</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Si l'entreprise alterne périodes de haute et de basse activité, elle peut prévoir un aménagement des horaires de travail. Cet aménagement peut conduire le salarié à travailler soit plus de 35 heures par semaine, soit moins, en fonction de l'activité de l'entreprise. Les conditions de mise en place de l'aménagement des horaires varient selon qu'il est prévu par accord collectif ou directement par l'employeur.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Aménagement des horaires prévu par accord collectif</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Durée de l'aménagement</Paragraphe>
</Titre><Paragraphe>L'aménagement des horaires de travail est possible sur une période supérieure à la semaine et au plus égale à l'année. Les conditions de l'aménagement des horaires sont établies par un accord collectif d'entreprise ou d'établissement (ou, à défaut, par convention ou accord de branche).</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Contenu de l'aménagement</Paragraphe>
</Titre><Paragraphe>L'accord collectif  prévoit au minimum les éléments suivants :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>conditions et délais de prévenance de chaque salarié salarié en cas de changement de durée ou d'horaires de travail,</Paragraphe>
</Item>
<Item>
<Paragraphe>limites pour le décompte des heures supplémentaires,</Paragraphe>
</Item>
<Item>
<Paragraphe>conditions de prise en compte, pour la rémunération des salariés, des absences et des arrivées et départs en cours de période.</Paragraphe>
</Item>
<Item>
<Paragraphe> conditions de récupération du salarié en-dehors des périodes forte activité. Il peut, par exemple, bénéficier de jours de repos ou travailler moins de 35 heures par semaine.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si l'accord  s'applique au salarié à temps partiel,  il prévoit les conditions de modification de la répartition de la durée et des horaires de travail. Il précise également le mode  de communication au salarié de ces aménagements.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Calcul des heures supplémentaires</Paragraphe>
</Titre><Paragraphe>La durée de travail du salarié variant en fonction des périodes d'activité, sont considérées comme des heures supplémentaires :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>les  heures effectuées au-delà d'un plafond fixé à 1 607 heures par an (ou moins,  si l'accord le prévoit),</Paragraphe>
</Item>
<Item>
<Paragraphe>ou les  heures effectuées au-delà de la durée moyenne de 35 heures par semaine (calculée sur la période de référence fixée par l'accord collectif).</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Lissage de la rémunération</Paragraphe>
</Titre><Paragraphe>L'accord peut prévoir que la rémunération mensuelle est indépendante de l'horaire réel. Dans ce cas, il fixe les conditions selon lesquelles la rémunération est calculée.</Paragraphe>
<Paragraphe>Si des heures supplémentaires sont accomplies, elles doivent être payées avec le salaire du mois au cours duquel elles ont été faites.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Accord individuel du salarié</Paragraphe>
</Titre><Paragraphe>Pour un salarié à temps plein, la répartition des horaires sur tout ou partie de l'année, prévu par accord collectif, ne constitue pas une modification du contrat de travail. L'accord individuel préalable de chaque salarié concerné n'est donc pas requis.</Paragraphe>
<Paragraphe>À l'inverse, l'accord individuel préalable est nécessaire pour tout salarié à temps partiel concerné.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Cas particulier : en cas d'accord d'aménagement conclu avant le 21 août 2008</Paragraphe>
</Titre><Paragraphe>Tout accord collectif de répartition des horaires de travail conclu avant le 21 août 2008 (prévoyant soit le travail par cycle, soit la modulation du temps de travail, soit des journées de réduction du temps de travail) reste applicable tant qu'il n'est pas remis en cause par les organisations qui l'ont signé.</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Travail par cycle</Paragraphe>
</Titre><Paragraphe>L'accord peut prévoir une organisation du temps de travail sous forme de cycles, dont la durée est fixée à quelques semaines, afin de s'adapter aux variations régulières d'activités.</Paragraphe>
<Paragraphe>La répartition de la durée du travail à l'intérieur d'un cycle se répète à l'identique d'un cycle à l'autre.</Paragraphe>
<Paragraphe>À l'intérieur d'un cycle, la durée hebdomadaire moyenne de travail est de 35 heures. Les heures effectuées au-delà sont considérées comme des heures supplémentaires.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Modulation du temps de travail</Paragraphe>
</Titre><Paragraphe>La modulation du temps de travail permet de répartir la durée du travail sur tout ou partie de l'année, en fonction de l'activité. Les horaires de travail sont augmentés en période de haute activité et réduits en période de basse activité.</Paragraphe>
<Paragraphe>La durée de travail ne doit pas dépasser 1 607 heures par an (ou moins, si la convention ou l'accord le prévoit).</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Réduction du temps de travail (RTT)</Paragraphe>
</Titre><Paragraphe>La durée hebdomadaire de travail peut être réduite en-deçà de 39 heures par l'attribution de journées ou demi-journées de repos.</Paragraphe>
<Paragraphe>Les jours de RTT sont répartis :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit sur tout ou partie de l'année, dans des conditions définies par la convention ou l'accord (il convient de s'y référer pour connaître l'ensemble des dispositions prévues),</Paragraphe>
</Item>
<Item>
<Paragraphe> soit sur des périodes de 4 semaines, selon un calendrier préalablement établi.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Aménagement des horaires en l'absence d'accord collectif</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Aménagement des horaires par l'employeur</Paragraphe>
</Titre><Paragraphe>En l'absence d'accord collectif, l'employeur conserve la possibilité d'aménager les horaires. Il établit le programme indicatif de la variation de la durée du travail. Ce programme est soumis à l'avis du comité d'entreprise ou, à défaut, aux délégués du personnel, s'ils existent.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Durée</Paragraphe>
</Titre><Paragraphe>La répartition de la durée du travail ne peut être effectuée que sur une période de 4 semaines maximum.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Heures supplémentaires</Paragraphe>
</Titre><Paragraphe>La durée de travail du salarié variant en fonction des périodes d'activité, sont considérées comme des heures supplémentaires :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>toute heure effectuée au-delà de 39 heures par semaine,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou toute heure effectuée au-delà de la durée moyenne de 35 heures hebdomadaires (calculée sur la période de référence de quatre semaines au plus).</Paragraphe>
</Item>
</Liste>
<Paragraphe>En cas d'arrivée ou de départ du salarié en cours de période de 4 semaines, les heures accomplies au-delà de 35 heures hebdomadaires sont considérées comme des heures supplémentaires.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>En cas de changement d'horaires</Paragraphe>
</Titre><Paragraphe>Tout salarié concerné par un changement de ses horaires de travail est prévenu au moins 7 jours ouvrés avant la date à laquelle ce changement intervient.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Lissage de la rémunération</Paragraphe>
</Titre><Paragraphe>La rémunération mensuelle des salariés des entreprises organisant des périodes de travail sur quatre semaines au plus est indépendante de l'horaire réel. Elle est calculée sur la base de 35 heures hebdomadaires.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Entreprise fonctionnant en continu</Paragraphe>
</Titre><Paragraphe>Si l'entreprise fonctionne en continu, l'employeur peut décider d'organiser le temps de travail sur plusieurs semaines.</Paragraphe>
<Paragraphe>Il n'a pas l'obligation de conclure un accord au préalable.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Respect des durées maximales de travail</Paragraphe>
</Titre><Paragraphe>Quel que soit le mode d'aménagement des horaires de travail, les durées maximales quotidiennes et hebdomadaires de travail prévues par la loi (10h par jour, 48h par semaine ou 44h par semaine en moyenne pendant 12 semaines d'affilée au maximum) doivent être respectées.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Information du salarié</Paragraphe>
</Titre><Paragraphe>La période de référence et la répartition de la durée du travail, pour chaque semaine incluse dans cette période, doivent être affichées.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R250" type="Centre de contact">
<Titre>3939 Allô Service Public</Titre>
<Complement>Pour toute information complémentaire</Complement><Texte>
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			3939 (coût : <Valeur>0,15 €</Valeur> TTC la minute)
		</Paragraphe>
								<Paragraphe>
			Du lundi au vendredi de 8h30 à 18h.
		</Paragraphe>
								<Paragraphe>
			Répond aux demandes de renseignement administratif concernant les droits et démarches.
		</Paragraphe>
								<Paragraphe>
			Depuis l'étranger ou hors métropole : +33 (0)1 73 60 39 39 uniquement depuis un poste fixe (coût d'une communication + coût de l'appel international variable selon les pays et les opérateurs).
		</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R3040" type="Local personnalisable">
<Titre>Votre direction des ressources humaines (DRH)</Titre>
<Complement>Pour toute information complémentaire</Complement>
<PivotLocal>drh</PivotLocal>
</OuSAdresser>
<OuSAdresser ID="R756" type="Local personnalisable">
<Titre>Vos représentants du personnel</Titre>
<Complement>Pour toute information complémentaire</Complement>
<PivotLocal>representant_personnel</PivotLocal>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000019356978&amp;cidTexte=LEGITEXT000006072050" ID="R38244">
<Titre>Code du travail : articles L3122-1 à L3122-6</Titre>
<Complement>Aménagement en cas d'accord collectif</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000019725777&amp;cidTexte=LEGITEXT000006072050" ID="R38245">
<Titre>Code du travail : articles D3122-7-1 à D3122-7-3</Titre>
<Complement>Aménagement en l'absence d'accord collectif</Complement>
</Reference>
</Publication>
