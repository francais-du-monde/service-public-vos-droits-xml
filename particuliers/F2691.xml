<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F2691" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Travail de nuit : protection de la salariée enceinte</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>La salariée enceinte bénéficie d'une protection spéciale lui permettant de ne pas travailler de nuit.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-01-07</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F2691</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006198526&amp;cidTexte=LEGITEXT000006072050</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N492</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N492">Conditions de travail dans le secteur privé</Niveau>
<Niveau ID="F2691" type="Fiche d'information">Travail de nuit : protection de la salariée enceinte</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19965">Santé, sécurité et conditions de travail</SousThemePere><DossierPere ID="N492">
<Titre>Conditions de travail dans le secteur privé</Titre><SousDossier ID="N492-1">
<Titre>Hygiène, sécurité et conditions de travail</Titre>
<Fiche ID="F2210">Obligations de l'employeur</Fiche>
<Fiche ID="F2344">Jeunes dans l'entreprise</Fiche>
<Fiche ID="F58">Travailleur à domicile</Fiche>
<Fiche ID="F2211">Médecine du travail</Fiche>
<Fiche ID="F15504">Compte de prévention pénibilité</Fiche>
<Fiche ID="F13851">Télétravail</Fiche>
</SousDossier>
<SousDossier ID="N492-2">
<Titre>Travail de nuit</Titre>
<Fiche ID="F2212">Principes généraux</Fiche>
<Fiche ID="F1688">Jeunes de moins de 18 ans</Fiche>
<Fiche ID="F2691">Protection de la salariée enceinte</Fiche>
</SousDossier>
<SousDossier ID="N492-3">
<Titre>Conditions de travail : informations diverses</Titre>
<Fiche ID="F31854">Évaluation du salarié</Fiche>
<Fiche ID="F1905">Règlement intérieur d'une entreprise</Fiche>
<Fiche ID="F78">Convention collective</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Travail de nuit</SousDossierPere>
<Introduction>
<Texte><Paragraphe>La salariée enceinte bénéficie d'une protection spéciale lui permettant de ne pas travailler de nuit.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Demande de changement d'affectation</Paragraphe>
</Titre><Paragraphe>La salariée enceinte qui <LienInterne LienPublication="F2212" type="Fiche d'information" audience="Particuliers">travaille de nuit</LienInterne> est, à sa demande, affectée à un poste de jour pendant la durée de sa grossesse. La salariée ayant accouché bénéficie des mêmes dispositions jusqu'à la fin du congé post-natal.</Paragraphe>
<Paragraphe>Le médecin du travail peut également demander par écrit l'affectation temporaire à un poste de jour, s'il constate que le poste de nuit est incompatible avec son état. Dans ce cas, cette affectation peut être prolongée pendant le congé postnatal et après son retour de congé pour une durée n'excédant pas 1 mois.</Paragraphe>
<Paragraphe>Ce changement d'affectation ne doit entraîner aucune diminution de rémunération.</Paragraphe>
<Paragraphe>L'affectation de la salariée  dans un autre établissement est possible si elle a donné son accord.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>En cas d'impossibilité de changer d'affectation</Paragraphe>
</Titre><Paragraphe>Si l'employeur est dans l'impossibilité de proposer à la salariée un autre poste, il doit l'informer par écrit des motifs empêchant le reclassement.</Paragraphe>
<Paragraphe>Dans ce cas, le contrat de travail est suspendu jusqu'à la date de début du congé de maternité, mais la salariée bénéficie d'une garantie de rémunération.</Paragraphe>
<Paragraphe>La garantie de rémunération est composée :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>d'allocations journalières versées par la caisse primaire d'assurance maladie (CPAM),</Paragraphe>
</Item>
<Item>
<Paragraphe>d'un complément à la charge de l'employeur.</Paragraphe>
</Item>
</Liste>

</Chapitre>
</Texte><OuSAdresser ID="R3040" type="Local personnalisable">
<Titre>Votre direction des ressources humaines (DRH)</Titre>
<Complement>Pour toute information complémentaire</Complement>
<PivotLocal>drh</PivotLocal>
</OuSAdresser>
<OuSAdresser ID="R756" type="Local personnalisable">
<Titre>Vos représentants du personnel</Titre>
<Complement>Pour toute information complémentaire</Complement>
<PivotLocal>representant_personnel</PivotLocal>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006198526&amp;cidTexte=LEGITEXT000006072050" ID="R1914">
<Titre>Code du travail : articles L1225-9 à L1225-11</Titre>
</Reference>
<QuestionReponse ID="F1144" audience="Particuliers">Une salariée enceinte est-elle obligée de révéler sa grossesse à son employeur ?</QuestionReponse>
<QuestionReponse ID="F2330" audience="Particuliers">Grossesse et autorisation d'absence : quelles sont les règles ?</QuestionReponse>
<QuestionReponse ID="F2775" audience="Particuliers">Une salariée enceinte peut-elle refuser d'effectuer certaines tâches ?</QuestionReponse>
</Publication>
