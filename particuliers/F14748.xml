<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F14748" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Qu'est-ce que la taxe annuelle sur les loyers élevés des micro-logements ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Logement</dc:subject>
<dc:description>Certains propriétaires de logements situés dans les grandes agglomérations doivent s'acquitter d'une taxe annuelle.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-01-29</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F14748</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000025432073&amp;cidTexte=LEGITEXT000006069577, http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000025094293&amp;cidTexte=LEGITEXT000006069574, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025059888, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029574683, http://bofip.impots.gouv.fr/bofip/9396-PGP.html</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N337</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19808">Logement</Niveau>
<Niveau ID="N337">Location immobilière : loyer</Niveau>
<Niveau ID="F14748" type="Fiche Question-réponse">Qu'est-ce que la taxe annuelle sur les loyers élevés des micro-logements ?</Niveau>
</FilDAriane>
<Theme ID="N19808">
<Titre>Logement</Titre>
</Theme>
<SousThemePere ID="N289">Location immobilière</SousThemePere><DossierPere ID="N337">
<Titre>Location immobilière : loyer</Titre><SousDossier ID="N337-1">
<Titre>Loyer d'un logement privé</Titre>
<Fiche ID="F1310">Fixation et paiement</Fiche>
<Fiche ID="F1311">Révision annuelle</Fiche>
<Fiche ID="F1312">Réévaluation d'un loyer sous-évalué</Fiche>
<Fiche ID="F33066">Réévaluation d'un loyer sur-évalué (action en diminution de loyer)</Fiche>
<Fiche ID="F13723">Indice de référence des loyers (IRL)</Fiche>
</SousDossier>
<SousDossier ID="N337-2">
<Titre>Loyer d'un logement social</Titre>
<Fiche ID="F1317">Détermination du loyer</Fiche>
<Fiche ID="F21051">Supplément de loyer de solidarité (SLS)</Fiche>
</SousDossier>
</DossierPere>

<Texte><Paragraphe>Certains propriétaires de logements situés sur des communes classées en zone A, c'est-à-dire dans les grandes agglomérations, doivent s'acquitter d'une taxe annuelle.</Paragraphe>
<Paragraphe>Il s'agit des propriétaires qui louent :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>un logement vide</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>ou un logement meublé pour une durée d'au moins 9 mois.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Les logements susceptibles d'être taxés doivent :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>comporter une surface inférieure ou égale à 14 m²,</Paragraphe>
</Item>
<Item>
<Paragraphe>et être loués en 2016 à un montant de loyer mensuel, charges non comprises, supérieur à un loyer de référence fixé à <Valeur>41,64 €</Valeur> par mètre carré de <LienIntra LienID="R18320" type="Définition de glossaire">surface habitable</LienIntra>  (<Valeur>41,61 €</Valeur> en 2015). </Paragraphe>
</Item>
</Liste>
<Paragraphe>Ce montant est révisé au 1<Exposant>er</Exposant> janvier de chaque année en fonction de <LienInterne LienPublication="F13723" type="Fiche d'information" audience="Particuliers">l'indice de référence des loyers (IRL)</LienInterne> du 2<Exposant>ème</Exposant> trimestre de l'année précédente.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>les résidences avec services (résidences pour étudiants, pour personnes âgées ou de tourisme) ne sont pas concernées par cette taxe.</Paragraphe>
</ASavoir><Paragraphe>Le montant de la taxe dépend d'un taux qui varie en fonction de l'écart entre le loyer pratiqué, hors charges, et le loyer de référence.</Paragraphe>
<Tableau>
<Titre>Barème de la taxe sur les loyers des micro-logements</Titre>
<Colonne largeur="50" type="normal"/>
<Colonne largeur="50" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Écart entre le loyer mensuel pratiqué et loyer de référence</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Taux applicable</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Inférieur à 15 %</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>10 %</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Supérieur ou égal à 15 % et inférieur à 30 %</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>18 %</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Supérieur ou égal à 30 % et inférieur à 55 %</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>25 %</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Supérieur ou égal à 55 % et inférieur à 90 %</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>33 %</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Supérieur ou égal à 90 %</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>40 %</Paragraphe>
</Cellule>
</Rangée>
</Tableau>
<Paragraphe>Ainsi, supposons un loyer d'un montant mensuel de <Valeur>750 €</Valeur> pour un logement dont la surface habitable est de 14 m², soit <Valeur>53,57 €</Valeur> le m². L'écart entre le loyer pratiqué et le loyer de référence s'élève à : (<Valeur>53,57 €</Valeur> - <Valeur>41,64 €</Valeur>) / <Valeur>41,64 €</Valeur> = 29,49 %. Le taux applicable est alors de 18 % puisqu'il se situe dans la tranche "Supérieur ou égal à 15 % et inférieur à 30 %". Le propriétaire serait donc redevable d'une taxe qui s'élève à : (<Valeur>750 €</Valeur> x 12) x 18 % = <Valeur>1620 €</Valeur>.</Paragraphe>
<Paragraphe>La taxe s'ajoute à l'impôt sur le revenu et n'est pas déductible.</Paragraphe>
<Paragraphe>Le propriétaire doit déclarer le montant des loyers soumis à cette taxe en même temps que ses revenus sur un imprimé joint à la déclaration sur les revenus.</Paragraphe>
<Paragraphe>La première année, le paiement interviendra </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit lors du paiement du troisième tiers de l'impôt sur le revenu, </Paragraphe>
</Item>
<Item>
<Paragraphe>soit lors de la régularisation de la mensualisation.</Paragraphe>
</Item>
</Liste>

</Texte><Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000025432073&amp;cidTexte=LEGITEXT000006069577" ID="R36680">
<Titre>Code général des impôts : article 234</Titre>
<Complement>Taxe sur les loyers élevés des logements de petite surface</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000025094293&amp;cidTexte=LEGITEXT000006069574" ID="R36681">
<Titre>Code général des impôts, annexe 3 : article 58 P</Titre>
<Complement>Communes concernées et montant du loyer mensuel au-delà duquel le logement est soumis à la taxe</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025059888" ID="R21004">
<Titre>Décret n°2011-2060 du 30 décembre 2011 relatif à la taxe sur les loyers élevés des logements de petite surface</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000029574683" ID="R15107">
<Titre>Arrêté du 30 septembre 2014 relatif au classement des communes par zone applicable à certaines aides au logement</Titre>
<Complement>Pour connaître la liste des communes classées en zone A</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://bofip.impots.gouv.fr/bofip/9396-PGP.html" ID="R36682">
<Titre>Bofip-impôts n°BOI-RFPI-CTRL-10 relatif à la taxe sur les loyers élevés des logements de petite surface ("Taxe Apparu")</Titre>
</Reference>
<ServiceEnLigne ID="R1281" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10424/fichedescriptiveformulaire_10424.pdf" numerocerfa="10330*20" autrenumero="2042" type="Formulaire">
<Titre>Déclaration 2016 des revenus</Titre>
<Source ID="R30612">Ministère en charge des finances</Source><NoticeLiee ID="R15720" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10420/fichedescriptiveformulaire_10420.pdf" numerocerfa="50796#16" format="application/pdf" poids="668.4 KB">Notice pour remplir votre déclaration de revenus</NoticeLiee>
<NoticeLiee ID="R40459" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10407/fichedescriptiveformulaire_10407.pdf" format="application/pdf" poids="153.0 KB">Déclaration des revenus 2015 - fiche facultative de calculs</NoticeLiee>
<NoticeLiee ID="R36611" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10403/fichedescriptiveformulaire_10403.pdf" numerocerfa="50988#12">Notice revenus 2015 : résidence alternée d'enfants mineurs</NoticeLiee>
<NoticeLiee ID="R36724" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10401/fichedescriptiveformulaire_10401.pdf" numerocerfa="50688#17">Notice revenus 2015 : allocations pour frais d'emploi</NoticeLiee>
<NoticeLiee ID="R36723" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10402/fichedescriptiveformulaire_10402.pdf" numerocerfa="50883#15" format="application/pdf" poids="155.6 KB">Notice revenus de 2015 : plafonnement des effets du quotient familial.</NoticeLiee>
<NoticeLiee ID="R36692" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10416/fichedescriptiveformulaire_10416.pdf" numerocerfa="50149#20">Notice revenus 2015 : Revenus exceptionnels ou différés et cas particuliers</NoticeLiee>
<NoticeLiee ID="R40371" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10399/fichedescriptiveformulaire_10399.pdf" numerocerfa="50154#20">Notice revenus 2015 : revenus des valeurs et capitaux mobiliers (RCM)</NoticeLiee>
<NoticeLiee ID="R36721" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10406/fichedescriptiveformulaire_10406.pdf" numerocerfa="50794#16">Notice revenus 2015 : Travaux dans l'habitation principale</NoticeLiee>
<NoticeLiee ID="R36677" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10400/fichedescriptiveformulaire_10400.pdf" numerocerfa="50153#20">Notice revenus 2015 : BIC non professionnels : revenus de locations meublées et autres activités BIC non professionnelles</NoticeLiee>
<NoticeLiee ID="R43226" URL="http://www.impots.gouv.fr/portal/deploiement/p1/fichedescriptiveformulaire_10398/fichedescriptiveformulaire_10398.pdf" numerocerfa="50318#19">Notice revenus 2015 : personnes fiscalement domiciliées hors de France</NoticeLiee>
</ServiceEnLigne>
<ServiceEnLigne ID="R32007" URL="http://www.impots.gouv.fr/portal/dgi/public/popup?docOid=ficheformulaire_5070&amp;typePage=ifi01" commentaireLien="Bailleurs personnes physiques" numerocerfa="14872*04" autrenumero="2042-LE" type="Formulaire">
<Titre>Taxe sur les loyers élevés des logements de petite surface</Titre>
<Source ID="R30612">Ministère en charge des finances</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R43941" URL="http://www.impots.gouv.fr/portal/dgi/public/popup?docOid=ficheformulaire_4974&amp;typePage=ifi01" commentaireLien="Bailleurs professionnels" numerocerfa="14813*03" autrenumero="2576-TSLE-SD" type="Formulaire">
<Titre>Taxe sur les loyers élevés de logements de petite surface</Titre>
<Source ID="R30612">Ministère en charge des finances</Source>
</ServiceEnLigne>
<Definition ID="R18320">
<Titre>Surface habitable des logements</Titre>
<Texte>
				<Paragraphe>Surface au sol déduction faite des murs, cloisons, marches et cages d'escaliers, gaines, embrasures de portes et de fenêtres, parties de locaux d'une hauteur inférieure à 1,8 mètre. Certains espaces sont exclus : garage,cave, terrasse, balcon...</Paragraphe>
			</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006896335&amp;cidTexte=LEGITEXT000006074096" ID="R35943">
<Titre>Code de la construction de de l'habitation : article R*111-2</Titre>
</Reference>
</Definition>
</Publication>
