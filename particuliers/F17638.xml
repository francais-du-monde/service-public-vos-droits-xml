<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F17638" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Quels sont les changements du système d'immatriculation des véhicules (SIV) ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Transports</dc:subject>
<dc:description>Un numéro SIV, de la forme "AB-123-CD" est attribué au véhicule qui va le conserver jusqu'à sa destruction, même en cas de changement de propriétaire.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de l'intérieur</dc:contributor>
<dc:date>modified 2014-05-27</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F17638</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177098&amp;cidTexte=LEGITEXT000006074228, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020237165</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N367</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<Cible>Automobiliste</Cible>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19812">Transports</Niveau>
<Niveau ID="N367">Carte grise (certificat d'immatriculation)</Niveau>
<Niveau ID="F17638" type="Fiche Question-réponse">Quels sont les changements du système d'immatriculation des véhicules (SIV) ?</Niveau>
</FilDAriane>
<Theme ID="N19812">
<Titre>Transports</Titre>
</Theme><DossierPere ID="N367">
<Titre>Carte grise (certificat d'immatriculation)</Titre><SousDossier ID="N367-1">
<Titre>Immatriculer un véhicule</Titre>
<Fiche ID="F10293">Immatriculer un véhicule neuf</Fiche>
<Fiche ID="F1050">Immatriculer un véhicule d'occasion</Fiche>
<Fiche ID="F1707">Vendre ou donner son véhicule</Fiche>
<Fiche ID="F1480">Hériter d'un véhicule</Fiche>
<Fiche ID="F1726">Obtenir un duplicata du certificat (vol, perte, détérioration)</Fiche>
<Fiche ID="F1360">Obtenir un certificat de non gage</Fiche>
<Fiche ID="F19211">Coût et exonérations</Fiche>
</SousDossier>
<SousDossier ID="N367-2">
<Titre>Titulaire du certificat et modification de sa situation</Titre>
<Fiche ID="F10477">Titulaire et cotitulaire de la carte grise</Fiche>
<Fiche ID="F12118">Changement d'adresse</Fiche>
<Fiche ID="F10613">Ajouter une autre personne sur sa carte grise</Fiche>
<Fiche ID="F31218">Retirer un nom au certificat</Fiche>
<Fiche ID="F16876">Changement du nom du titulaire</Fiche>
</SousDossier>
<SousDossier ID="N367-3">
<Titre>Modifications du véhicule</Titre>
<Fiche ID="F1754">Carte grise d'un véhicule retiré de la circulation</Fiche>
<Fiche ID="F1473">Véhicule accidenté</Fiche>
<Fiche ID="F1478">Véhicule transformé</Fiche>
<Fiche ID="F1468">Véhicule à détruire</Fiche>
</SousDossier>
<SousDossier ID="N367-4">
<Titre>Plaques d'immatriculation</Titre>
<Fiche ID="F20319">Plaques d'immatriculation</Fiche>
<Fiche ID="F18679">Vol ou usurpation de plaques d'immatriculation</Fiche>
</SousDossier>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Si vous devez faire immatriculer votre véhicule, qu'il soit neuf ou d'occasion, vous recevrez un certificat d'immatriculation (ex-carte grise) avec un numéro correspondant au système d'immatriculation des véhicules (SIV). Ce numéro SIV, de la forme <Citation>AB-123-CD</Citation>, est attribué au véhicule qui va le conserver jusqu'à sa destruction, même en cas de changement de propriétaire.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Numéro d'immatriculation SIV</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Un véhicule peut-il changer de numéro d'immatriculation ?</Paragraphe>
</Titre><Paragraphe>Un véhicule enregistré dans le SIV dispose d'un numéro à vie et ne peut donc pas être changé.</Paragraphe>
<Paragraphe>Si vous déménagez, vous devez faire les démarches de <LienInterne LienPublication="F12118" type="Fiche d'information" audience="Particuliers">changement de domicile</LienInterne> mais, si vous possédez déjà un numéro SIV, vous n'avez pas à  modifier les plaques (même en cas de changement de département).</Paragraphe>
<Paragraphe>Si votre véhicule dispose d'une ancienne immatriculation (de type <Citation>123-AB-01</Citation>) et que vous changez d'adresse, votre certificat d’immatriculation devra être basculé dans le  système SIV. Vous devrez faire changer vos plaques pour qu'elles correspondent au numéro SIV que vous aurez reçu.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Puis-je choisir mon numéro d'immatriculation ?</Paragraphe>
</Titre><Paragraphe>Non, ce numéro est attribué automatiquement. </Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Comment est déterminé le numéro ?</Paragraphe>
</Titre><Paragraphe>Le numéro d'une voiture est formé de caractères alphanumériques sur le modèle <Citation>AA-123-AA</Citation> et est attribué chronologiquement.</Paragraphe>
<Paragraphe>Le numéro d'un  cyclomoteur est constitué d'une série alphanumérique sans tiret (1 ou 2 lettres - 2 ou 3 chiffres -  1 lettre).</Paragraphe>
<Paragraphe>Certaines lettres ne sont pas utilisées pour éviter des confusions avec des chiffres. Vous ne trouverez donc pas de <Citation>I</Citation> ni de <Citation>O</Citation>.</Paragraphe>
<Paragraphe>De même, vous ne trouverez pas  de <Citation>U</Citation> pour éviter de confusions avec le <Citation>V</Citation>.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Puis-je demander librement une immatriculation avec le nouveau système ?</Paragraphe>
</Titre><Paragraphe>Non, vous ne pouvez pas volontairement demander une conversion de l'immatriculation de votre véhicule de  l'ancien au nouveau système.</Paragraphe>
<Paragraphe>La conversion ne peut se faire qu'en cas de <LienInterne LienPublication="F12118" type="Fiche d'information" audience="Particuliers">changement d'adresse</LienInterne> ou de <LienInterne LienPublication="F1707" type="Fiche d'information" audience="Particuliers">vente du véhicule</LienInterne>.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Plaque(s) d'immatriculation</Paragraphe>
</Titre><Paragraphe>La  <LienInterne LienPublication="F20319" type="Fiche d'information" audience="Particuliers">plaque d'immatriculation</LienInterne> du véhicule doit correspondre à celui du certificat d'immatriculation et correspondre à un modèle homologué.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Simplifications des démarches</Paragraphe>
</Titre><Paragraphe>Pour obtenir votre nouveau certificat d'immatriculation, que votre  véhicule soit <LienInterne LienPublication="F10293" type="Fiche d'information" audience="Particuliers">neuf</LienInterne> ou d'<LienInterne LienPublication="F1050" type="Fiche d'information" audience="Particuliers">occasion</LienInterne>, vous pouvez effectuer la démarche :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>en vous rendant directement :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>dans la préfecture ou la sous-préfecture de votre choix (vérifiez auparavant car certaines sous-préfectures ne réalisent plus cette formalité),</Paragraphe>
</Item>
<Item>
<Paragraphe>ou <LienInterne LienPublication="F20710" type="Fiche Question-réponse" audience="Particuliers">à Paris</LienInterne>, au service des cartes grises de la préfecture de police ou <LienInterne LienPublication="F20710" type="Fiche Question-réponse" audience="Particuliers">à l'antenne de police administrative pour certains arrondissements</LienInterne>,</Paragraphe>
</Item>
</Liste>
</Item>
<Item>
<Paragraphe>en  donnant <LienInterne LienPublication="F21103" type="Fiche Question-réponse" audience="Particuliers">procuration</LienInterne> à un tiers pour faire la démarche à votre place,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe> <LienInterne LienPublication="F21031" type="Fiche Question-réponse" audience="Particuliers">par correspondance</LienInterne> en transmettant votre dossier par courrier postal :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>à la préfecture ou la sous-préfecture de votre choix (vérifiez auparavant car certaines sous-préfectures ne réalisent plus cette formalité),</Paragraphe>
</Item>
<Item>
<Paragraphe>ou <LienInterne LienPublication="F20710" type="Fiche Question-réponse" audience="Particuliers">à Paris,</LienInterne> au service des cartes grises de la préfecture de police,</Paragraphe>
</Item>
</Liste>
</Item>
<Item>
<Paragraphe>ou en mandatant un <LienInterne LienPublication="F20324" type="Fiche Question-réponse" audience="Particuliers">professionnel agréé</LienInterne> qui se chargera de faire la démarche pour vous.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Vous recevez votre nouveau certificat  par courrier  sécurisé à votre domicile.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Coût du certificat d'immatriculation</Paragraphe>
</Titre><Paragraphe>Le <LienInterne LienPublication="F19211" type="Fiche d'information" audience="Particuliers">coût du certificat</LienInterne> dépend en général du type de véhicule et du département dans lequel vous habitez.</Paragraphe>

</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177098&amp;cidTexte=LEGITEXT000006074228" ID="R778">
<Titre>Code de la route : articles R322-1 à R322-14</Titre>
<Complement>Délivrance du certificat d'immatriculation</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020237165" ID="R20050">
<Titre>Arrêté du 9 février 2009 relatif aux modalités d'immatriculation des véhicules</Titre>
</Reference>
<QuestionReponse ID="F16536" audience="Particuliers">Peut-on choisir l'adresse qui figurera sur la carte grise ?</QuestionReponse>
<QuestionReponse ID="F20324" audience="Particuliers">Un professionnel automobile peut-il se charger d'une demande de carte grise ?</QuestionReponse>

</Publication>
