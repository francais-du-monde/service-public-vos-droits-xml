<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F3104" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Salarié étranger : visite médicale et remise du titre</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Étranger - Europe</dc:subject>
<dc:description>En cas d'accord sur votre autorisation de travail, la procédure que vous devez suivre diffère selon que vous résidez déjà ou pas en France. Dans tous les cas, vous devez passer une visite médicale.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-07-23</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F3104</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006189813&amp;cidTexte=LEGITEXT000006072050, http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000019108553&amp;idSectionTA=LEGISCTA000018525798&amp;cidTexte=LEGITEXT000006072050, http://www.legifrance.gouv.fr/affichTexte.do;?cidTexte=LEGITEXT000006053178&amp;dateTexte=vig</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N107</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19804">Étranger - Europe</Niveau>
<Niveau ID="N107">Travail des étrangers non européens en France</Niveau>
<Niveau ID="F3104" type="Fiche d'information">Salarié étranger : visite médicale et remise du titre</Niveau>
</FilDAriane>
<Theme ID="N19804">
<Titre>Étranger - Europe</Titre>
</Theme>
<SousThemePere ID="N20306">Étrangers en France</SousThemePere><DossierPere ID="N107">
<Titre>Travail des étrangers non européens en France</Titre>
<Fiche ID="F2728">Obligation de détenir une autorisation de travail</Fiche>
<Fiche ID="F2734">Validité des autorisations de travail</Fiche>
<Fiche ID="F2729">Dépôt de la demande d'autorisation de travail</Fiche>
<Fiche ID="F3100">Instruction de la demande et décision</Fiche>
<Fiche ID="F3104">Visite médicale et remise du titre</Fiche>
<Fiche ID="F2229">Changement de statut d'étudiant à salarié</Fiche>
<Fiche ID="F2733">Renouvellement de l'autorisation de travail</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>En cas d'accord sur votre autorisation de travail, la procédure que vous devez suivre diffère selon que vous résidez  déjà ou pas en France.
			Dans tous les cas, vous devez passer une visite médicale.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Étranger résidant hors de France</Paragraphe>
</Titre><Paragraphe>En cas d'accord sur votre autorisation de travail, la <LienInterne LienPublication="R31466" type="Acronyme">Direccte</LienInterne> transmet votre dossier  à la représentation concernée de l'<LienInterne LienPublication="R31171" type="Acronyme">Ofii</LienInterne> à l'étranger ou, s'il n'y en a pas, à la direction compétente en France de l'Ofii. </Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>En cas de représentation de l'Ofii à l'étranger</Paragraphe>
</Titre><Paragraphe>La représentation concernée de l'Ofii à l'étranger (au Cameroun, Mali, Maroc, Sénégal, en Tunisie et Turquie) vous  convoque pour passer une visite médicale avant votre entrée en France. Si vous êtes déclaré apte, l'Ofii transmet votre dossier au consulat de France concerné pour la délivrance de votre visa.  Une fois arrivé en France, vous devrez vous présenter : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit  à l'Offi si vous êtes muni d'un <LienInterne LienPublication="F39" type="Fiche d'information" audience="Particuliers">visa de long séjour valant titre de séjour</LienInterne> (VLS-TS) pour le faire valider,</Paragraphe>
</Item>
<Item>
<Paragraphe> soit  à la préfecture de votre domicile si vous êtes titulaire d'un autre visa,  pour obtenir une carte de séjour. </Paragraphe>
</Item>
</Liste>
<Paragraphe>Dans l'attente, vous pourrez travailler muni de votre contrat de travail.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Si l'Ofii n'a pas de représentation à l'étranger</Paragraphe>
</Titre><Paragraphe> Une fois entré en France, vous devrez vous présenter : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit  à l'Offi si vous êtes muni d'un <LienInterne LienPublication="F39" type="Fiche d'information" audience="Particuliers">visa de long séjour valant titre de séjour</LienInterne> (VLS-TS) pour le faire valider,</Paragraphe>
</Item>
<Item>
<Paragraphe> soit  à la préfecture de votre domicile si vous êtes titulaire d'un autre visa,  pour obtenir une carte de séjour. </Paragraphe>
</Item>
</Liste>
<Paragraphe>Dans les 3 mois de votre entrée, vous serez convoqué à l'Offi pour passer la visite médicale obligatoire. Dans l'attente de cette visite, vous pourrez commencer à travailler. Si vous êtes déclaré apte, selon votre situation : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>l'Offi validera votre VLS-TS,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou la préfecture vous délivrera une carte de séjour. </Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Étranger résidant déjà en France</Paragraphe>
</Titre><Paragraphe>Si vous résidez déjà  en France et demandez un changement de statut (par exemple de carte de visiteur à salarié) ou une autorisation provisoire de travail (par exemple si vous êtes demandeur d'asile), vous êtes aussi soumis à la visite médicale sauf exceptions. </Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Demande de changement de statut</Paragraphe>
</Titre><Paragraphe>En cas d'accord sur votre autorisation de travail, les services de la Direccte transmettent le dossier à l'Ofii. Vous y êtes  convoqué pour passer une visite médicale.</Paragraphe>
<Paragraphe>Si vous êtes déclarez apte, la préfecture vous délivrera une nouvelle carte de séjour, sur présentation notamment du certificat médical de l'Ofii.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Demande d'autorisation de travail sans changement de document de séjour</Paragraphe>
</Titre><Paragraphe>Vous recevez normalement directement votre autorisation provisoire de travail des services de la Direccte si vous êtes   :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>sous document provisoire de séjour (demandeur d'asile, parent d'un enfant mineur malade...),</Paragraphe>
</Item>
<Item>
<Paragraphe>ou titulaire d'un titre de séjour mais qui ne vous permet pas d'exercer, à titre accessoire, une activité salariée (si vous êtes commerçant par exemple).</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si votre contrat de travail dépasse 3 mois, vous êtes aussi convoqué pour passer la visite médicale à l'offi.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><OuSAdresser ID="R12971" type="Local personnalisé sur SP">
<Titre>Office français de l'immigration et de l'intégration (Ofii), réseau local</Titre>
<Complement>Pour toute information, passer la visite médicale et faire valider son VLS-TS</Complement>
<PivotLocal>ofii</PivotLocal>
<RessourceWeb URL="http://www.ofii.fr/qui_sommes-nous_46/ou_nous_trouver_23.html"/>
<Source ID="R30731">Office français de l'immigration et de l'intégration (Ofii)</Source>
</OuSAdresser>
<OuSAdresser ID="R20" type="Local personnalisé sur SP">
<Titre>Unité territoriale de la Direccte</Titre>
<Complement>Pour toute information sur l'autorisation de travail</Complement>
<PivotLocal>direccte_ut</PivotLocal>
<RessourceWeb URL="https://lannuaire.service-public.fr/recherche?whoWhat=Unit%C3%A9+territoriale+de+la+Direccte&amp;where="/>
</OuSAdresser>
<OuSAdresser ID="R2" type="Local personnalisé sur SP">
<Titre>Préfecture</Titre>
<Complement>Étranger hors Paris : pour déposer sa demande de titre de séjour</Complement>
<PivotLocal>prefecture</PivotLocal>
<RessourceWeb URL="http://www.interieur.gouv.fr/Le-ministere/Prefectures"/>
<Source ID="R30603">Ministère en charge de l'intérieur</Source>
</OuSAdresser>
<OuSAdresser ID="R14146" type="Local personnalisé sur SP">
<Titre>Préfecture de police de Paris</Titre>
<Complement>Étranger domicilié à Paris : pour déposer sa demande de titre de séjour</Complement>
<PivotLocal>paris_ppp</PivotLocal>
<RessourceWeb URL="http://www.prefecturedepolice.interieur.gouv.fr/Nous-connaitre/Nous-contacter"/>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006189813&amp;cidTexte=LEGITEXT000006072050" ID="R35062">
<Titre>Code du travail : articles L5221-5 à L5221-11</Titre>
<Complement>Visite médicale (article L5221-5)</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000019108553&amp;idSectionTA=LEGISCTA000018525798&amp;cidTexte=LEGITEXT000006072050" ID="R32683">
<Titre>Code du travail : article R5221-1 à R5221-10</Titre>
<Complement>Différentes catégories d'autorisation de travail</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do;?cidTexte=LEGITEXT000006053178&amp;dateTexte=vig" ID="R17475">
<Titre>Arrêté du 11 janvier 2006 relatif à la visite médicale des étrangers autorisés à séjourner en France</Titre>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R696" URL="http://www.ofii.fr/tests_197/la_visite_medicale_est_-_elle_obligatoire_1007.html" audience="Particuliers">
<Titre>Visite médicale des étrangers</Titre>
<Source ID="R30731">Office français de l'immigration et de l'intégration (Ofii)</Source>
</PourEnSavoirPlus>
<Abreviation ID="R31466" type="Acronyme">
<Titre>Direccte</Titre>
<Texte>
<Paragraphe>Direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi</Paragraphe>
</Texte>
</Abreviation>
<Abreviation ID="R31171" type="Acronyme">
<Titre>Ofii</Titre>
<Texte>
<Paragraphe>Office français de l'immigration et de l'intégration</Paragraphe>
</Texte>
</Abreviation>
</Publication>
