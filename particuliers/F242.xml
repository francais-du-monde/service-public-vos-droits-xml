<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F242" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Salarié en congé de paternité et d'accueil de l'enfant : indemnités journalières</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>Si vous êtes en congé paternité, vous pouvez percevoir des indemnités journalières de la sécurité sociale, sous conditions de cotisation.s.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-01-01</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F242</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006172599&amp;cidTexte=LEGITEXT000006073189, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006173387&amp;cidTexte=LEGITEXT000006073189, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006172185&amp;cidTexte=LEGITEXT000006073189, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027436189</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N510</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N510">Congés dans le secteur privé</Niveau>
<Niveau ID="F242" type="Fiche d'information">Salarié en congé de paternité et d'accueil de l'enfant : indemnités journalières</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19964">Temps de travail et congés</SousThemePere><DossierPere ID="N510">
<Titre>Congés dans le secteur privé</Titre><SousDossier ID="N510-1">
<Titre>Congés annuels</Titre>
<Fiche ID="F2258">Congés payés</Fiche>
<Fiche ID="F2262">Maladie et congés payés annuels</Fiche>
</SousDossier>
<SousDossier ID="N510-2">
<Titre>Congés liés à l'arrivée d'un enfant</Titre>
<Fiche ID="F2265">Congé de maternité</Fiche>
<Fiche ID="F207">Indemnités journalières de congé de maternité</Fiche>
<Fiche ID="F3156">Congé de paternité et d'accueil de l'enfant</Fiche>
<Fiche ID="F242">Indemnités journalières de congé de paternité et d'accueil de l'enfant</Fiche>
<Fiche ID="F2268">Congé d'adoption</Fiche>
<Fiche ID="F2266">Congé de 3 jours pour naissance ou adoption</Fiche>
</SousDossier>
<SousDossier ID="N510-3">
<Titre>Congé parental</Titre>
<Fiche ID="F2280">Congé parental - Temps plein</Fiche>
<Fiche ID="F2332">Congé parental - Temps partiel</Fiche>
</SousDossier>
<SousDossier ID="N510-4">
<Titre>Congés pour maladie, handicap ou dépendance d'un membre de la famille</Titre>
<Fiche ID="F151">Congé pour enfant malade</Fiche>
<Fiche ID="F1631">Congé de présence parentale</Fiche>
<Fiche ID="F16920">Congé de soutien familial</Fiche>
<Fiche ID="F1767">Congé de solidarité familiale</Fiche>
<Fiche ID="F706">Allocation journalière d'accompagnement d'une personne en fin de vie</Fiche>
<Fiche ID="F32112">Don de jours de repos à un salarié parent d'enfant gravement malade</Fiche>
</SousDossier>
<SousDossier ID="N510-5">
<Titre>Congés et aides en cas de création ou de reprise d'entreprise</Titre>
<Fiche ID="F2382">Congé et temps partiel</Fiche>
<Fiche ID="F2742">Levée provisoire des clauses d'exclusivité</Fiche>
</SousDossier>
<SousDossier ID="N510-6">
<Titre>Congés spécifiques</Titre>
<Fiche ID="F2278">Congés pour événement familial</Fiche>
<Fiche ID="F2381">Congé sabbatique</Fiche>
<Fiche ID="F2310">Congé pour exercer un mandat local</Fiche>
<Fiche ID="F2295">Congé d'enseignement, de recherche et d'innovation</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Congés liés à l'arrivée d'un enfant</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Pendant votre congé de paternité et d'accueil de l'enfant, le contrat de travail est suspendu et vous avez droit au versement d'indemnités journalières (IJ) versées par la sécurité sociale. Ces IJ sont versées sous conditions de cotisations. Le montant versé varie en fonction de votre salaire.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Bénéficiaires</Paragraphe>
</Titre><Paragraphe>Pour percevoir les IJ versées durant le congé de paternité et d'accueil de l'enfant, vous devez respecter les conditions suivantes.</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Prendre le congé de paternité et d'accueil de l'enfant</Paragraphe>
</Titre><Paragraphe>Vous devez demander à prendre le <LienInterne LienPublication="F3156" type="Fiche d'information" audience="Particuliers">congé de paternité et d'accueil de l'enfant</LienInterne> dans les 4 mois qui suivent la naissance de l'enfant (sauf report du délai).</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Immatriculation à la sécurité sociale</Paragraphe>
</Titre><Paragraphe>Vous devez justifier de 10 mois d'immatriculation (possession d'un numéro d'assuré social) à la date du début du congé.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Cotisation à la sécurité sociale</Paragraphe>
</Titre><Paragraphe>Vous devez :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit avoir travaillé au moins 150 heures au cours des 3 mois précédant le début du congé,</Paragraphe>
</Item>
<Item>
<Paragraphe> soit avoir cotisé sur un salaire au moins équivalent à 1 015 fois le <LienInterne LienPublication="R31127" type="Acronyme">Smic</LienInterne> horaire au cours des 6 derniers mois précédant le début de son congé de paternité.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si vous exercez une activité saisonnière ou discontinue, vous devez :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit avoir travaillé au moins 600 heures,</Paragraphe>
</Item>
<Item>
<Paragraphe>soit avoir cotisé sur un salaire au moins égal à 2 030 fois le montant du Smic horaire au cours des 12 mois précédents.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Cesser toute activité rémunérée</Paragraphe>
</Titre><Paragraphe>Vous devez cesser toute activité salariée, même si vous travaillez pour 2 employeurs différents. En cas de demande de congé chez un employeur et de poursuite de l'activité chez l'autre, la caisse primaire d'assurance maladie (CPAM) peut réclamer le remboursement de la somme versée.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Démarches à effectuer</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Si vous êtes le père de l'enfant</Paragraphe>
</Titre><Paragraphe>Pour percevoir les indemnités, vous devez adresser à votre caisse d'assurance maladie l'une des pièces suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit la copie intégrale de l'acte de naissance de l'enfant,</Paragraphe>
</Item>
<Item>
<Paragraphe>soit la copie du livret de famille mis à jour,</Paragraphe>
</Item>
<Item>
<Paragraphe>soit la copie de l'acte de reconnaissance de l'enfant (si l'enfant est mort-né),</Paragraphe>
</Item>
<Item>
<Paragraphe>soit la copie de l'acte d'enfant sans vie et un certificat médical d'accouchement d'un enfant né mort et viable (si l'enfant est mort-né).</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Si vous n'êtes pas le père de l'enfant</Paragraphe>
</Titre><Paragraphe>Le congé est également ouvert au salarié soit conjoint de la mère, soit liée à elle par un pacte civil de solidarité (Pacs) ou vivant maritalement avec elle. Si c'est votre cas, pour bénéficier des indemnités, vous devez fournir l'une des pièces suivantes attestant de la naissance de l'enfant :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit la copie intégrale de l'acte de naissance de l'enfant,</Paragraphe>
</Item>
<Item>
<Paragraphe>soit la copie de l'acte d'enfant sans vie et un certificat médical d'accouchement d'un enfant né mort et viable (si l'enfant est mort-né).</Paragraphe>
</Item>
</Liste>
<Paragraphe>Vous devez également joindre l'un des justificatifs suivants attestant de votre lien avec la mère de l'enfant :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>soit un extrait d'acte de mariage,</Paragraphe>
</Item>
<Item>
<Paragraphe>soit une copie du Pacs,</Paragraphe>
</Item>
<Item>
<Paragraphe>soit un certificat de vie commune ou de concubinage de moins d'un an ou, à défaut, une attestation sur l'honneur de vie maritale cosignée par la mère de l'enfant.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Montant</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Calcul</Paragraphe>
</Titre><Paragraphe>Pour calculer le montant des IJ, votre <LienInterne LienPublication="R15469" type="Sigle">CPAM</LienInterne> détermine un salaire journalier de base calculé en prenant en compte le total des 3 derniers salaires perçus avant la date d'interruption du travail, divisé par 91,25 (pour les salariés mensualisés). </Paragraphe>
<Paragraphe>Votre salaire est pris en compte dans la limite du montant du plafond mensuel de la sécurité sociale en vigueur lors du dernier jour du mois qui précède l'arrêt (soit <Valeur>3 218 €</Valeur> par mois en 2016).</Paragraphe>
<Paragraphe>La sécurité sociale retire à ce salaire journalier de base un taux forfaitaire de 21%.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Montant minimum</Paragraphe>
</Titre><Paragraphe>Le montant minimum des IJ de paternité est fixé à <Valeur>9,27 €</Valeur> par jour.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Montant maximum</Paragraphe>
</Titre><Paragraphe>Le montant maximum des IJ de paternité est fixé à <Valeur>83,58 €</Valeur> par jour.</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>le contrat de travail ou la convention collective applicable peut prévoir des conditions d'indemnisation plus favorables que celles de la sécurité sociale, pouvant aller jusqu'au maintien intégral du salaire.</Paragraphe>
</ANoter>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Date de début du congé (pour percevoir les indemnités)</Paragraphe>
</Titre><Paragraphe>Pour percevoir les indemnités de la CPAM, le congé doit être pris dans les 4 mois qui suivent la date de naissance de l'enfant.</Paragraphe>
<Paragraphe>Un report du délai est possible si l'enfant est hospitalisé ou si vous bénéficiez du congé postnatal de maternité à la place de la mère (en cas de décès).</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Versement</Paragraphe>
</Titre>
<Paragraphe>Les indemnités journalières sont versées tous les 14 jours.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Règles de cumul</Paragraphe>
</Titre><Paragraphe>Les indemnités journalières ne sont pas cumulables avec :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>les indemnités journalières versée pour maladie ou accident du travail,</Paragraphe>
</Item>
<Item>
<Paragraphe>les allocations de chômage ou les minima sociaux.</Paragraphe>
</Item>
</Liste>

</Chapitre>
</Texte><OuSAdresser ID="R19040" type="Centre de contact">
<Titre>Assurance maladie - 3646</Titre>
<Complement>Pour toute information sur votre indemnisation (par téléphone)</Complement><Texte>
						
								<Paragraphe>
			Le 36 46 vous permet d'obtenir des renseignements sur vos droits et démarches, de poser une question sur votre dossier, de signaler un changement de situation ou encore de consulter vos remboursements.
		</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			<MiseEnEvidence>3646</MiseEnEvidence>
</Paragraphe>
								<Paragraphe>
			Ouvert du lundi au vendredi. Attention : les horaires varient selon votre département. En règle générale, les horaires d'ouverture sont au minimum de 8h30 à 16h. </Paragraphe>
								<Paragraphe>Numéro violet ou majoré : coût d'un appel vers un numéro fixe + service payant, depuis un téléphone fixe ou mobile</Paragraphe>
								<Paragraphe>Pour connaître le tarif, écoutez le message en début d'appel</Paragraphe>
								<Paragraphe>
			Depuis l'étranger : +33 (0) 811 70 36 46
		</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par messagerie</Paragraphe>
</Titre>
								<Paragraphe>
			
      Connectez-vous sur votre
      <LienExterne URL="http://www.ameli.fr/">compte ameli</LienExterne>
,      puis sélectionnez l'onglet
      <Citation>Vos demandes</Citation>
      et cliquez sur
      <Citation>Contactez-nous / Vos questions</Citation>
		.</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R40" type="Local personnalisé sur SP">
<Titre>Caisse primaire d'assurance maladie (CPAM)</Titre>
<Complement>Pour toute information sur votre indemnisation (par messagerie ou par courrier)</Complement>
<PivotLocal>cpam</PivotLocal>
<RessourceWeb URL="http://www.ameli.fr/assures/votre-caisse/index.php"/>
<Source ID="R30675">Caisse nationale d'assurance maladie des travailleurs salariés (Cnamts)</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006172599&amp;cidTexte=LEGITEXT000006073189" ID="R11278">
<Titre>Code de la sécurité sociale : article L331-8</Titre>
<Complement>Principes généraux</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006173387&amp;cidTexte=LEGITEXT000006073189" ID="R10719">
<Titre>Code de la sécurité sociale : articles R331-5 à R331-7</Titre>
<Complement>Montant des indemnités journalières</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006172185&amp;cidTexte=LEGITEXT000006073189" ID="R11283">
<Titre>Code de la sécurité sociale : articles D331-3 à D331-4</Titre>
<Complement>Date de début du congé (pour percevoir les indemnités)</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000027436189" ID="R2545">
<Titre>Arrêté du 3 mai 2013 fixant la liste des pièces justificatives à fournir pour bénéficier de l'indemnisation du congé de paternité et d'accueil de l'enfant</Titre>
</Reference>
<ServiceEnLigne ID="R10850" URL="http://www.ameli.fr/simulateur" type="Simulateur">
<Titre>Simulateur de calcul d'indemnités journalières maternité ou paternité</Titre>
<Source ID="R30675">Caisse nationale d'assurance maladie des travailleurs salariés (Cnamts)</Source>
</ServiceEnLigne>
<Abreviation ID="R15469" type="Sigle">
<Titre>CPAM</Titre>
<Texte>
<Paragraphe>Caisse primaire d'assurance maladie</Paragraphe>
</Texte>
</Abreviation>
<Abreviation ID="R31127" type="Acronyme">
<Titre>Smic</Titre>
<Texte>
<Paragraphe>Salaire minimum interprofessionnel de croissance</Paragraphe>
</Texte>
</Abreviation>
<QuestionReponse ID="F3152" audience="Particuliers">Les indemnités pour arrêt de travail sont-elles imposées sur le revenu ?</QuestionReponse>

</Publication>
