<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F282" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Achat d'un logement social</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Logement</dc:subject>
<dc:description>Vous pouvez acheter un logement social sous certaines conditions. Les conditions de revente ou de mise en location du logement social acheté sont encadrées par la loi.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-10-13</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F282</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006188330&amp;cidTexte=LEGITEXT000006074096, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177797&amp;cidTexte=LEGITEXT000006074096, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000021627416&amp;cidTexte=LEGITEXT000006074096</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N339</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19808">Logement</Niveau>
<Niveau ID="N339">Location immobilière : fin du bail</Niveau>
<Niveau ID="F282" type="Fiche d'information">Achat d'un logement social</Niveau>
</FilDAriane>
<Theme ID="N19808">
<Titre>Logement</Titre>
</Theme>
<SousThemePere ID="N289">Location immobilière</SousThemePere><DossierPere ID="N339">
<Titre>Location immobilière : fin du bail</Titre>
<Fiche ID="F1168">Congé donné par le locataire</Fiche>
<Fiche ID="F929">Congé donné par le bailleur</Fiche>
<Fiche ID="F33671">État des lieux de sortie</Fiche>
<Fiche ID="F1239">Échange de logements vides (secteur privé et secteur social)</Fiche>
<Fiche ID="F282">Achat d'un logement social</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Vous pouvez acheter un logement social sous certaines conditions. Les conditions de revente ou de mise en location du logement social acheté sont encadrées par la loi. </Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Conditions</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Conditions relatives au logement</Paragraphe>
</Titre><Paragraphe>Les logements sociaux peuvent être vendus si ils ont été acquis ou construits depuis plus de 10 ans par un bailleur social.</Paragraphe>
<Paragraphe>Les organismes HLM peuvent également vendre des logements neufs réalisés dans le cadre du <LienInterne LienPublication="F31151" type="Fiche d'information" audience="Particuliers">dispositif Duflot</LienInterne>.</Paragraphe>
<Paragraphe>Ces logements doivent par ailleurs être conformes aux normes de décence et de performance énergétique.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Conditions relatives à l'acheteur</Paragraphe>
</Titre><Paragraphe>Lorsque le logement social est occupé, seul le locataire en place est susceptible de l'acheter.</Paragraphe>
<Paragraphe>Toutefois, si le locataire en place en fait la demande, le logement peut être vendu à :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>son époux,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>ou à ses <LienIntra LienID="R12668" type="Définition de glossaire">ascendants</LienIntra> ou <LienIntra LienID="R12574" type="Définition de glossaire">descendants,</LienIntra> s'ils respectent les plafonds de ressources exigés pour l'attribution d'un logement social.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Lorsque le logement social est vacant (pas d'occupant) il doit être  proposé en priorité :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>à l'ensemble des locataires de logements de l'organisme HLM dans le département, </Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>et aux gardiens employés par le bailleur social.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Si les acheteurs prioritaires ne sont pas intéressés, toute personne peut se porter acquéreur.</Paragraphe>
<Paragraphe>Dans tous les cas, un particulier qui achète un logement social doit le faire pour l'occuper personnellement, ou pour le louer à usage de résidence principale sous certaines conditions de loyer et de ressources des futurs locataires.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Initiative de la vente</Paragraphe>
</Titre><Paragraphe>La vente peut être initiée par : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>un organisme HLM lorsque celui-ci souhaite vendre un logement HLM,</Paragraphe>
</Item>
<Item>
<Paragraphe>un locataire, s'il formule à son bailleur une demande d'acquisition de son propre logement.</Paragraphe>
</Item>
</Liste>
<Attention>
<Titre>Attention</Titre><Paragraphe>le locataire doit adresser sa demande par lettre recommandée avec accusé de réception et le bailleur a 2 mois pour donner une réponse motivée.</Paragraphe>
</Attention>
</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Autorisations administratives</Paragraphe>
</Titre><Paragraphe>La décision de vendre le logement est toujours prise par le bailleur social qui doit obtenir l'accord des autorités administratives.</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Transmission au Préfet</Paragraphe>
</Titre><Paragraphe>Le bailleur social doit transmettre sa décision de vendre un de ses logements au Préfet qui est  tenu de consulter :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>la commune,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>et l'ensemble des acteurs publics locaux qui ont participé au financement du logement social. </Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Avis de la commune</Paragraphe>
</Titre><Paragraphe>À partir de la date de cette consultation, la commune dispose de 2 mois  pour émettre un avis. </Paragraphe>
<Paragraphe>Sans réponse au-delà de ce délai, l'accord de la commune est acquis.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Opposition du Préfet</Paragraphe>
</Titre><Paragraphe>Le Préfet dispose encore de 4 mois pour s'opposer à l'avis de la commune. </Paragraphe>
<Paragraphe>Sans opposition motivée de sa part dans le délai, la vente du logement est autorisée. </Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Mise en vente</Paragraphe>
</Titre><Paragraphe>La mise en vente s'effectue lorsque toutes les autorisations ont été données. </Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Fixation du prix</Paragraphe>
</Titre><Paragraphe>L'organisme HLM doit solliciter le service des domaines pour évaluer le prix du logement.</Paragraphe>
<Paragraphe>Le prix de vente est ensuite librement fixé par l'organisme HLM :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>sur la base du prix d'un logement comparable libre d'occupation,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>et sans pouvoir être supérieur ou inférieur de plus de 35 % à l'évaluation faite par le service des domaines.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Affichage</Paragraphe>
</Titre><Paragraphe>Lorsque l'organisme HLM décide de mettre en vente un logement vacant, il doit le faire savoir par voie d'affichage : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>à son siège social,</Paragraphe>
</Item>
<Item>
<Paragraphe>aux emplacements habituellement utilisés pour l'information des locataires dans les immeubles locatifs qui lui appartienne,</Paragraphe>
</Item>
<Item>
<Paragraphe>dans des journaux locaux diffusés dans le département,</Paragraphe>
</Item>
<Item>
<Paragraphe>et s'il s'agit d'une maison individuelle, par l'apposition sur cette maison ou à proximité immédiate d'un écriteau visible depuis la voie publique.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Cet affichage doit mentionner la superficie du logement et le prix de vente proposé.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Information de l'acheteur</Paragraphe>
</Titre><Paragraphe>Avant la vente, le bailleur doit transmettre au locataire certaines informations par écrit :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>le montant des charges locatives des 2 dernières années (et des charges de copropriété si elles existent),</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>la liste des travaux réalisés les 5 dernières années,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>la liste des travaux d'amélioration des parties et équipements communs envisagés par le bailleur social,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>une évaluation du montant global de ces travaux et de la quote-part qui sera à la charge de l'acquéreur,</Paragraphe>
</Item>
<Item>
<Paragraphe>un document précisant que l'acquéreur sera redevable de la taxe foncière sur les propriétés bâties, tous les ans, à compter de la première année suivant celle de la vente,</Paragraphe>
</Item>
<Item>
<Paragraphe>un exemplaire de l'état descriptif de division de l'immeuble et le règlement de copropriété si l'immeuble est en copropriété.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Revente ou location </Paragraphe>
</Titre><Paragraphe>Les conditions de revente ou de mise en location d'un logement social acheté dans les 5 années précédentes sont encadrées par la loi.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>au-delà de 5 ans après l'achat initial, la vente ou la mise en location du logement reste libre.</Paragraphe>
</ASavoir>
<SousChapitre>
<Titre>
<Paragraphe>Revente</Paragraphe>
</Titre><Paragraphe>Si l'acquéreur souhaite revendre son logement au cours des 5 années suivant son achat, il doit en informer l'organisme HLM qui est prioritaire pour le racheter.</Paragraphe>
<Paragraphe>Si le bailleur social ne se porte pas acquéreur et que le bien initialement acheté à un prix inférieur à l'évaluation du service des domaines est revendu à un prix supérieur, l'excédent doit être restitué au bailleur social.</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>le montant de cet excédent est limité à l'écart constaté entre l'évaluation faite par le service du domaine à l'occasion de la vente initiale et le prix de revente. </Paragraphe>
</ANoter>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Mise en location</Paragraphe>
</Titre><Paragraphe>Lorsque l'acquéreur a acquis son logement à un prix inférieur à l'évaluation faite par le service des domaines et qu'il le loue dans les 5 ans qui suivent son acquisition, il ne peut percevoir un loyer supérieur au dernier loyer qu'il acquittait pour ce logement avant cette acquisition.</Paragraphe>
<Paragraphe>S'il s'agit d'un logement acheté dans le cadre du <LienInterne LienPublication="F31151" type="Fiche d'information" audience="Particuliers">dispositif Duflot</LienInterne>, les loyers praticables sont plafonnés et le locataire doit avoir des ressources ne dépassant pas les montants fixés par la loi.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006188330&amp;cidTexte=LEGITEXT000006074096" ID="R34463">
<Titre>Code de la construction et de l'habitation : articles L443-7 à L443-15-5</Titre>
<Complement>Règles générales</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006177797&amp;cidTexte=LEGITEXT000006074096" ID="R34464">
<Titre>Code de la construction et de l'habitation : article R443-34</Titre>
<Complement>Condition d'occupation personnelle de l'acquéreur</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000021627416&amp;cidTexte=LEGITEXT000006074096" ID="R38509">
<Titre>Code de la construction et de l'habitation : articles R443-10 à R443-17</Titre>
<Complement>Mesures de publicité (article R443-12) et information sur la taxe foncière (article R443-13-1)</Complement>
</Reference>
<Definition ID="R12668">
<Titre>Ascendant</Titre>
<Texte>
				<Paragraphe>Parents, grands-parents et arrière-grands-parents d'une personne</Paragraphe>
			</Texte>
</Definition>
<Definition ID="R12574">
<Titre>Descendant</Titre>
<Texte>
				<Paragraphe>Personne qui descend directement d'une autre, soit au 1er degré (enfant), soit à un degré plus éloigné (petit-enfant, arrière-petit-enfant)</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F31126" audience="Particuliers">Le PTZ peut-il financer l'acquisition d'un logement social ?</QuestionReponse>
</Publication>
