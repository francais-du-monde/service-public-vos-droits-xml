<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F3165" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Quel est le coût d'une tutelle ou d'une curatelle ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Famille</dc:subject>
<dc:description>Une mesure de protection est gratuite si elle est assurée par l'entourage de la personne protégée. Si elle est assurée par un mandataire, une participation financière peut être demandée à la personne protégée.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2015-05-21</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F3165</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006150530&amp;cidTexte=LEGITEXT000006070721, http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020050566&amp;cidTexte=LEGITEXT000006071154, http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000024234581&amp;cidTexte=LEGITEXT000006074069, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020021088, http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023086116, https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024434299</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N155</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19805">Famille</Niveau>
<Niveau ID="N155">Protection juridique (tutelle, curatelle...)</Niveau>
<Niveau ID="F3165" type="Fiche Question-réponse">Quel est le coût d'une tutelle ou d'une curatelle ?</Niveau>
</FilDAriane>
<Theme ID="N19805">
<Titre>Famille</Titre>
</Theme>
<SousThemePere ID="N20091">Protection des personnes</SousThemePere><DossierPere ID="N155">
<Titre>Protection juridique (tutelle, curatelle...)</Titre>
<Fiche ID="F2075">Sauvegarde de justice</Fiche>
<Fiche ID="F33367">Habilitation familiale</Fiche>
<Fiche ID="F2094">Curatelle</Fiche>
<Fiche ID="F2120">Tutelle d'une personne majeure</Fiche>
<Fiche ID="F1336">Mesure d'accompagnement social personnalisé (Masp) ou judiciaire (Maj)</Fiche>
<Fiche ID="F16670">Mandat de protection future</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Une mesure de protection est gratuite si elle est assurée par l'entourage de la personne protégée. Si elle est assurée par un mandataire, une participation financière peut être demandée à la personne protégée.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Mise en place de la mesure</Paragraphe>
</Titre><Paragraphe>La procédure judiciaire de mise en place d'une <LienInterne LienPublication="F2120" type="Fiche d'information" audience="Particuliers">tutelle</LienInterne> ou d'une <LienInterne LienPublication="F2094" type="Fiche d'information" audience="Particuliers">curatelle</LienInterne> est gratuite.</Paragraphe>
<Paragraphe>Toutefois, le certificat du médecin nécessaire à l'ouverture de ces mesures de protection est à la charge du majeur à protéger. Son coût de <Valeur>160 €</Valeur> est avancé par l'État si le certificat est requis par le procureur de la République ou ordonné par le juge des tutelles.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Rémunération de la personne assurant la protection</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Mesure confiée à un membre de l'entourage</Paragraphe>
</Titre><Paragraphe>Si la mesure a été confiée à la personne avec qui le majeur protégé vit en couple, à un membre de sa famille ou à un proche, la mesure est exercée à titre gratuit. </Paragraphe>
<Paragraphe>Toutefois, le juge des tutelles ou le conseil de famille peut autoriser, selon l'importance des biens gérés ou la difficulté d'exercer la mesure, le versement d'une indemnité à la personne chargée de la protection. Il en fixe le montant. Cette indemnité est à la charge de la personne protégée.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Mesure confiée à un mandataire judiciaire à la protection des majeurs</Paragraphe>
</Titre><Paragraphe>Si la mesure a été confiée à un mandataire judiciaire à la protection des majeurs, la personne protégée doit participer au financement en fonction de ses revenus. </Paragraphe>
<Paragraphe>Les revenus pris en compte sont ceux perçus au cours de l'avant-dernière année précédant la mise en place de la mesure.</Paragraphe>
<Paragraphe>Le prélèvement s'effectue par tranche comme pour <LienInterne LienPublication="F1419" type="Fiche Question-réponse" audience="Particuliers">l'impôt sur le revenu</LienInterne>. C'est le juge des tutelles qui fixe cette participation.</Paragraphe>
<Tableau>
<Titre>Participation de la personne protégée</Titre>
<Colonne largeur="54" type="normal"/>
<Colonne largeur="18" type="normal"/>
<Colonne largeur="37" type="normal"/>
<Colonne largeur="40" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Tranche de revenu</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Pourcentage prélevée</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Montant maximum par tranche</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Montant maximum cumulé</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Jusqu'à <Valeur>9 319,08 €</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>0 %</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>0 €</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>0 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Entre <Valeur>9 319,08 €</Valeur> et <Valeur>17 162,61</Valeur> inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>7 %</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>549,05 €</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>549,05 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Entre  <Valeur>17 162,61</Valeur> et <Valeur>42 906,51 €</Valeur> inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>15 %</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>3 861,59 €</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>4 410,63</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Entre  <Valeur>42 906,51 €</Valeur> et  <Valeur>102 975,60 €</Valeur> inclus</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>2 %</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>1 201,38 €</Valeur>
</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>
<Valeur>5 612,01 €</Valeur>
</Paragraphe>
</Cellule>
</Rangée>
</Tableau>
<Paragraphe>Au-delà de cette participation, et à titre exceptionnel, le juge peut, après avis du procureur de la République, allouer au mandataire une indemnité complémentaire pour l'accomplissement d'un acte ou d'une série d'actes demandant des travaux complexes. L'indemnité est à la charge du majeur protégé.</Paragraphe>
<Paragraphe>Le préfet peut accorder, à titre exceptionnel, temporaire et non renouvelable, une exonération d'une partie ou de l'ensemble de la participation de la personne protégée, en raison de difficultés particulières liées à l'existence de dettes contractées par la personne protégée avant l'ouverture d'une mesure de protection juridique des majeurs ou à la nécessité de faire face à des dépenses impératives.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006150530&amp;cidTexte=LEGITEXT000006070721" ID="R20846">
<Titre>Code civil : articles 415 à 424</Titre>
<Complement>Indemnité du membre de l'entourage chargé de la protection (article 419)</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000020050566&amp;cidTexte=LEGITEXT000006071154" ID="R33860">
<Titre>Code de procédure pénale : article R217-1</Titre>
<Complement>Coût du certificat médical</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000024234581&amp;cidTexte=LEGITEXT000006074069" ID="R21678">
<Titre>Code de l'action sociale et des familles : article 471-5</Titre>
<Complement>Ressources prises en compte pour déterminer la participation financière de la personne protégée</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020021088" ID="R21696">
<Titre>Décret n°2008-1554 du 31 décembre 2008 sur la participation des personnes protégées au financement de leur mesure de protection</Titre>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000023086116" ID="R21697">
<Titre>Décret n°2010-1404 du 12 novembre 2010 fixant l'indemnité complémentaire allouée aux mandataires judiciaires à la protection des majeurs</Titre>
</Reference>
<Reference type="Texte de référence" URL="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000024434299" ID="R43167">
<Titre>Décret n°2011-936 du 1er août 2011 relatif à la rémunération des mandataires judiciaires</Titre>
</Reference>
</Publication>
