<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F14750" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Accès au logement privé : non discrimination des candidats à la location</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Justice</dc:subject>
<dc:description>Le bailleur a le droit de choisir librement un locataire sous réserve de ne pas établir sa sélection sur un ou plusieurs motifs discriminatoires.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-07-23</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F14750</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000028806595&amp;cidTexte=LEGITEXT000006069108, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006165298&amp;cidTexte=LEGITEXT000006070719</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N286</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19807">Justice</Niveau>
<Niveau ID="N286">Discrimination</Niveau>
<Niveau ID="F14750" type="Fiche d'information">Accès au logement privé : non discrimination des candidats à la location</Niveau>
</FilDAriane>
<Theme ID="N19807">
<Titre>Justice</Titre>
</Theme>
<SousThemePere ID="N20326">Infractions</SousThemePere><DossierPere ID="N286">
<Titre>Discrimination</Titre>
<Fiche ID="F1642">Discrimination à l'embauche dans le secteur privé</Fiche>
<Fiche ID="F14750">Discrimination à la location</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Le bailleur a le droit de choisir librement un locataire sous réserve de ne pas établir sa sélection sur un ou plusieurs motifs discriminatoires. </Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Critères de sélection autorisés</Paragraphe>
</Titre><Paragraphe>Pour sélectionner un locataire parmi les candidats, le bailleur a le droit de se fonder sur des critères objectifs ayant trait à sa solvabilité tels que :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>le revenu,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>la présence ou non d'une caution,</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>la situation professionnelle (CDI, CDD) etc ...</Paragraphe>
</Item>
</Liste>
<Attention>
<Titre>Attention</Titre><Paragraphe> pour évaluer la solvabilité du futur locataire, le bailleur n'a pas le droit de réclamer <LienInterne LienPublication="F1169" type="Fiche d'information" audience="Particuliers">certains documents</LienInterne>. </Paragraphe>
</Attention>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Critères de sélection interdits</Paragraphe>
</Titre><Paragraphe>Certains critères ne doivent pas être retenus pour choisir un locataire, car ils sont discriminatoires :</Paragraphe>

						<Liste type="puce">
							<Item>
								<Paragraphe>l'origine géographique, le nom de famille, le lieu de résidence,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>l'appartenance ou non-appartenance, réelle ou supposée, à une ethnie ou à une nation déterminée,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>le sexe, l'identité sexuelle, </Paragraphe>
							</Item>
							<Item>
								<Paragraphe>la situation de famille, la grossesse ou la maternité,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>l'orientation sexuelle, les mœurs,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>l'apparence physique,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>l'âge,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>l'état de santé, le handicap,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>les caractéristiques génétiques,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>la religion, les convictions politiques ou activités syndicales,</Paragraphe>
							</Item>
							<Item>
								<Paragraphe>la précarité de sa situation économique.</Paragraphe>
							</Item>
						</Liste>
					

<ANoter>
<Titre>À noter</Titre><Paragraphe>le candidat locataire qui estime avoir fait l'objet d'une discrimination peut exercer différents <LienInterne LienPublication="F19448" type="Fiche Question-réponse" audience="Particuliers">recours</LienInterne>, à charge pour le bailleur de démontrer que son choix n'est pas constitutif d'une discrimination. </Paragraphe>
</ANoter>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Recours</Paragraphe>
</Titre><Paragraphe>En cas de discrimination, un candidat locataire peut saisir <LienInterne LienPublication="F19448" type="Fiche Question-réponse" audience="Particuliers">le Défenseur des droits (ex-Halde) ou la justice</LienInterne>.</Paragraphe>

</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000028806595&amp;cidTexte=LEGITEXT000006069108" ID="R37787">
<Titre>Loi n°89-462 du 6 juillet 1989 relative aux rapports locatifs : article 1</Titre>
<Complement>Discrimination au logement interdite</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006165298&amp;cidTexte=LEGITEXT000006070719" ID="R15920">
<Titre>Code pénal : articles 225-1 à 225-4</Titre>
<Complement>Critères de discrimination interdits (article 225-1)</Complement>
</Reference>

</Publication>
