<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F14035" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Titularisation d'un contractuel de la FPT : recrutements réservés</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail</dc:subject>
<dc:description>Par dérogation aux modes d'accès à la fonction publique territoriale (FPT), des moyens d'accès dits “réservés” sont organisés jusqu'au 13 mars 2018 pour certains contractuels afin de les titulariser. Ces recrutements sont organisés, pour chaque cadre d'emplois, par voie de sélection professionnelle ou de recrutement sans concours pour les emplois de catégorie C.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2016-05-20</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F14035</dc:identifier>
<dc:source>http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025489865, http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000026666052, http://circulaires.legifrance.gouv.fr/pdf/2012/12/cir_36227.pdf</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N505</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N505">Carrière dans la fonction publique</Niveau>
<Niveau ID="F14035" type="Fiche d'information">Titularisation d'un contractuel de la FPT : recrutements réservés</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19962">Contrats et carrière</SousThemePere><DossierPere ID="N505">
<Titre>Carrière dans la fonction publique</Titre><SousDossier ID="N505-1">
<Titre>Déroulement de carrière</Titre>
<Fiche ID="F18933">Stage et titularisation</Fiche>
<Fiche ID="F568">Avancements</Fiche>
<Fiche ID="F17757">Promotion interne</Fiche>
</SousDossier>
<SousDossier ID="N505-2">
<Titre>Notation - Évaluation</Titre>
<Fiche ID="F11992">Dans la fonction publique d'État (FPE)</Fiche>
<Fiche ID="F566">Dans la fonction publique territoriale (FPT)</Fiche>
<Fiche ID="F31255">Dans la fonction publique hospitalière (FPH)</Fiche>
</SousDossier>
<SousDossier ID="N505-3">
<Titre>Situation des agents contractuels</Titre>
<Fiche ID="F13117">Conditions d'emploi d'un agent contractuel</Fiche>
<Fiche ID="F13852">Passage automatique en CDI</Fiche>
</SousDossier>
<SousDossier ID="N505-4">
<Titre>Titularisation d'un agent contractuel</Titre>
<Fiche ID="F13976">Recrutements réservés dans la FPE</Fiche>
<Fiche ID="F14035">Recrutements réservés dans la FPT</Fiche>
<Fiche ID="F13731">Recrutements réservés dans la FPH</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Titularisation d'un agent contractuel</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Par dérogation aux modes d'accès  à la fonction publique territoriale (FPT), des moyens d'accès dits “réservés” sont organisés jusqu'au 13 mars 2018 pour certains contractuels afin de les titulariser. Ces recrutements sont organisés, pour chaque cadre d'emplois, par voie de sélection professionnelle ou de recrutement sans concours pour les emplois de catégorie C.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Conditions à remplir par l'agent</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Nationalité</Paragraphe>
</Titre><Paragraphe>L'agent  doit être français, citoyen de l'Espace économique européen (EEE) ou suisse.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Situation au 31 mars 2013</Paragraphe>
</Titre><Paragraphe>Au 31 mars 2013, l'agent devait :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>occuper un emploi en <LienInterne LienPublication="R24389" type="Sigle">CDI</LienInterne> ou <LienInterne LienPublication="R2454" type="Sigle">CDD</LienInterne> dans la FPT à 50 % minimum (à temps complet ou  incomplet), </Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe>et être en fonction ou en congés.</Paragraphe>
</Item>
</Liste>
<ANoter>
<Titre>À noter</Titre><Paragraphe>un agent en CDD au 31 mars 2011 qui a bénéficié d'un <LienInterne LienPublication="F13852" type="Fiche d'information" audience="Particuliers">passage automatique en CDI</LienInterne> au 13 mars 2012 est concerné.</Paragraphe>
</ANoter>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Ancienneté</Paragraphe>
</Titre><Paragraphe>L'agent en <LienInterne LienPublication="R2454" type="Sigle">CDD</LienInterne> au 31 mars 2013 devait justifier d'au moins <LienInterne LienPublication="F13803" type="Fiche Question-réponse" audience="Particuliers">4 ans de services</LienInterne> :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>entre le 31 mars 2007 et le 31 mars 2013,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou à la clôture des inscriptions au recrutement  dont au moins 2 ans entre le 31 mars 2009 et le  31 mars 2013.</Paragraphe>
</Item>
</Liste>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>un agent dont le contrat a cessé entre le 1<Exposant>er</Exposant> janvier et le 31 mars 2013 peut bénéficier des accès réservés s'il remplit la condition d'ancienneté.</Paragraphe>
</ASavoir>
</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Accès aux recrutements réservés</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Objet</Paragraphe>
</Titre><Paragraphe> Des recrutements réservés sont organisés jusqu'au 13 mars 2018 par  :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe> sélections professionnelles,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou  recrutements directs au 1<Exposant>er</Exposant> grade des cadres d'emplois de catégorie C.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Ces recrutements prennent en compte les acquis de l'expérience professionnelle.</Paragraphe>
<Paragraphe>Aucun diplôme n'est  exigé sauf pour l'accès aux cadres d'emplois correspondant à une profession réglementée (infirmier territorial, par exemple).</Paragraphe>
<Paragraphe>Un agent ne peut se présenter qu'à un seul recrutement réservé par cadre d'emplois au cours d'une année civile.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Auprès de quelle collectivité</Paragraphe>
</Titre><Tableau>
<Titre>Collectivité  auprès de laquelle l'agent peut se présenter en fonction de sa situation</Titre>
<Colonne largeur="41" type="normal"/>
<Colonne largeur="74" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Situation du contractuel</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>CT auprès de laquelle l'agent peut se présenter</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Agent en CDI au 31 mars 2013</Paragraphe>
</Cellule>
<Cellule>
<Liste type="puce">
<Item>
<Paragraphe>Collectivité dont il relève à la  clôture des inscriptions au recrutement auquel il postule,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou s'il n'a plus d'emploi à cette date, collectivité dont il relevait au 31 mars 2013</Paragraphe>
</Item>
</Liste>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Agent Cdisé automatiquement</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Collectivité dont il relevait au 13 mars 2012</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Agent en CDD au 31 mars 2013</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Collectivité dont il relevait au 31 mars 2013,</Paragraphe>
<Paragraphe>(ou collectivité dont il relève après le 31 mars 2013  après un transfert à une autre   collectivité en cas de transfert de compétence)</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Agent dont le contrat a cessé au 1<Exposant>er</Exposant> trimestre 2013</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Collectivité dont il relevait au terme de son contrat</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>Agent en <LienInterne LienPublication="F539" type="Fiche Question-réponse" audience="Particuliers">congé de mobilité</LienInterne> au 31 mars 2013</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Collectivité dont il relevait au 31 mars 2013</Paragraphe>
</Cellule>
</Rangée>
</Tableau>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Dans quels cadres d'emplois ?</Paragraphe>
</Titre><Paragraphe>Un agent en <LienInterne LienPublication="R2454" type="Sigle">CDD</LienInterne> au 31 mars 2013 peut prétendre à l'accès aux cadres d'emplois de catégorie A, B, C équivalente à celle dont relèvent les fonctions qu'il a exercé pendant 4 ans.</Paragraphe>
<Paragraphe>S'il a une ancienneté supérieure à 4 ans, les fonctions retenues sont celles qu'il a exercées pendant 4 ans et qui relèvent de la catégorie la plus élevée .</Paragraphe>
<Paragraphe>Si son ancienneté a été acquise dans différentes catégories, l'agent peut accéder aux cadres d'emplois relevant de la catégorie dans laquelle il a exercé  le plus longtemps pendant  4 ans.</Paragraphe>
<Paragraphe>Un agent en <LienInterne LienPublication="R24389" type="Sigle">CDI</LienInterne> au 31 mars 2013 peut accéder aux cadres d'emplois de catégorie hiérarchique équivalente à celle dont relevaient les fonctions qu'il exerçait au 31 mars 2013.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Sélection par la Commission d'évaluation professionnelle</Paragraphe>
</Titre><Paragraphe>Une commission d'évaluation professionnelle est chargée de la sélection professionnelle.</Paragraphe>
<Paragraphe>Elle fait passer un entretien à chaque candidat à partir d'un dossier remis  au moment de son inscription et ayant pour point de départ un exposé des acquis de son expérience professionnelle.</Paragraphe>
<Paragraphe>Ce dossier doit comporter :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>une lettre de candidature,</Paragraphe>
</Item>
<Item>
<Paragraphe>un curriculum vitae,</Paragraphe>
</Item>
<Item>
<Paragraphe>tout élément complémentaire permettant d'apprécier le parcours professionnel du candidat (par exemple, attestations de stage).</Paragraphe>
</Item>
</Liste>
<Paragraphe>La durée de l'audition dépend de la catégorie de l'agent :</Paragraphe>
<Tableau>
<Titre>Durée en fonction de la catégorie de l'emploi</Titre>
<Colonne largeur="11" type="normal"/>
<Colonne largeur="22" type="normal"/>
<Colonne largeur="28" type="normal"/>
<Rangée type="header">
<Cellule>
<Paragraphe>Catégorie</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Durée de l'exposé</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>Durée de l'audition</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>A</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>10 minutes maximum</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>30 minutes</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>B</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>5 minutes maximum</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>20 minutes</Paragraphe>
</Cellule>
</Rangée>
<Rangée type="normal">
<Cellule>
<Paragraphe>C</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>5 minutes maximum</Paragraphe>
</Cellule>
<Cellule>
<Paragraphe>20 minutes</Paragraphe>
</Cellule>
</Rangée>
</Tableau>
<Paragraphe>Après les  auditions, la commission dresse la liste des agents aptes à être intégrés dans le cadre d'emplois concerné. Cette liste est affichée dans les locaux de la collectivité ou du centre de gestion et publiée, lorsqu'il existe, sur son site internet.</Paragraphe>
<Paragraphe>L'autorité territoriale procède à la nomination en qualité de fonctionnaire stagiaire des agents déclarés aptes.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Nomination et classement de l'agent recruté</Paragraphe>
</Titre><Paragraphe>Un  agent recruté par la voie d'un recrutement réservé est nommé stagiaire au plus tard le 31 décembre de l'année au titre de laquelle le recrutement a été fait. Le stage est de 6 mois. Pendant le stage, l'agent est placé, au titre de son contrat, en congé non rémunéré.</Paragraphe>
<Paragraphe>Il est classé, en qualité de fonctionnaire stagiaire, à un échelon  prenant en compte une partie de l'ancienneté de services publics.</Paragraphe>
<Paragraphe>L'agent titularisé doit suivre la <LienInterne LienPublication="F18460" type="Fiche d'information" audience="Particuliers">formation de professionnalisation</LienInterne>.</Paragraphe>
<Paragraphe> Les services publics accomplis en qualité de contractuel dans un emploi de même niveau sont considérés comme des services effectifs accomplis dans le cadre d'emplois d'accueil et le grade d'intégration pour l'avancement de grade.</Paragraphe>

</Chapitre>
</Texte><Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000025489865" ID="R2441">
<Titre>Loi n°2012-347 du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique</Titre>
<Complement>Articles 13 à 18</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000026666052" ID="R31026">
<Titre>Décret n°2012-1293 du 22 novembre 2012 relatif à l'accès à l'emploi titulaire des agents contractuels territoriaux</Titre>
<Complement>Articles 1 à 6</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://circulaires.legifrance.gouv.fr/pdf/2012/12/cir_36227.pdf" ID="R31286" format="application/pdf" poids="2.0 MB">
<Titre>Circulaire du 12 décembre 2012 relative à la titularisation des contractuels de la fonction publique territoriale (FPT)</Titre>
</Reference>
<Abreviation ID="R2454" type="Sigle">
<Titre>CDD</Titre>
<Texte>
<Paragraphe>Contrat à durée déterminée</Paragraphe>
</Texte>
</Abreviation>
<Abreviation ID="R24389" type="Sigle">
<Titre>CDI</Titre>
<Texte>
<Paragraphe>Contrat de travail à durée indéterminée</Paragraphe>
</Texte>
</Abreviation>
<QuestionReponse ID="F13803" audience="Particuliers">Accès réservés à la fonction publique : comment calculer l'ancienneté de 4 ans ?</QuestionReponse>
<QuestionReponse ID="F12344" audience="Particuliers">Catégorie, corps, cadre d'emploi, grade et échelon : quelles différences ?</QuestionReponse>
<QuestionReponse ID="F12413" audience="Particuliers">Quelles sont les positions administratives dans la fonction publique ?</QuestionReponse>
<InformationComplementaire ID="R1030" date="2016-06-30">
<Titre>Pays de l'Espace économique européen (EEE)</Titre>
<Texte>
				<Paragraphe>Allemagne,
			Autriche, Belgique,
			Bulgarie,
			Chypre, Croatie, Danemark,
			Espagne,
			Estonie, Finlande, France,
			Grèce,
			Hongrie,
			Irlande, Islande,
			Italie, Lettonie, Liechtenstein,
			Lituanie,
			Luxembourg,
			Malte,
			Norvège,
			Pays-Bas,
			Pologne,
			Portugal,
			République tchèque,
			Roumanie,
			Royaume-Uni,
			Slovaquie, Slovénie,
			Suède</Paragraphe>
			</Texte>
</InformationComplementaire>
</Publication>
