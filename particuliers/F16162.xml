<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F16162" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Visa de long séjour pour la France</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Étranger - Europe</dc:subject>
<dc:description>Pour pouvoir vivre en France, vous devez obligatoirement détenir un visa de long séjour. Ce visa est accordé par les autorités consulaires françaises. Il est valable uniquement pour la France et vous autorise à séjourner pour plus de 3 mois. Il est délivré le plus souvent pour les études, le travail ou des raisons familiales. Plusieurs types de visas de long séjour existent.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2015-06-26</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F16162</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006163221&amp;cidTexte=LEGITEXT000006070158, http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006335030&amp;idSectionTA=LEGISCTA000006163226&amp;cidTexte=LEGITEXT000006070158, https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006180202&amp;cidTexte=LEGITEXT000006070158, http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006180219&amp;cidTexte=LEGITEXT000006070158</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N105</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19804">Étranger - Europe</Niveau>
<Niveau ID="N105">Entrée en France des étrangers</Niveau>
<Niveau ID="F16162" type="Fiche d'information">Visa de long séjour pour la France</Niveau>
</FilDAriane>
<Theme ID="N19804">
<Titre>Étranger - Europe</Titre>
</Theme>
<SousThemePere ID="N20306">Étrangers en France</SousThemePere><DossierPere ID="N105">
<Titre>Entrée en France des étrangers</Titre><SousDossier ID="N105-1">
<Titre>Conditions pour entrer en France</Titre>
<Fiche ID="F2672">Documents nécessaires</Fiche>
<Fiche ID="F16146">Visas de court séjour et de transit Schengen</Fiche>
<Fiche ID="F16162">Visas de long séjour pour la France</Fiche>
<Fiche ID="F16163">Formalités d'obtention des visas de court et de long séjour</Fiche>
<Fiche ID="F2230">Refus et abrogation de visa</Fiche>
<Fiche ID="F2191">Attestation d'accueil</Fiche>
</SousDossier>
<SousDossier ID="N105-2">
<Titre>Refus d'entrée - Zone d'attente</Titre>
<Fiche ID="F2190">Refus d'entrée en France</Fiche>
<Fiche ID="F11144">Zone d'attente</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Conditions pour entrer en France</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Pour pouvoir vivre en France, vous devez obligatoirement détenir un visa de long séjour. Ce visa est accordé par les autorités consulaires françaises.  Il est valable uniquement pour la France et vous autorise à séjourner pour plus de 3 mois. Il est délivré le plus souvent pour les études, le travail ou des raisons familiales. Plusieurs types de visas de long séjour existent.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Étrangers concernés</Paragraphe>
</Titre><Paragraphe>Si vous êtes étranger, vous devez posséder un visa de long séjour.</Paragraphe>
<Paragraphe>Vous en êtes toutefois dispensé si vous êtes ressortissant :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>
<LienInterne LienPublication="N123" type="Dossier" audience="Particuliers">européen ou suisse ou  membre non européen de  sa famille</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>andorran,</Paragraphe>
</Item>
<Item>
<Paragraphe>monégasque,</Paragraphe>
</Item>
<Item>
<Paragraphe> du Saint-Siège,</Paragraphe>
</Item>
<Item>
<Paragraphe>  de Saint-Marin.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Visa valant titre de séjour délivré pour motif familial, études, travail, stage</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Nature du visa</Paragraphe>
</Titre><Paragraphe>Ce visa, dit VLS-TS, est particulier. Il vaut titre de séjour  et vous dispense de démarches auprès de la préfecture pendant sa durée de validité. </Paragraphe>
<Paragraphe>Vous <LienInterne LienPublication="F39" type="Fiche d'information" audience="Particuliers">devez toutefois accomplir obligatoirement certaines formalités</LienInterne> à la direction territoriale compétente de l'<LienInterne LienPublication="R31171" type="Acronyme">Ofii</LienInterne> dans les 3 mois de votre arrivée en France.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Bénéficiaires</Paragraphe>
</Titre><Paragraphe>Vous pouvez en bénéficier si vous êtes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>époux de Français,</Paragraphe>
</Item>
<Item>
<Paragraphe>époux <LienInterne LienPublication="F11170" type="Fiche d'information" audience="Particuliers">bénéficiaire d'un regroupement familial</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>étudiant,</Paragraphe>
</Item>
<Item>
<Paragraphe>stagiaire,</Paragraphe>
</Item>
<Item>
<Paragraphe>scientifique-chercheur,</Paragraphe>
</Item>
<Item>
<Paragraphe>salarié (titulaire d'un contrat de travail d'au moins 1 an),</Paragraphe>
</Item>
<Item>
<Paragraphe>travailleur temporaire (titulaire d'un contrat de travail de moins d'1 an),</Paragraphe>
</Item>
<Item>
<Paragraphe>visiteur (vous devez pouvoir vivre de vos seules ressources en France et   vous engager à ne pas y travailler).</Paragraphe>
</Item>
</Liste>
<Attention>
<Titre>Attention</Titre><Paragraphe>si vous êtes Algérien, ce visa ne vous concerne pas.</Paragraphe>
</Attention>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Cas particulier du visa de l'époux de Français</Paragraphe>
</Titre><Paragraphe>Si vous êtes marié avec un Français, le visa de long séjour ne peut vous être refusé que pour un des 4 motifs suivants :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>votre mariage est frauduleux,</Paragraphe>
</Item>
<Item>
<Paragraphe>votre mariage a été annulé,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous représentez une menace à l'ordre public,</Paragraphe>
</Item>
<Item>
<Paragraphe>vous ne produisez pas         l'attestation prouvant que vous avez  préparé, dans votre pays de résidence, <LienInterne LienPublication="F20542" type="Fiche Question-réponse" audience="Particuliers">votre intégration dans la société française</LienInterne>.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Votre demande de visa doit être examinée dans les meilleurs délais.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>si vous êtes entré en France démuni du VLS-TS, vous pouvez néanmoins recevoir une <LienInterne LienPublication="F2209" type="Fiche d'information" audience="Particuliers">carte de séjour vie privée et familiale, sous certaines conditions</LienInterne>. </Paragraphe>
</ASavoir>
</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Visa nécessitant une demande de carte pour motif familial, travail, retraite</Paragraphe>
</Titre><Paragraphe>Ce visa porte la mention <Expression>carte de séjour à solliciter dans les 2 mois suivant l'arrivée</Expression>. Il vous permet d'entrer en France et d'obtenir une carte de séjour en préfecture.</Paragraphe>
<Paragraphe>Il vous est remis en vue de bénéficier d'une carte de séjour (annuelle, pluriannuelle ou de 10 ans selon votre situation), notamment en qualité :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>de famille de Français (enfant âgé de 16 à 21 ans ou à charge d'un Français, ascendant à charge d'un Français et de son époux),</Paragraphe>
</Item>
<Item>
<Paragraphe>de profession libérale ou indépendante (commerçant, artisan...),</Paragraphe>
</Item>
<Item>
<Paragraphe>de travailleur (salarié en mission, carte bleue européenne, saisonnier, compétences et talents) ou  famille de travailleur,</Paragraphe>
</Item>
<Item>
<Paragraphe>de retraité ou conjoint de retraité,</Paragraphe>
</Item>
<Item>
<Paragraphe>d'artiste.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Visa de long séjour spécial (étudiants, artistes, visiteurs, jeunes)</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Visa de long séjour temporaire pour certains étudiants, artistes, visiteurs</Paragraphe>
</Titre><Paragraphe>Ce visa a une durée comprise entre 4 et 6 mois maximum. Il  vaut autorisation temporaire de séjourner en France. Il peut vous être délivré si vous venez en France :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>pour suivre un enseignement court,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou pour exercer une activité artistique,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou comme visiteur (vous devez pouvoir vivre de vos seules ressources).</Paragraphe>
</Item>
</Liste>
<Paragraphe>Durant la  validité de votre visa, vous êtes dispensé de  demander une carte de séjour en préfecture. À son expiration, vous devez regagner votre pays d'origine.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Visa pour mineur scolarisé ou étudiant mineur</Paragraphe>
</Titre><Paragraphe>Vous pouvez  bénéficier de ce visa dans les conditions suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>vous devez être mineur (moins de 18 ans),</Paragraphe>
</Item>
<Item>
<Paragraphe>vous devez suivre votre scolarité ou vos études en France pour plus de 3 mois,</Paragraphe>
</Item>
<Item>
<Paragraphe>vos parents doivent résider à l'étranger.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Ce visa, qui est à entrées multiples, a une durée de 11 mois maximum.</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>en tant que mineur, vous n'avez pas à détenir de carte de séjour.</Paragraphe>
</ANoter>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Visa vacances-travail</Paragraphe>
</Titre><Paragraphe>Ce visa s'adresse aux jeunes (de 18 à 30 ans) de quelques nationalités. Il  peut seulement vous être délivré <LienExterne URL="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation-22367/emploi/article/programme-vacances-travail-117914">si votre pays est lié avec la France par un accord bilatéral "vacances-travail"</LienExterne>

.</Paragraphe>
<Paragraphe>Ce visa a une durée maximum de 12 mois et vous dispense de demander une carte de séjour. </Paragraphe>
<Paragraphe>Vous n'avez pas non plus à obtenir une autorisation de travail sauf si vous êtes : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>Australien, </Paragraphe>
</Item>
<Item>
<Paragraphe>Japonais, </Paragraphe>
</Item>
<Item>
<Paragraphe>Néo-zélandais, </Paragraphe>
</Item>
<Item>
<Paragraphe>ou Russe. </Paragraphe>
</Item>
</Liste>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Demande de visa</Paragraphe>
</Titre><Paragraphe>Vous devez déposer votre <LienInterne LienPublication="F16163" type="Fiche d'information" audience="Particuliers">demande de visa dans votre pays de résidence</LienInterne>.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R2750" type="Local personnalisable">
<Titre>Ambassade ou consulat français à l'étranger</Titre>
<Complement>Pour toute question sur les visas et déposer votre demande</Complement>
<PivotLocal>ambassade_france_etranger</PivotLocal>
<RessourceWeb URL="http://www.diplomatie.gouv.fr/fr/le-ministere-et-son-reseau/annuaires-et-adresses-du-maedi/ambassades-et-consulats-francais-a-l-etranger/"/>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006163221&amp;cidTexte=LEGITEXT000006070158" ID="R33037">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles L211-2 à L211-2-2</Titre>
<Complement>Visa délivré à l'époux de Français (article L211-2-1)</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006335030&amp;idSectionTA=LEGISCTA000006163226&amp;cidTexte=LEGITEXT000006070158" ID="R34654">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles L311-1 à L311-8</Titre>
<Complement>Obligation de présenter, sauf exceptions, un visa de long séjour pour obtenir une carte de séjour temporaire (article L311-7)</Complement>
</Reference>
<Reference type="Texte de référence" URL="https://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006180202&amp;cidTexte=LEGITEXT000006070158" ID="R35845">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles L314-11 à L314-12</Titre>
<Complement>Visa de long séjour obligatoire pour la délivrance de la carte de résident à l'enfant et l'ascendant de Français (article L314-11)</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do;?idSectionTA=LEGISCTA000006180219&amp;cidTexte=LEGITEXT000006070158" ID="R34657">
<Titre>Code de l'entrée et du séjour des étrangers et du droit d'asile : articles R311-1 à R311-3</Titre>
<Complement>Visa de long séjour valant titre de séjour et visa de long séjour temporaire (article R311-3)</Complement>
</Reference>
<PourEnSavoirPlus type="Information pratique" ID="R34061" URL="http://www.diplomatie.gouv.fr/fr/services-aux-citoyens/preparer-son-expatriation-22367/emploi/article/programme-vacances-travail-117914" audience="Particuliers">
<Titre>Visa vacances-travail</Titre>
<Source ID="R30604">Ministère en charge des affaires étrangères</Source>
</PourEnSavoirPlus>
<Abreviation ID="R31171" type="Acronyme">
<Titre>Ofii</Titre>
<Texte>
<Paragraphe>Office français de l'immigration et de l'intégration</Paragraphe>
</Texte>
</Abreviation>
<QuestionReponse ID="F20542" audience="Particuliers">L'étranger doit-il préparer son intégration avant d'entrer en France ?</QuestionReponse>
</Publication>
