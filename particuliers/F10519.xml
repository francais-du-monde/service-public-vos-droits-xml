<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F10519" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Un étranger qui s'installe en France doit-il y faire immatriculer son véhicule ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Transports</dc:subject>
<dc:description>Un étranger qui s'installe en France doit-il y faire immatriculer son véhicule ?</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-11-13</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F10519</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020237165</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N367</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19812">Transports</Niveau>
<Niveau ID="N367">Carte grise (certificat d'immatriculation)</Niveau>
<Niveau ID="F10519" type="Fiche Question-réponse">Un étranger qui s'installe en France doit-il y faire immatriculer son véhicule ?</Niveau>
</FilDAriane>
<Theme ID="N19812">
<Titre>Transports</Titre>
</Theme><DossierPere ID="N367">
<Titre>Carte grise (certificat d'immatriculation)</Titre><SousDossier ID="N367-1">
<Titre>Immatriculer un véhicule</Titre>
<Fiche ID="F10293">Immatriculer un véhicule neuf</Fiche>
<Fiche ID="F1050">Immatriculer un véhicule d'occasion</Fiche>
<Fiche ID="F1707">Vendre ou donner son véhicule</Fiche>
<Fiche ID="F1480">Hériter d'un véhicule</Fiche>
<Fiche ID="F1726">Obtenir un duplicata du certificat (vol, perte, détérioration)</Fiche>
<Fiche ID="F1360">Obtenir un certificat de non gage</Fiche>
<Fiche ID="F19211">Coût et exonérations</Fiche>
</SousDossier>
<SousDossier ID="N367-2">
<Titre>Titulaire du certificat et modification de sa situation</Titre>
<Fiche ID="F10477">Titulaire et cotitulaire de la carte grise</Fiche>
<Fiche ID="F12118">Changement d'adresse</Fiche>
<Fiche ID="F10613">Ajouter une autre personne sur sa carte grise</Fiche>
<Fiche ID="F31218">Retirer un nom au certificat</Fiche>
<Fiche ID="F16876">Changement du nom du titulaire</Fiche>
</SousDossier>
<SousDossier ID="N367-3">
<Titre>Modifications du véhicule</Titre>
<Fiche ID="F1754">Carte grise d'un véhicule retiré de la circulation</Fiche>
<Fiche ID="F1473">Véhicule accidenté</Fiche>
<Fiche ID="F1478">Véhicule transformé</Fiche>
<Fiche ID="F1468">Véhicule à détruire</Fiche>
</SousDossier>
<SousDossier ID="N367-4">
<Titre>Plaques d'immatriculation</Titre>
<Fiche ID="F20319">Plaques d'immatriculation</Fiche>
<Fiche ID="F18679">Vol ou usurpation de plaques d'immatriculation</Fiche>
</SousDossier>
</DossierPere>

<Texte><Paragraphe>Oui. L'étranger qui déclare que sa <LienIntra LienID="R1064" type="Définition de glossaire">résidence principale</LienIntra> est en France doit y faire immatriculer son véhicule dans le délai d'un mois. L'adresse figurant sur son certificat sera celle de son domicile en France.</Paragraphe>

<Chapitre>
<Titre>
<Paragraphe>Démarche</Paragraphe>
</Titre><Liste type="puce">
<Item>
<Paragraphe>Vous pouvez mandater un <LienInterne LienPublication="F20324" type="Fiche Question-réponse" audience="Particuliers">professionnel agréé</LienInterne> qui fera la démarche pour vous.</Paragraphe>
</Item>
<Item>
<Paragraphe>Vous pouvez généralement faire la démarche  à la  <LienIntra LienID="R2" type="Local personnalisé sur SP">préfecture</LienIntra> (ou la sous-préfecture) de votre choix, pas nécessairement dans votre département, soit vous-même, soit en  donnant <LienInterne LienPublication="F21103" type="Fiche Question-réponse" audience="Particuliers">procuration</LienInterne>   à un proche.</Paragraphe>
<Paragraphe>
<MiseEnEvidence>Vérifiez auparavant</MiseEnEvidence>  sur son   <LienIntra LienID="R2" type="Local personnalisé sur SP">site internet ou auprès de son standard</LienIntra> comment la démarche peut être accomplie. En effet, elle peut parfois être effectuée uniquement sur place ou simplement  <LienInterne LienPublication="F21031" type="Fiche Question-réponse" audience="Particuliers">par courrier</LienInterne> et  certaines sous-préfectures ne s'occupent plus  de l'immatriculation des véhicules.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Pièces à produire</Paragraphe>
</Titre><Paragraphe>Pour immatriculer votre véhicule, vous devrez fournir les documents suivants :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>
<LienInterne LienPublication="F31853" type="Fiche Question-réponse" audience="Particuliers">Justificatif d'identité</LienInterne>  (1 par <LienInterne LienPublication="F10477" type="Fiche d'information" audience="Particuliers">cotitulaire</LienInterne>) ou copie si la démarche est effectuée par correspondance</Paragraphe>
</Item>
<Item>
<Paragraphe>
<LienInterne LienPublication="F1028" type="Fiche Question-réponse" audience="Particuliers">Justificatif de domicile</LienInterne>  (s'il y a plusieurs titulaires, le justificatif du domicile du propriétaire dont l'adresse va figurer sur le certificat d'immatriculation), ou copie si la démarche est effectuée par correspondance</Paragraphe>
</Item>
<Item>
<Paragraphe>Procuration si quelqu'un fait la démarche pour vous ou si un des cotitulaires la fait pour l'ensemble des cotitulaires</Paragraphe>
</Item>
<Item>
<Paragraphe>
<LienInterne LienPublication="F19211" type="Fiche d'information" audience="Particuliers">Montant</LienInterne> du coût du certificat d'immatriculation : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>en chèque  (vérifiez l'ordre auquel l'établir, généralement indiqué sur le site internet de la préfecture),</Paragraphe>
</Item>
<Item>
<Paragraphe>ou, dans le cas d'une démarche sur place, parfois également en espèces ou par carte bancaire, sous réserve d'un certain montant (se renseigner préalablement)</Paragraphe>
</Item>
</Liste>
</Item>
<Item>
<Paragraphe>Ancien certificat d'immatriculation complet</Paragraphe>
</Item>
</Liste>
<Liste type="puce">
<Item>
<Paragraphe> Si le véhicule a plus de 4 ans et <LienInterne LienPublication="F2880" type="Fiche Question-réponse" audience="Particuliers">n'est pas dispensé du contrôle technique</LienInterne>,le justificatif de ce <LienInterne LienPublication="F2878" type="Fiche d'information" audience="Particuliers">contrôle</LienInterne> fait en France  (ou dans l'Union européenne si le véhicule y était précédemment immatriculé) depuis moins de 6 mois (2 mois si une <LienIntra LienID="R12140" type="Définition de glossaire">contre-visite</LienIntra> a été prescrite)</Paragraphe>
</Item>
<Item>
<Paragraphe>Formulaire <LienInterne LienPublication="R13567" type="Formulaire" audience="Particuliers">cerfa n°13750*05</LienInterne> de demande de certificat,</Paragraphe>
</Item>
<Item>
<Paragraphe>Justificatif fiscal :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>si le véhicule était immatriculé dans l'Union européenne : <LienInterne LienPublication="F179" type="Fiche Question-réponse" audience="Particuliers">quitus fiscal</LienInterne> délivré par la recette principale des impôts attestant que la TVA a bien été payée en France,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou si le véhicule était immatriculé hors Union européenne : certificat de dédouanement 846A délivré par le service des douanes</Paragraphe>
<Paragraphe>Si vous n'avez pas un de ces documents, contactez ces services pour connaître les pièces nécessaires et les modalités pour l'obtenir</Paragraphe>
</Item>
</Liste>
</Item>
<Item>
<Paragraphe>Justificatifs techniques de conformité : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>un  <Expression>certificat de conformité à un type CE</Expression>,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou <Expression>attestation d'identification à un type CE</Expression>, </Paragraphe>
</Item>
<Item>
<Paragraphe>ou si le véhicule était auparavant immatriculé dans un pays de l'Union européenne et que son <LienIntra LienID="R15487" type="Définition de glossaire">PTAC</LienIntra>  est inférieur ou égal à 3,5 tonnes, votre certificat d'immatriculation à condition qu'il comporte toutes les informations nécessaires à son immatriculation.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Le certificat de conformité et l'attestation sont délivrés par le constructeur lors de la 1<Exposant>ère</Exposant> vente du véhicule. Si vous ne les avez pas, vous devez les réclamer au constructeur ou à son représentant en France. Si ce constructeur n'existe plus, vous pouvez demander une <Expression>attestation d'identification</Expression> à la Dreal/Driee/Deal compétente. Contactez-la pour connaître les documents à fournir et prendre rendez-vous pour faire examiner votre véhicule.</Paragraphe>
</Item>
</Liste>
<ANoter>
<Titre>À noter</Titre><Paragraphe>si vous réalisez la démarche à un guichet, prévoyez, en plus de vos originaux, des photocopies de vos justificatifs d'identité et de domicile et éventuellement de votre contrôle technique.</Paragraphe>
</ANoter>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Coût</Paragraphe>
</Titre><Paragraphe>Le <LienInterne LienPublication="F19211" type="Fiche d'information" audience="Particuliers">coût de la démarche</LienInterne> est variable, notamment selon la puissance du véhicule et la région où vous vous installez.</Paragraphe>

</Chapitre>
</Texte><OuSAdresser ID="R2" type="Local personnalisé sur SP">
<Titre>Préfecture</Titre>
<Complement>Pour connaître les moyens acceptés pour réaliser la démarche et l'accomplir (sauf à Paris)</Complement>
<PivotLocal>prefecture</PivotLocal>
<RessourceWeb URL="http://www.interieur.gouv.fr/Le-ministere/Prefectures"/>
<Source ID="R30603">Ministère en charge de l'intérieur</Source>
</OuSAdresser>
<OuSAdresser ID="R14145" type="Local personnalisé sur SP">
<Titre>Sous-préfecture</Titre>
<Complement>Pour connaître les moyens acceptés pour réaliser la démarche et l'accomplir (sauf à Paris)</Complement>
<PivotLocal>sous_pref</PivotLocal>
<RessourceWeb URL="http://www.interieur.gouv.fr/Le-ministere/Prefectures"/>
<Source ID="R30603">Ministère en charge de l'intérieur</Source>
</OuSAdresser>
<OuSAdresser ID="R22425" type="Local personnalisé sur SP">
<Titre>Préfecture de police de Paris - Bureau des cartes grises</Titre>
<Complement>Pour connaître les moyens acceptés pour réaliser la démarche et l'accomplir à Paris</Complement>
<PivotLocal>paris_ppp_certificat_immatriculation</PivotLocal>
<RessourceWeb URL="http://www.prefecturedepolice.interieur.gouv.fr/Demarches/Particulier/Permis-de-conduire-et-papiers-du-vehicule/Certificat-immatriculation/Contacts/Nous-contacter-Certificat-d-immatriculation"/>
</OuSAdresser>
<OuSAdresser ID="R19482" type="Local personnalisé sur SP">
<Titre>Direction régionale des douanes et droits indirects (DRDDI)</Titre>
<Complement>Pour obtenir un certificat de dédouanement</Complement>
<PivotLocal>drddi</PivotLocal>
</OuSAdresser>
<OuSAdresser ID="R22" type="Local personnalisé sur SP">
<Titre>Direction régionale de l'environnement, de l'aménagement et du logement (Dreal)</Titre>
<Complement>Pour obtenir un procès-verbal de réception à titre isolé</Complement>
<PivotLocal>dreal</PivotLocal>
<RessourceWeb URL="https://lannuaire.service-public.fr/recherche?whoWhat=Direction+régionale+de+l'environnement%2C+de+l'aménagement+et+du+logement+(Dreal)&amp;where="/>
</OuSAdresser>
<OuSAdresser ID="R17593" type="Local personnalisé sur SP">
<Titre>Direction régionale et interdépartementale de l'environnement et de l'énergie (Driee)</Titre>
<Complement>Pour obtenir un procès-verbal de réception à titre isolé si vous habitez en Île-de-France</Complement>
<PivotLocal>driee</PivotLocal>
<RessourceWeb URL="https://lannuaire.service-public.fr/recherche?where=&amp;whoWhat=driee"/>
</OuSAdresser>
<OuSAdresser ID="R38617" type="Local personnalisé sur SP">
<Titre>Direction de l'environnement, de l'aménagement et du logement (Deal)</Titre>
<Complement>Pour obtenir un procès-verbal de réception à titre isolé si vous habitez en outre-mer</Complement>
<PivotLocal>deal</PivotLocal>
<RessourceWeb URL="https://lannuaire.service-public.fr/recherche?whoWhat=Direction+de+l%27environnement%2C+de+l%27am%C3%A9nagement+et+du+logement+%28Deal%29&amp;where="/>
</OuSAdresser>
<OuSAdresser ID="R10115" type="Local personnalisable">
<Titre>Professionnel habilité ou agréé pour l'immatriculation des véhicules</Titre>
<Complement>Pour mandater un professionnel agréé qui se chargera de faire la démarche pour vous</Complement>
<PivotLocal>professionnel_immatriculation_vehicule</PivotLocal>
<RessourceWeb URL="https://immatriculation.ants.gouv.fr/Services-associes/Ou-immatriculer-votre-vehicule2"/>
<Source ID="R30628">Agence nationale des titres sécurisés (ANTS)</Source>
</OuSAdresser>
<OuSAdresser ID="R15019" type="Local personnalisable">
<Titre>Service en charge des impôts (trésorerie, centre des impôts fonciers...)</Titre>
<Complement>Pour obtenir le quitus fiscal</Complement>
<PivotLocal>centre_impots</PivotLocal>
<RessourceWeb URL="http://www.impots.gouv.fr/portal/dgi/public/contacts?pageId=contacts&amp;sfid=07#services"/>
<Source ID="R30612">Ministère en charge des finances</Source>
</OuSAdresser>
<OuSAdresser ID="R22333" type="Local personnalisable">
<Titre>Services douaniers en France et en Union européenne (recette de douane, bureau de garantie, direction régionale...)</Titre>
<Complement>Pour obtenir un certificat de dédouanement 846A</Complement>
<PivotLocal>douane_metropole</PivotLocal>
<RessourceWeb URL="https://pro.douane.gouv.fr/rush/API_Service.asp?sid=&amp;app="/>
<Source ID="R30612">Ministère en charge des finances</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020237165" ID="R20050">
<Titre>Arrêté du 9 février 2009 relatif aux modalités d'immatriculation des véhicules</Titre>
</Reference>
<Definition ID="R12140">
<Titre>Contre-visite (contrôle technique)</Titre>
<Texte>
				<Paragraphe>Second examen nécessaire pour vérifier si les points défectueux détectés lors de la visite initiale du véhicule ont été réparés</Paragraphe>
			</Texte>
</Definition>
<Definition ID="R1064">
<Titre>Résidence principale (droit immobilier)</Titre>
<Texte>
				<Paragraphe>Logement occupé au moins 8 mois par an (sauf obligation professionnelle, raison de santé ou cas de force majeure), soit par le preneur (le locataire) ou la personne avec laquelle il vit, soit par une personne à charge</Paragraphe>
			</Texte>
</Definition>
<Definition ID="R15487">
<Titre>PTAC</Titre>
<Texte>
				<Paragraphe>Poids total en charge du véhicule : poids maximal autorisé pour un véhicule, c'est-à-dire le poids du véhicule et de ce qu’il transporte (personnes, marchandises, etc.)</Paragraphe>
			</Texte>
</Definition>
<QuestionReponse ID="F21031" audience="Particuliers">Comment demander son certificat d'immatriculation par correspondance ?</QuestionReponse>
<QuestionReponse ID="F21103" audience="Particuliers">Comment faire une procuration pour obtenir sa carte grise ?</QuestionReponse>
<QuestionReponse ID="F20710" audience="Particuliers">Carte grise (certificat d'immatriculation) : comment faire sa démarche à Paris ?</QuestionReponse>
<QuestionReponse ID="F1028" audience="Particuliers">Carte grise (certificat d'immatriculation) : comment justifier de son domicile ?</QuestionReponse>
<QuestionReponse ID="F31529" audience="Particuliers">Quels recours si une demande de carte grise n'aboutit pas ?</QuestionReponse>
<QuestionReponse ID="F16868" audience="Particuliers">Que faire si le certificat d'immatriculation (carte grise) comporte une erreur ?</QuestionReponse>
<QuestionReponse ID="F31853" audience="Particuliers">Carte grise : avec quels documents prouver son identité ? </QuestionReponse>


</Publication>
