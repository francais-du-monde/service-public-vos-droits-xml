<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F21559" type="Fiche Question-réponse" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Qui doit payer en cas de casse dans un magasin ?</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Argent</dc:subject>
<dc:description>Si vous avez cassé involontairement un objet dans un magasin, la loi vous oblige à réparer le préjudice que vous faîtes subir au commerçant.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-10-06</dc:date>
<dc:type>Question-réponse</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F21559</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006136352&amp;cidTexte=LEGITEXT000006070721</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N24033</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Question-réponse</SurTitre>
<Audience>Particuliers</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19803">Argent</Niveau>
<Niveau ID="N24033">Information et protection du consommateur pour tout achat</Niveau>
<Niveau ID="F21559" type="Fiche Question-réponse">Qui doit payer en cas de casse dans un magasin ?</Niveau>
</FilDAriane>
<Theme ID="N19803">
<Titre>Argent</Titre>
</Theme>
<SousThemePere ID="N20154">Consommation</SousThemePere><DossierPere ID="N24033">
<Titre>Information et protection du consommateur pour tout achat</Titre>
<Fiche ID="F24037">Prix, soldes, arrhes et acomptes : ce qu'il faut savoir avant d'acheter</Fiche>
<Fiche ID="F24042">Protection du consommateur</Fiche>
<Fiche ID="F22397">Vente au déballage</Fiche>
<Fiche ID="F33338">Médiation des litiges de la consommation</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Si vous avez cassé involontairement un objet dans un magasin, la loi vous oblige à réparer le préjudice que vous faîtes subir au commerçant.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Responsabilité de la casse</Paragraphe>
</Titre><Paragraphe>Vous devez réparer le préjudice si vous êtes à l'origine de l'accident.</Paragraphe>
<Paragraphe>Vous devez également le faire si l'accident a été causé par :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>votre enfant mineur,</Paragraphe>
</Item>
<Item>
<Paragraphe>un animal dont vous avez la garde,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou une poussette ou un chariot dont vous étiez le conducteur.</Paragraphe>
</Item>
</Liste>
<Paragraphe>En revanche, vous n'êtes pas responsable de l'accident s'il est imputable au commerçant. C'est le cas notamment si l'accident résulte d'allées trop encombrées, de produits placés trop haut ou en équilibre instable.</Paragraphe>
<ANoter>
<Titre>À noter</Titre><Paragraphe>de nombreux commerçants ne vous demanderont rien, car ils sont eux-mêmes couverts par une assurance et qu'ils la feront intervenir pour cela.</Paragraphe>
</ANoter>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Procédure</Paragraphe>
</Titre><Paragraphe>La procédure sera différente selon que vous avez ou non une assurance responsabilité civile. Cette assurance est incluse notamment dans votre <LienInterne LienPublication="F1350" type="Fiche d'information" audience="Particuliers">assurance multirisques habitation</LienInterne> et couvre l'ensemble des membres de votre foyer.</Paragraphe>

<SousChapitre>
<Titre>
<Paragraphe>Vous disposez d'une assurance responsabilité civile</Paragraphe>
</Titre><Paragraphe>S'il s'agit d'un acte involontaire, d'un accident, vous pouvez faire jouer votre garantie responsabilité civile.</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>Communiquez au commerçant votre identité et celle de votre compagnie d'assurance, afin qu'il puisse faire une déclaration auprès de son assurance.</Paragraphe>
</Item>
<Item>
<Paragraphe>Demandez au commerçant les coordonnées de son assurance.</Paragraphe>
</Item>
<Item>
<Paragraphe>Faîtes vous-même une déclaration auprès de votre assurance, dans les 5 jours suivants, par lettre recommandée, et indiquez tous les détails (lieu et date du sinistre, coordonnées du commerçant, circonstances du sinistre).</Paragraphe>
</Item>
<Item>
<Paragraphe>Ne signez aucune déclaration de reconnaissance de responsabilité.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Vous n'avez pas à verser immédiatement d'argent au commerçant, sauf si la casse est un acte volontaire et délibéré.</Paragraphe>
<Attention>
<Titre>Attention</Titre><Paragraphe>il restera probablement une <LienInterne LienPublication="F2706" type="Fiche Question-réponse" audience="Particuliers">franchise</LienInterne> à votre charge pour votre responsabilité civile, il faut donc vérifier dans votre contrat d'assurance la valeur de cette franchise.</Paragraphe>
</Attention>
</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Si vous n'avez pas d'assurance responsabilité civile</Paragraphe>
</Titre><Paragraphe>Même si  vous n'avez  pas d'une assurance responsabilité civile, vous devez  réparer le préjudice causé au commerçant.</Paragraphe>
<Paragraphe>Il ne s'agit pas d'une vente du produit.</Paragraphe>
<Paragraphe>Vous ne devrez payer que la valeur du préjudice subi par le commerçant, c'est à dire <MiseEnEvidence>le prix qu'il a payé lui-même</MiseEnEvidence> le produit. </Paragraphe>
<Paragraphe>Il doit donc justifier de ce prix en vous produisant la facture d'achat du produit que vous avez involontairement cassé et vous n'avez pas à payer plus que ce prix.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><OuSAdresser ID="R50" type="Centre de contact">
<Titre>Fédération française des sociétés d'assurances (FFSA)</Titre>
<Complement>Pour un complément d'information sur la responsabilité civile</Complement><Texte>
						
								<Paragraphe>
			La fédération française des sociétés d'assurances (FFSA) informe le public sur les assurances et participe à la promotion des actions de prévention.
		</Paragraphe>
							
						<Chapitre>
<Titre>
<Paragraphe>Par messagerie</Paragraphe>
</Titre>
								<Paragraphe>
			information@ffsa.fr
		</Paragraphe>
							</Chapitre>
						<Chapitre>
<Titre>
<Paragraphe>Par courrier</Paragraphe>
</Titre>
								<Paragraphe>
			26 boulevard Haussmann
		</Paragraphe>
								<Paragraphe>
			75311 Paris Cedex 09
		</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R2699" type="Local personnalisable">
<Titre>Votre assureur</Titre>
<Complement>Pour un complément d'information sur votre responsabilité civile ou vérifier les franchises de votre contrat</Complement>
<PivotLocal>compagnie_assurance</PivotLocal>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006136352&amp;cidTexte=LEGITEXT000006070721" ID="R610">
<Titre>Code civil : articles 1382 à 1386</Titre>
</Reference>
<QuestionReponse ID="F13977" audience="Particuliers">Que peut faire l'employeur si son salarié à domicile casse un objet ?</QuestionReponse>

</Publication>
