<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F1915" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Contrat de travail du salarié à temps partiel dans le secteur privé</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Travail, Ressources humaines</dc:subject>
<dc:description>Le salarié à temps partiel conclut un contrat de travail avec son employeur. La loi impose la mention de certains éléments. Durant son exécution, le contrat peut faire l'objet de modifications, sous conditions.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre)</dc:contributor>
<dc:date>modified 2014-07-01</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F1915</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006195784&amp;cidTexte=LEGITEXT000006072050, http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006195786&amp;cidTexte=LEGITEXT000006072050</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N458</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Particuliers</Audience>
<Audience>Professionnels</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Particuliers">Accueil particuliers</Niveau>
<Niveau ID="N19806">Travail</Niveau>
<Niveau ID="N458">Temps de travail dans le secteur privé</Niveau>
<Niveau ID="F1915" type="Fiche d'information">Contrat de travail du salarié à temps partiel dans le secteur privé</Niveau>
</FilDAriane>
<Theme ID="N19806">
<Titre>Travail</Titre>
</Theme>
<SousThemePere ID="N19964">Temps de travail et congés</SousThemePere><DossierPere ID="N458">
<Titre>Temps de travail dans le secteur privé</Titre><SousDossier ID="N458-1">
<Titre>Durée du travail</Titre>
<Fiche ID="F1911">Durée légale du travail</Fiche>
<Fiche ID="F2216">Durée légale du travail des jeunes</Fiche>
<Fiche ID="F19261">Durée du travail du salarié : forfait en heures ou en jours</Fiche>
</SousDossier>
<SousDossier ID="N458-2">
<Titre>Travail à temps partiel</Titre>
<Fiche ID="F874">Bénéficiaires du temps partiel</Fiche>
<Fiche ID="F1915">Contrat de travail</Fiche>
<Fiche ID="F32428">Durée de travail</Fiche>
<Fiche ID="F878">Heures complémentaires</Fiche>
<Fiche ID="F876">Droits du salarié à temps partiel</Fiche>
<Fiche ID="F2243">Travail à temps partiel pour raisons familiales</Fiche>
<Fiche ID="F2247">Travail intermittent</Fiche>
</SousDossier>
<SousDossier ID="N458-3">
<Titre>Repos et jours fériés</Titre>
<Fiche ID="F990">Repos quotidien</Fiche>
<Fiche ID="F2327">Repos hebdomadaire</Fiche>
<Fiche ID="F13887">Repos dominical</Fiche>
<Fiche ID="F2405">Jours fériés et ponts</Fiche>
<Fiche ID="F1907">Compte épargne-temps (CET)</Fiche>
</SousDossier>
<SousDossier ID="N458-4">
<Titre>Aménagement du temps de travail</Titre>
<Fiche ID="F75">Répartition des horaires</Fiche>
<Fiche ID="F125">Récupération des heures perdues</Fiche>
<Fiche ID="F74">Horaires individualisés</Fiche>
</SousDossier>
<SousDossier ID="N458-5">
<Titre>Heures supplémentaires, équivalence et astreintes</Titre>
<Fiche ID="F2391">Heures supplémentaires</Fiche>
<Fiche ID="F1903">Heures d'équivalence</Fiche>
<Fiche ID="F20873">Astreintes</Fiche>
</SousDossier>
</DossierPere>

<SousDossierPere>Travail à temps partiel</SousDossierPere>
<Introduction>
<Texte><Paragraphe>Le salarié à temps partiel conclut un contrat de travail avec son employeur.
			La loi impose la mention de certains éléments. Durant son exécution, le contrat peut faire l'objet de modifications, sous conditions.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Type de contrat</Paragraphe>
</Titre><Paragraphe>Le contrat de travail d'un salarié à temps partiel peut être conclu pour une durée indéterminée ou déterminée.</Paragraphe>
<Paragraphe>Il doit être établi par écrit.
		Tout avenant au contrat doit également faire l'objet d'un écrit. En l'absence d'écrit, le contrat est présumé être à temps plein.</Paragraphe>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Mentions du contrat</Paragraphe>
</Titre><Paragraphe>Le contrat de travail doit préciser toutes les mentions suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>qualification du salarié,</Paragraphe>
</Item>
<Item>
<Paragraphe>éléments de la <LienInterne LienPublication="F876" type="Fiche d'information" audience="Particuliers">rémunération,</LienInterne>
</Paragraphe>
</Item>
<Item>
<Paragraphe>
<LienInterne LienPublication="F32428" type="Fiche d'information" audience="Particuliers">durée de travail</LienInterne> hebdomadaire ou mensuelle prévue,</Paragraphe>
</Item>
<Item>
<Paragraphe>répartition de la durée du travail entre les jours de la semaine ou les semaines du mois,</Paragraphe>
</Item>
<Item>
<Paragraphe>limites de l'utilisation des <LienInterne LienPublication="F878" type="Fiche d'information" audience="Particuliers">heures complémentaires</LienInterne>,</Paragraphe>
</Item>
<Item>
<Paragraphe>modalités de communication par écrit au salarié des horaires de travail pour chaque journée travaillée,</Paragraphe>
</Item>
<Item>
<Paragraphe>cas dans lesquels la répartition de la durée du travail peut être modifiée et nature de cette modification.</Paragraphe>
</Item>
</Liste>
<Paragraphe>Par exception, il n'est pas nécessaire de mentionner la répartition de la durée du travail dans les cas suivants :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>si  le salarié travaille dans une association ou une entreprise d'aide à domicile,</Paragraphe>
</Item>
<Item>
<Paragraphe>ou si  la répartition de la durée du travail est établie sur une période supérieure à la semaine et au plus égale à l'année.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Répartition des heures de travail</Paragraphe>
</Titre><Paragraphe>L'horaire de travail ne doit pas comporter, au cours d'une même journée, plus d'une interruption d'activité, de 2 heures maximum. </Paragraphe>
<Paragraphe>Toutefois, une convention ou un accord collectif peut déroger à ce principe (et prévoir une interruption supérieure à 2 heures), à condition de :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>définir les amplitudes horaires pendant lesquelles les salariés doivent exercer leur activité,</Paragraphe>
</Item>
<Item>
<Paragraphe>fixer leur répartition dans la journée de travail, moyennant des contreparties spécifiques et en tenant compte des exigences propres à l'activité exercée.</Paragraphe>
</Item>
</Liste>

</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Modification du contrat</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Principe</Paragraphe>
</Titre><Paragraphe>Les règles habituelles concernant la <LienInterne LienPublication="F2339" type="Fiche d'information" audience="Particuliers">modification du contrat de travail</LienInterne> sont applicables au salarié à temps partiel. Ainsi, par exemple, l'employeur ne peut pas modifier la durée de travail du salarié sans son accord.</Paragraphe>
<Paragraphe>Cependant, des spécificités sont prévues dans les cas suivants :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>modification de la répartition de la durée de travail,</Paragraphe>
</Item>
<Item>
<Paragraphe>accomplissement régulier d'heures complémentaires.</Paragraphe>
</Item>
</Liste>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Modification de la répartition de la durée de travail</Paragraphe>
</Titre><Paragraphe>L'employeur peut modifier la répartition de la durée du travail, dans les conditions prévues par le contrat. Dans ce cas, le refus du salarié peut être considéré comme une faute ou un motif de licenciement, sauf s'il est justifié par l'une des raisons suivantes :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>ce changement n'est pas compatible avec des obligations familiales impérieuses ou le suivi d'un enseignement scolaire ou supérieur,</Paragraphe>
</Item>
<Item>
<Paragraphe>la modification n'est pas compatible avec une période d'activité fixée chez un autre employeur ou avec une activité professionnelle non salariée.</Paragraphe>
</Item>
</Liste>
<Paragraphe>L'employeur respecte un préavis de 7 jours, pouvant être réduit à 3 jours ouvrés minimum si une convention ou un accord le prévoit.</Paragraphe>
<Paragraphe>L'employeur peut demander au salarié de changer la répartition de sa durée du travail, même si le contrat de travail n'en a pas prévu les conditions. Dans ce cas, le salarié peut refuser la proposition. Son refus ne peut constituer une faute ou un motif de licenciement.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Heures complémentaires régulières</Paragraphe>
</Titre><Paragraphe>Sauf opposition du salarié, le contrat de travail est modifié dès lors que l'horaire moyen prévu au contrat est dépassé d'au moins 2 heures par semaine (ou de l'équivalent mensuel de cette durée). Ce dépassement doit être constaté durant 12 semaines consécutives ou pendant 12 semaines au cours d'une période de 15 semaines.</Paragraphe>
<Paragraphe>La modification est apportée au contrat sous réserve d'un préavis de 7 jours.</Paragraphe>

</SousChapitre>
</Chapitre>
</Texte><OuSAdresser ID="R250" type="Centre de contact">
<Titre>3939 Allô Service Public</Titre>
<Complement>Pour toute information complémentaire</Complement><Texte>
						<Chapitre>
<Titre>
<Paragraphe>Par téléphone</Paragraphe>
</Titre>
								<Paragraphe>
			3939 (coût : <Valeur>0,15 €</Valeur> TTC la minute)
		</Paragraphe>
								<Paragraphe>
			Du lundi au vendredi de 8h30 à 18h.
		</Paragraphe>
								<Paragraphe>
			Répond aux demandes de renseignement administratif concernant les droits et démarches.
		</Paragraphe>
								<Paragraphe>
			Depuis l'étranger ou hors métropole : +33 (0)1 73 60 39 39 uniquement depuis un poste fixe (coût d'une communication + coût de l'appel international variable selon les pays et les opérateurs).
		</Paragraphe>
							</Chapitre>
					</Texte>
</OuSAdresser>
<OuSAdresser ID="R3040" type="Local personnalisable">
<Titre>Votre direction des ressources humaines (DRH)</Titre>
<Complement>Pour toute information complémentaire</Complement>
<PivotLocal>drh</PivotLocal>
</OuSAdresser>
<OuSAdresser ID="R756" type="Local personnalisable">
<Titre>Vos représentants du personnel</Titre>
<Complement>Pour toute information complémentaire</Complement>
<PivotLocal>representant_personnel</PivotLocal>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006195784&amp;cidTexte=LEGITEXT000006072050" ID="R34150">
<Titre>Code du travail : articles L3123-14 à L3123-16</Titre>
<Complement>Mentions du contrat et heures complémentaires régulières</Complement>
</Reference>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichCode.do?idSectionTA=LEGISCTA000006195786&amp;cidTexte=LEGITEXT000006072050" ID="R34119">
<Titre>Code du travail : articles L3123-21 et L3123-24</Titre>
<Complement>Modification de la répartition de la durée de travail</Complement>
</Reference>
<QuestionReponse ID="F31920" audience="Particuliers">Qu'est-ce qu'un complément d'heures pour le salarié à temps partiel ?</QuestionReponse>
</Publication>
