# Fiches _vos droits_ de service-public.fr au format XML

Ce dépôt Git est mis à jour quotidiennement à partir des fiches _vos droits_ de [https://www.service-public.fr/](service-public.fr) au format XML.

Ces fiches sont des données publiques (ie _open data_) fournies sous licence ouverte par la Direction de l'informationlégale et administrative (Dila).

Les fiches sont mises dans le dépôt, sans aucune modication. Elles correspondent donc exactement aux fiches fournies par la Dila.

* [Scripts de téléchargement et de conversion des fiches](https://framagit.org/francais-du-monde/service-public-to-markdown)
* [À propos de la disponibilité de ces fichiers XML en licence ouverte](https://www.service-public.fr/a-propos/donnees-ouvertes)
* [Téléchargement et documentation des fichiers XML](https://www.service-public.fr/partenaires/comarquage/documentation)
* [Plus de documentation, notamment sur le co-marquage](https://www.service-public.fr/partenaires/comarquage/mise-oeuvre)
