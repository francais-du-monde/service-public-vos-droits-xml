<?xml version="1.0" encoding="UTF-8"?><Publication xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ID="F1926" type="Fiche d'information" xsi:noNamespaceSchemaLocation="../Schemas/3.0/Publication.xsd">
<dc:title>Numéros d'identification et d'immatriculation d'une association</dc:title>
<dc:creator>Direction de l'information légale et administrative</dc:creator>
<dc:subject>Formalités administratives d'une association</dc:subject>
<dc:description>Une association doit demander son identification ou immatriculation auprès de différents organismes.</dc:description>
<dc:publisher>Direction de l'information légale et administrative</dc:publisher>
<dc:contributor>Direction de l'information légale et administrative (Premier ministre), Ministère en charge de la vie associative</dc:contributor>
<dc:date>modified 2015-11-19</dc:date>
<dc:type>Fiche pratique</dc:type>
<dc:format>text/xml</dc:format>
<dc:identifier>F1926</dc:identifier>
<dc:source>http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021190323</dc:source>
<dc:language>Fr</dc:language>
<dc:relation>isPartOf N19554</dc:relation>
<dc:coverage>France entière</dc:coverage>
<dc:rights>https://www.service-public.fr/a-propos/mentions-legales</dc:rights>
<SurTitre>Fiche pratique</SurTitre>
<Audience>Associations</Audience>
<Canal>www.service-public.fr</Canal>
<FilDAriane>
<Niveau ID="Associations">Accueil associations</Niveau>
<Niveau ID="N31403">Formalités administratives d'une association</Niveau>
<Niveau ID="N19554">Création d'une association</Niveau>
<Niveau ID="F1926" type="Fiche d'information">Numéros d'identification et d'immatriculation d'une association</Niveau>
</FilDAriane>
<Theme ID="N31403">
<Titre>Formalités administratives d'une association</Titre>
</Theme><DossierPere ID="N19554">
<Titre>Création d'une association</Titre>
<Fiche ID="F1120">Rédaction des statuts</Fiche>
<Fiche ID="F1119">Déclaration</Fiche>
<Fiche ID="F1926">Immatriculation</Fiche>
</DossierPere>

<Introduction>
<Texte><Paragraphe>Pour être identifiée par les acteurs institutionnels ou privés, l'association doit s'enregistrer auprès de différents organismes et posséder un certain nombre de numéros d'immatriculation. Cet enregistrement est indispensable pour des formalités relatives à l'embauche, l'obtention de subvention, etc.</Paragraphe>
</Texte>
</Introduction>
<Texte><Chapitre>
<Titre>
<Paragraphe>Numéro RNA</Paragraphe>
</Titre><Paragraphe>Lors de la <LienInterne LienPublication="F1119" type="Fiche d'information" audience="Associations">déclaration de création</LienInterne> en préfecture, le greffe des associations procède à son inscription dans le répertoire national des associations (RNA), anciennement répertoire Waldec (Web des associations librement déclarées).</Paragraphe>
<Paragraphe>Cette inscription donne lieu à une première immatriculation sous la forme d'un numéro RNA (appelé parfois par l'administration "numéro de dossier"), composé d'un W suivi de 9 chiffres.</Paragraphe>
<ASavoir>
<Titre>À savoir</Titre><Paragraphe>les numéros Waldec attribués avant 2010 ont automatiquement été requalifiés en numéros RNA.</Paragraphe>
</ASavoir>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Numéros Siren et Siret</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Utilité</Paragraphe>
</Titre><Paragraphe>Les numéros Siren et <LienIntra LienID="R1041" type="Définition de glossaire">Siret</LienIntra> identifient l'association auprès de l'<LienInterne LienPublication="R12417" type="Acronyme">Insee</LienInterne>, afin que son activité puisse être comptabilisée dans les productions statistiques nationales, notamment dans celles relatives à l'activité économique. </Paragraphe>
<Paragraphe>Ils sont uniques et invariables.</Paragraphe>
<Paragraphe>Le Siren, composé de 9 chiffres, identifie l'association elle-même, tandis que le Siret, composé de 14 chiffres, identifie chacun de ses établissements.</Paragraphe>
<Paragraphe>Chaque Siret est une extension du numéro Siren par l'ajout de 5 chiffres.</Paragraphe>
<Paragraphe>Ils n'ont pas de lien avec les caractéristiques de l'association.</Paragraphe>
<Paragraphe>Lorsque l'association n'a qu'un seul établissement, elle possède un seul Siret : celui de son siège social.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Attribution</Paragraphe>
</Titre><Paragraphe>L'attribution de numéros Siren et Siret n'est pas systématique. Elle peut ou doit être demandée par l'association selon son activité.</Paragraphe>
<BlocCas affichage="radio">
<Cas>
<Titre>
<Paragraphe>Association sans personnel</Paragraphe>
</Titre><Paragraphe>Une association subventionnée présente  une demande d'attribution directement auprès de la direction régionale de l'Insee compétente par rapport à son siège social, par courrier (il existe <LienInterne LienPublication="R2628" type="Modèle de document" audience="Associations">un modèle-type</LienInterne>).</Paragraphe><OuSAdresser ID="R2246" type="Local personnalisable">
<Titre>Direction régionale de l'Insee attributaire des numéros Siren et Siret</Titre>
<PivotLocal>insee_siren_siret</PivotLocal>
<RessourceWeb URL="http://www.insee.fr/fr/service/default.asp?page=entreprises/demarches-immatriculation.htm"/>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
</OuSAdresser>

<Paragraphe>Par ailleurs, une association sans personnel peut être assujettie à la TVA ou à l'impôt sur les sociétés. Elle doit présenter une demande d'attribution auprès du greffe du tribunal de commerce par courrier.</Paragraphe><OuSAdresser ID="R29" type="Local personnalisé sur SP">
<Titre>Greffe du tribunal de commerce</Titre>
<PivotLocal>tribunal_commerce</PivotLocal>
<RessourceWeb URL="https://www.infogreffe.fr/societes/recherche-greffe-tribunal/chercher-greffe-tribunal-de-commerce.html"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>

<Paragraphe>Dans les 2 cas, l'association doit joindre à son dossier de demande : </Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>une copie de ses statuts </Paragraphe>
</Item>
<Item>
<Paragraphe>et une copie de l'extrait publié au Journal officiel ou, à défaut, le récépissé de dépôt des statuts en préfecture.</Paragraphe>
</Item>
</Liste>
<Paragraphe>L'association reçoit ensuite un certificat d'inscription.</Paragraphe>
<Attention>
<Titre>Attention</Titre><Paragraphe>le certificat d'inscription doit être précieusement conservé, car il n'est pas délivré de duplicata en cas de perte.</Paragraphe>
</Attention>
</Cas>
<Cas>
<Titre>
<Paragraphe>Association employeur</Paragraphe>
</Titre><Paragraphe>L'association présente une demande d'attribution auprès de l'Urssaf, par téléservice.</Paragraphe><ServiceEnLigne ID="R2089" URL="https://www.cfe.urssaf.fr/saisiepl/unsecure_index.jsp" type="Téléservice">
<Titre>Demande des numéros Siren et Siret comme association employeur</Titre>
<Source ID="R30607">Agence centrale des organismes de sécurité sociale (Acoss)</Source>
<Texte><Paragraphe>Permet à une association d'obtenir ses numéros Siren et Siret, ainsi que son code APE (ou code Naf), en s'enregistrant auprès de l'Urssaf comme association envisageant de devenir employeur.</Paragraphe>
<Paragraphe>Cliquer dans la colonne de gauche sur "déclarer une formalité" ; puis sur "association employeur" et "création".</Paragraphe>

</Texte>
</ServiceEnLigne>

<Paragraphe>L'association doit joindre à son dossier de demande :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>une copie de ses statuts </Paragraphe>
</Item>
<Item>
<Paragraphe>et une copie de l'extrait publié au Journal officiel ou, à défaut, le récépissé de dépôt des statuts en préfecture.</Paragraphe>
</Item>
</Liste>
<Paragraphe>L'association reçoit ensuite un certificat d'inscription.</Paragraphe>
<Attention>
<Titre>Attention</Titre><Paragraphe>le certificat d'inscription doit être précieusement conservé car il n'est pas délivré de duplicata en cas de perte.</Paragraphe>
</Attention>
</Cas>
</BlocCas>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Modifications</Paragraphe>
</Titre><Paragraphe>En cas d'importante <LienInterne LienPublication="F1123" type="Fiche d'information" audience="Associations">modification de l'association</LienInterne> portant sur :</Paragraphe>
<Liste type="puce">
<Item>
<Paragraphe>son titre, </Paragraphe>
</Item>
<Item>
<Paragraphe>son objet, </Paragraphe>
</Item>
<Item>
<Paragraphe>ses activités, </Paragraphe>
</Item>
<Item>
<Paragraphe>son siège social ou ses établissements (ouverture ou fermeture), </Paragraphe>
</Item>
</Liste>
<Paragraphe>Le centre de formalités des entreprises compétent (le greffe du tribunal de commerce, l'URSSAF ou la direction régionale de l'<LienInterne LienPublication="R12417" type="Acronyme">Insee</LienInterne>) doit en être avertie pour actualiser le dossier de l'association.</Paragraphe>
<Paragraphe>Des changements peuvent alors intervenir dans l'attribution du ou des numéros de Siret, mais le numéro Siren reste toujours le même jusqu'à la <LienInterne LienPublication="F1122" type="Fiche d'information" audience="Associations">dissolution de l'association</LienInterne>.</Paragraphe>

</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Code APE (ou code Naf)</Paragraphe>
</Titre><SousChapitre>
<Titre>
<Paragraphe>Utilité</Paragraphe>
</Titre><Paragraphe>Le code d'activité principale exercée (code APE) est attribué par l'<LienInterne LienPublication="R12417" type="Acronyme">Insee</LienInterne>, en même temps que son numéro de Siret et lui permet à des fins statistiques de classer les activités principales de l'association par rapport à la nomenclature d'activités française (code Naf).</Paragraphe>
<Paragraphe>Le code APE se compose de 4 chiffres et une lettre.</Paragraphe>
<Paragraphe>Par association d'idées, le code APE peut, de ce fait, être appelé code Naf par certains acteurs institutionnels.</Paragraphe>
<Paragraphe>Il peut être complété par des codes d'activité principale exercée spécifiques à chaque établissement (codes Apet).</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Attribution</Paragraphe>
</Titre><Paragraphe>Les codes sont déterminés par l'<LienInterne LienPublication="R12417" type="Acronyme">Insee</LienInterne> lors de la demande d'attribution des numéros Siren et Siret.</Paragraphe>
<Paragraphe>L'association n'a pas à les déterminer elle-même.</Paragraphe>

</SousChapitre>
<SousChapitre>
<Titre>
<Paragraphe>Modifications</Paragraphe>
</Titre><Paragraphe>Si l'association considère que les codes ne correspondent pas ou plus à la réalité de ses activités, elle peut en demander le changement, en utilisant un formulaire réservé à cet effet.</Paragraphe>
<ServiceEnLigne ID="R18901" URL="http://www.insee.fr/fr/service/entreprises/sirene/pdf/formulaire-APEN-V6-recto-verso.pdf" format="application/pdf" poids="27.4 KB" type="Formulaire">
<Titre>Demande de modification du code d'activité principale (APE) d'une entreprise</Titre>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
<Texte><Paragraphe>Si vous n'avez pas changé d'activité et que vous estimez que le code APE attribué par l'Insee doit être corrigé, la demande de modification doit être adressée par courrier ou par courriel, sur papier à en-tête ou revêtu du cachet de l'entreprise, à la direction régionale de l'Insee compétente pour le département d'implantation de votre siège social, de votre établissement ou de votre association. </Paragraphe>

</Texte>
</ServiceEnLigne>


</SousChapitre>
</Chapitre>
<Chapitre>
<Titre>
<Paragraphe>Numéro d'agrément</Paragraphe>
</Titre><Paragraphe>Après un nombre minimal d'années d'existence, l'association peut disposer d'un <LienInterne LienPublication="F11966" type="Fiche d'information" audience="Associations">agrément ministériel</LienInterne> : un numéro d'enregistrement est alors souvent attribué.</Paragraphe>

</Chapitre>
</Texte><VoirAussi important="non">
<Fiche ID="F3180" audience="Associations">
<Titre>Subventions versées aux associations</Titre>
<Theme ID="N31405">
<Titre>Financement d'une association</Titre>
</Theme>
</Fiche>
</VoirAussi>
<OuSAdresser ID="R17216" type="Local personnalisé sur SP">
<Titre>Mission d'accueil et d'information des associations (Maia)</Titre>
<Complement>Pour s'informer</Complement>
<PivotLocal>maia</PivotLocal>
<RessourceWeb URL="http://www.associations.gouv.fr/10-toutes-les-ressources-pres-de-chez.html"/>
<Source ID="R30678">Ministère en charge de la vie associative</Source>
</OuSAdresser>
<OuSAdresser ID="R32106" type="Local personnalisé sur SP">
<Titre>Greffe des associations</Titre>
<Complement>Pour connaître son immatriculation au RNA</Complement>
<PivotLocal>prefecture_greffe_associations</PivotLocal>
<RessourceWeb URL="https://lannuaire.service-public.fr/recherche?whoWhat=Greffe+des+associations&amp;where="/>
</OuSAdresser>
<OuSAdresser ID="R45" type="Local personnalisé sur SP">
<Titre>Urssaf</Titre>
<Complement>Pour demander une immatriculation comme association employeur</Complement>
<PivotLocal>urssaf</PivotLocal>
<RessourceWeb URL="http://www.contact.urssaf.fr/categorie.do"/>
<Source ID="R30607">Agence centrale des organismes de sécurité sociale (Acoss)</Source>
</OuSAdresser>
<OuSAdresser ID="R29" type="Local personnalisé sur SP">
<Titre>Greffe du tribunal de commerce</Titre>
<Complement>Pour demander une immatriculation comme association assujettie à la TVA ou à l'impôt sur les sociétés</Complement>
<PivotLocal>tribunal_commerce</PivotLocal>
<RessourceWeb URL="https://www.infogreffe.fr/societes/recherche-greffe-tribunal/chercher-greffe-tribunal-de-commerce.html"/>
<Source ID="R30663">Ministère en charge de la justice</Source>
</OuSAdresser>
<OuSAdresser ID="R2246" type="Local personnalisable">
<Titre>Direction régionale de l'Insee attributaire des numéros Siren et Siret</Titre>
<Complement>Pour demander une immatriculation comme association subventionnée</Complement>
<PivotLocal>insee_siren_siret</PivotLocal>
<RessourceWeb URL="http://www.insee.fr/fr/service/default.asp?page=entreprises/demarches-immatriculation.htm"/>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
</OuSAdresser>
<Reference type="Texte de référence" URL="http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000021190323" ID="R36061">
<Titre>Arrêté du 14 octobre 2009 relatif au répertoire national des associations</Titre>
</Reference>
<ServiceEnLigne ID="R18901" URL="http://www.insee.fr/fr/service/entreprises/sirene/pdf/formulaire-APEN-V6-recto-verso.pdf" format="application/pdf" poids="27.4 KB" type="Formulaire">
<Titre>Demande de modification du code d'activité principale (APE) d'une entreprise</Titre>
<Source ID="R30605">Institut national de la statistique et des études économiques (Insee)</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R2628" URL="" type="Modèle de document">
<Titre>Demander des numéros Siren et Siret comme association subventionnée</Titre>
<Source ID="R30608">Direction de l'information légale et administrative (Premier ministre)</Source>
</ServiceEnLigne>
<ServiceEnLigne ID="R2089" URL="https://www.cfe.urssaf.fr/saisiepl/unsecure_index.jsp" type="Téléservice">
<Titre>Demande des numéros Siren et Siret comme association employeur</Titre>
<Source ID="R30607">Agence centrale des organismes de sécurité sociale (Acoss)</Source>
</ServiceEnLigne>
<Definition ID="R1041">
<Titre>Siret</Titre>
<Texte>
				<Paragraphe>Identifiant géographique d'un établissement ou d'une entreprise, composé de 14 chiffres</Paragraphe>
			</Texte>
</Definition>
<Abreviation ID="R12417" type="Acronyme">
<Titre>Insee</Titre>
<Texte>
<Paragraphe>Institut national de la statistique et des études économiques</Paragraphe>
</Texte>
</Abreviation>

</Publication>
